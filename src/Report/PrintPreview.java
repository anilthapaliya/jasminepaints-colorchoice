package Report;

import Controller.Util;
import Database.ShadesModel;
import Tools.LoadingDialog;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javax.imageio.ImageIO;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import javafx.scene.shape.Rectangle;
import javax.swing.JDialog;
import net.sf.jasperreports.engine.JRException;

public class PrintPreview {

    public PrintPreview(ObservableList<Rectangle> rectangleList, String imagePath, final LoadingDialog ld) {
        try {
            Set<Color> shadeColor = new HashSet<>();
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/Report/PrintPreview.jasper"));
            Collection<Map<String, ?>> data = new ArrayList<>();

            rectangleList.stream().forEach((r) -> {
                java.awt.Color c = new java.awt.Color(Integer.parseInt(r.getAccessibleHelp()));
                if (!shadeColor.contains(c)) {
                    shadeColor.add(c);
                    data.add(adSubdData(data, r));
                }
            });

            System.out.println("The data size is" + data.size());
            if (data.size() < 1) {
                Map<String, String> subData = new HashMap<>();
                subData.put("Detail", "");
                subData.put("Color", writeImage("image" + 0, Color.WHITE));
                data.add(subData);
            }
            Map parameters = new HashMap<>();
            parameters.put("title", "Jasmine Paints Color Choice ");
            parameters.put("footerNote", "*Note:- The color of shades displayed may differ. "
                    + "Please verify shades with Jasmine Paints shades tool.");
            parameters.put("image", imagePath);
            parameters.put("logo", ImageIO.read(getClass().getResourceAsStream("/Report/new_logo.png")));
            JRDataSource dataSource = new JRMapCollectionDataSource(data);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
                    parameters, dataSource);
            JasperViewer jasperView = new JasperViewer(jasperPrint, false);
            JDialog dialog = new JDialog(ld.getOwner());//the owner
            dialog.setContentPane(jasperView.getContentPane());
            dialog.setSize(jasperView.getSize());
            dialog.setTitle("Print Preview");
            dialog.setLocationRelativeTo(null);
            dialog.setIconImage(Toolkit.getDefaultToolkit().getImage(
                    getClass().getResource("/Controller/images/logo.png")));
            dialog.setVisible(true);
            dialog.addWindowListener(new WindowAdapter() {

                @Override
                public void windowOpened(WindowEvent e) {
                    ld.dispose();
                }

            });
        } catch (JRException | IOException ex) {
            Logger.getLogger(PrintPreview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public PrintPreview() {
    
    }
    
    public void ExportImage(ObservableList<Rectangle> rectangleList, String imagePath, final LoadingDialog ld,File file) {
        try {
            Set<Color> shadeColor = new HashSet<>();
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(getClass().getResourceAsStream("/Report/PrintPreview.jasper"));
            Collection<Map<String, ?>> data = new ArrayList<>();

            rectangleList.stream().forEach((r) -> {
                java.awt.Color c = new java.awt.Color(Integer.parseInt(r.getAccessibleHelp()));
                if (!shadeColor.contains(c)) {
                    shadeColor.add(c);
                    data.add(adSubdData(data, r));
                }
            });

            System.out.println("The data size is" + data.size());
            if (data.size() < 1) {
                Map<String, String> subData = new HashMap<>();
                subData.put("Detail", "");
                subData.put("Color", writeImage("image" + 0, Color.WHITE));
                data.add(subData);
            }
            Map parameters = new HashMap<>();
            parameters.put("title", "Jasmine Paints Color Choice ");
            parameters.put("image", imagePath);
            parameters.put("footerNote", "*Note:- The color of shades displayed may differ. "
                    + "Please verify shades with Jasmine Paints shades tool.");
            parameters.put("logo", ImageIO.read(getClass().getResourceAsStream("/Report/new_logo.png")));
            JRDataSource dataSource = new JRMapCollectionDataSource(data);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
                    parameters, dataSource);

            Util.extractPrintImage(file.getAbsolutePath(), jasperPrint);
            ld.dispose();
        } catch (JRException | IOException ex) {
            Logger.getLogger(PrintPreview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map<String, String> adSubdData(Collection<Map<String, ?>> data, Rectangle r) {
        Map<String, String> subData = new HashMap<>();
        java.awt.Color c = new java.awt.Color(Integer.parseInt(r.getAccessibleHelp()));
        for (ShadesModel s : ShadesModel.findAll()) {
            if (s.red == c.getRed() && s.green == c.getGreen() && s.blue == c.getBlue()) {
                r.setAccessibleRoleDescription(s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in);
                break;
            }
        }
        subData.put("Detail", r.getAccessibleRoleDescription());
        subData.put("Color", writeImage("image" + data.size(), new java.awt.Color(Integer.parseInt(r.getAccessibleHelp()))));
        return subData;
    }

    public String writeImage(String imageName, java.awt.Color c) {
        try {
            int width = 10, height = 10;
            BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bi.setRGB(x, y, c.getRGB());
                }
            }
            String imagePath = "data" + "/" + imageName + ".png";
            ImageIO.write(bi, "png", new File(imagePath));
            return imagePath;
        } catch (IOException ie) {
            Logger.getLogger(PrintPreview.class.getName()).log(Level.SEVERE, null, ie);
        }
        return null;
    }
}
