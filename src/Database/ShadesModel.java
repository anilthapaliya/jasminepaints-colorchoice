package Database;

import Controller.Util;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ShadesModel {

    public String name;
    public String code;
    public String family;
    public int sequence;
    public int red;
    public int green;
    public int blue;
    public String L;
    public String A;
    public String B;
    public String Na_in;
    public String page;
    public String position;
    public String category;
    public String type;
    public static List<ShadesModel> shadesList = new ArrayList<>();

    public ShadesModel() {

    }

    public ShadesModel(ResultSet rs) {
        shadesList.clear();
        try {
            while (rs.next()) {
                ShadesModel s = new ShadesModel();
                s.name = rs.getString("name");
                s.code = rs.getString("code");
                s.family = rs.getString("Family");
                s.sequence = rs.getInt("sequence");
                s.category = rs.getString("category");
                s.red = rs.getInt("Red");
                s.green = rs.getInt("Green");
                s.blue = rs.getInt("Blue");
                s.L = rs.getString("L");
                s.A = rs.getString("A");
                s.B = rs.getString("B");
                s.Na_in = rs.getString("Na_in");
                s.page = rs.getString("page");
                s.position = rs.getString("position");
                s.type=rs.getString("type");
                shadesList.add(s);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static List<ShadesModel> findAll() {
        ShadesModel shadethisodel = null;
        try {
            ResultSet rs = DriverManager.getConnection(Util.getConnection()).prepareStatement("select * from shades").executeQuery();
            shadethisodel = new ShadesModel(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return shadesList;
    }

    public static List<ShadesModel> findByFamily(String family) {
        ShadesModel shadethisodel = null;
        try {
            ResultSet rs = DriverManager.getConnection(Util.getConnection()).prepareStatement("SELECT * FROM shades where family='" + family + "'").executeQuery();
            shadethisodel = new ShadesModel(rs);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return shadesList;
    }

    public static List<ShadesModel> findByFamilyAndColor(String family, int red, int green, int blue) {
        ShadesModel shadethisodel = null;
        try {
            ResultSet rs = DriverManager.getConnection(Util.getConnection()).prepareStatement("SELECT * FROM shades where family='" + family + "' and red='" + red + "'"
                    + " and green='" + green + "' and blue='" + blue + "' order by sequence desc,position desc").executeQuery();
            shadethisodel = new ShadesModel(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return shadesList;
    }

}
