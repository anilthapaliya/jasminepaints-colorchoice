package Database;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CombinationsModel {

    public String family;
    public String category;
    public int combination;
    public String name;
    public int code;
    public int position;
    public static List<CombinationsModel> combinationList = new ArrayList<>();

    public CombinationsModel(ResultSet rs) {
        combinationList.clear();
        try {
            while (rs.next()) {
                this.family = rs.getString("Family");
                this.category = rs.getString("category");
                this.combination = rs.getInt("combination");
                this.name = rs.getString("name");
                this.code = rs.getInt("code");
                this.position = rs.getInt("position");
                CombinationsModel combinationModel = new CombinationsModel(rs);
                combinationList.add(combinationModel);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
