package Database;

import Controller.Util;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class FamilyModel {

    public String name;
    public int red;
    public int green;
    public int blue;
    public static List<FamilyModel> familyList = new ArrayList<>();
    

    public FamilyModel() {
        
    }

    public FamilyModel(ResultSet rs) {
        familyList.clear();
        try {
            while (rs.next()) {
                FamilyModel fM = new FamilyModel();
                fM.name = rs.getString("Name");
                fM.red = rs.getInt("Red");
                fM.green = rs.getInt("Green");
                fM.blue = rs.getInt("Blue");
                familyList.add(fM);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static List<FamilyModel> findAll() {
        System.out.println(Util.getConnection());
        FamilyModel familyModel = null;
        try {
            ResultSet rs = DriverManager.getConnection(Util.getConnection()).prepareStatement("select * from family").executeQuery();
            familyModel = new FamilyModel(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return familyModel.familyList;
    }

    public static List<FamilyModel> getFamilyList() {
        return familyList;
    }
    
}
