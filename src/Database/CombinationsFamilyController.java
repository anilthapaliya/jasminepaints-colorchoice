/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Controller.Util;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author dinesh
 */
public class CombinationsFamilyController {


    public List<CombinationsFamilyModel> findAll() {
        CombinationsFamilyModel cM = null;
        try {
            ResultSet rs = DriverManager.getConnection(Util.getConnection()).prepareStatement("SELECT * FROM combinationsfamily").executeQuery();
            cM = new CombinationsFamilyModel(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cM.combinationFamilyList;
    }

}
