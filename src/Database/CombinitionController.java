package Database;

import Controller.Util;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.List;

public class CombinitionController {


    public CombinitionController() {

    }

    /**
     *
     * @return CombinationModel
     */
    public List<CombinationsModel> findAll() {
        CombinationsModel combinationModel;
        try {
            ResultSet rs = DriverManager.getConnection(Util.getConnection()).prepareStatement("select * from combinations").executeQuery();
            combinationModel = new CombinationsModel(rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return CombinationsModel.combinationList;
    }

   

}
