package Database;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CombinationsFamilyModel {

    public String name;
    public int red;
    public int green;
    public int blue;
    public List<CombinationsFamilyModel> combinationFamilyList = new ArrayList<>();

    public CombinationsFamilyModel() {
    }

    public CombinationsFamilyModel(ResultSet rs) {
        try {
            while (rs.next()) {
                CombinationsFamilyModel cM = new CombinationsFamilyModel();
                cM.name = rs.getString("Name");
                cM.red = rs.getInt("Red");
                cM.green = rs.getInt("Green");
                cM.blue = rs.getInt("Blue");
                combinationFamilyList.add(cM);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
