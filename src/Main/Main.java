package Main;

import Controller.GlobalValues;
import Controller.VisualizerController;
import static Controller.VisualizerController.panel;
import Controller.WelcomeController;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class Main extends Application {

    Stage primaryStage;
    
    @Override
    public void start(Stage primaryStage) {
        try {
            this.primaryStage = primaryStage;
            GlobalValues.setPrimaryStage(this.primaryStage);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/welcome.fxml"));
            Parent root = (Parent) loader.load();
            WelcomeController controller = (WelcomeController) loader.getController();
            Image image = new Image("/Controller/images/bg.jpg");
            controller.imageView.setImage(image);
            
            controller.imageView.fitWidthProperty().bind(primaryStage.widthProperty());
            controller.imageView.fitHeightProperty().bind(primaryStage.heightProperty());
            
            Platform.runLater(()->{
                ObservableList<Screen> screens = Screen.getScreensForRectangle(new Rectangle2D(primaryStage.getX(), primaryStage.getY(), primaryStage.getWidth(), primaryStage.getHeight()));
                Rectangle2D bounds = screens.get(0).getVisualBounds();
                primaryStage.setX(bounds.getMinX());
                primaryStage.setX(bounds.getMinY());
                primaryStage.setWidth(bounds.getWidth());
                primaryStage.setHeight(bounds.getHeight());
                
                Scene scene = new Scene(root);
                primaryStage.setScene(scene);
                
                controller.imageView.fitHeightProperty().bind(scene.heightProperty());
                primaryStage.sizeToScene();
                primaryStage.setMaximized(true);
                primaryStage.setResizable(true);
            });
            
            primaryStage.setTitle("Jasmine Paints - Color Choice");
            primaryStage.show();
            primaryStage.getIcons().add(new Image(Main.class.getResource("logo.png").toExternalForm()));
            //primaryStage.sizeToScene();
            primaryStage.setOnCloseRequest((WindowEvent event) -> {
                System.exit(0);
            });
            KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            manager.addKeyEventDispatcher(new MyDispatcher());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private class MyDispatcher implements KeyEventDispatcher {
        private boolean alpha = true;
        private long lastPressProcessed = 0;

        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            Platform.runLater(() -> {
                if (System.currentTimeMillis() - lastPressProcessed > 500) {
                    if (e.isControlDown()) {
                        switch (e.getKeyCode()) {
                            case KeyEvent.VK_ADD:
                                VisualizerController.leftController.onZoomInHandler(null);
                                break;
                            case KeyEvent.VK_SUBTRACT:
                                VisualizerController.leftController.onZoomOutHandler(null);
                                break;
                            case KeyEvent.VK_P:
                                VisualizerController.headerController.printImageHandle();
                                break;
                            case KeyEvent.VK_E:
                                VisualizerController.headerController.onExportJpg(null);
                                break;
                            case KeyEvent.VK_S:
                                VisualizerController.headerController.onSaveImage(null);
                                break;
                            case KeyEvent.VK_DELETE:
                                VisualizerController.headerController.onDeleteShadeHandle(null);
                                break;
                            case KeyEvent.VK_Z:
                                VisualizerController.headerController.onUndoBtnHandle(null);
                                break;
                            case KeyEvent.VK_Y:
                                VisualizerController.headerController.onRedoBtnHandle(null);
                                break;
                            case KeyEvent.VK_R:
                                VisualizerController.headerController.onResetHandler(null);
                                break;
                            case KeyEvent.VK_G:
                                VisualizerController.headerController.onGroupBtnHandle(null);
                                break;
                            case KeyEvent.VK_U:
                                VisualizerController.headerController.onUngroupBtnHandle(null);
                                break;
                        }
                    } else if (e.isShiftDown()) {
                        if (e.getKeyCode() == KeyEvent.VK_S) {
                            VisualizerController.headerController.save();
                        }
                    } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        if (!VisualizerController.headerController.doneButton.isDisabled()) {
                            VisualizerController.headerController.onDoneHandler(null);
                        }
                    } else if (e.getKeyCode() == KeyEvent.VK_P) {
                        VisualizerController.leftController.polygonToolBtn.setSelected(true);
                        VisualizerController.leftController.onPolygonToolHandle(null);
                    } else if (e.getKeyCode() == KeyEvent.VK_B) {
                        VisualizerController.leftController.brushToolButton.setSelected(true);
                        VisualizerController.leftController.onBrushToolHandle(null);
                    } else if (e.getKeyCode() == KeyEvent.VK_F) {
                        VisualizerController.leftController.onFitOnScreenHandle(null);
                    }
                    lastPressProcessed = System.currentTimeMillis();
                }
            });
            if (e.getID() == KeyEvent.KEY_PRESSED) {
                if (alpha) {
                    panel.getDrawingCanvas().keyPressed(e);
                    alpha = false;
                }
            } else if (e.getID() == KeyEvent.KEY_RELEASED) {
                panel.getDrawingCanvas().keyReleased(e);
                panel.polygonDrawingCanvas.keyReleased(e);
                alpha = true;
            }

            return false;
        }
    }
    
}
