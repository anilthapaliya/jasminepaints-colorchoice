package Utility;
 
import Controller.Util;
import Tools.CustomBrush;
import Tools.CustomPolygon;
import Tools.DrawingCanvas;
import Tools.EndPointRect;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.ArrayList;

public class Utility {

    public static BufferedImage bufferedImageDeepCopy(BufferedImage bi) {
        java.awt.image.ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public static void copyFromToArray(ArrayList<Integer> src, ArrayList<Integer> dst) {
        for (Integer point : src) {
            dst.add(point);
        }
    }

    public static CustomPolygon createLineFromPoints(ArrayList<EndPointRect> tempLinePoints2) {

        int x[] = Utility.fetchArrayOfXFromPoint(tempLinePoints2);
        int y[] = Utility.fetchArrayOfYFromPoint(tempLinePoints2);

        return new CustomPolygon(x, y, x.length);
    }

    public static CustomBrush createBrushFromPath(Path2D.Double path) {
        path.closePath();
        Area area = new Area(path);
        return new CustomBrush(area);
    }

    public static int[] fetchArrayOfXFromPoint(ArrayList<EndPointRect> tempLinePoints2) {
        int x[] = new int[tempLinePoints2.size()];
        for (int i = 0; i < tempLinePoints2.size(); i++) {
            EndPointRect e = tempLinePoints2.get(i);
            e.setPad(DrawingCanvas.getRECT_PAD());
            e.adjustCenter();
            Point p = e.getXy();
            x[i] = p.x;
        }

        return x;

    }

    public static int[] fetchArrayOfYFromPoint(ArrayList<EndPointRect> tempLinePoints2) {
        int y[] = new int[tempLinePoints2.size()];
        for (int i = 0; i < tempLinePoints2.size(); i++) {
            EndPointRect e = tempLinePoints2.get(i);
            e.setPad(DrawingCanvas.getRECT_PAD());
            e.adjustCenter();
            Point p = e.getXy();
            y[i] = p.y;
        }

        return y;

    }

    public static Path2D.Double createPathFromPoints(ArrayList<EndPointRect> points, boolean isClosed) {
        Path2D.Double path = new Path2D.Double();

        int x[] = Utility.fetchArrayOfXFromPoint(points);
        int y[] = Utility.fetchArrayOfYFromPoint(points);

        path.moveTo(x[0], y[0]);
        for (int i = 1; i < x.length; ++i) {
            path.lineTo(x[i], y[i]);
        }
        if (isClosed) {
            path.closePath();
        }

        return path;
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public static ArrayList<Point> getPoints(Path2D.Double path) {
        ArrayList<Point> pointList = new ArrayList<>();
        double[] coords = new double[6];
        int numSubPaths = 0;
        for (PathIterator pi = path.getPathIterator(null);
                !pi.isDone();
                pi.next()) {
            switch (pi.currentSegment(coords)) {
                case PathIterator.SEG_MOVETO:
                    pointList.add(new Point((int) coords[0], (int) coords[1]));
                    ++numSubPaths;
                    break;
                case PathIterator.SEG_LINETO:
                    pointList.add(new Point((int) coords[0], (int) coords[1]));
                    break;
                case PathIterator.SEG_CLOSE:
                    if (numSubPaths > 1) {
                        throw new IllegalArgumentException("Path contains multiple subpaths");
                    }
                    return pointList;
                default:
                    throw new IllegalArgumentException("Path contains curves");
            }
        }
        throw new IllegalArgumentException("Unclosed path");
    }

    public static Point getScaledPoint(Point p) {
        return new Point((int) (p.x / Util.getScale()), (int) (p.y / Util.getScale()));
//        return new Point(p.x, p.y);
    }

    public static Color getContrastColor(Color color) {
        double y = (299 * color.getRed() + 587 * color.getGreen() + 114 * color.getBlue()) / 1000;
        return y >= 128 ? Color.black : Color.white;
    }
}
