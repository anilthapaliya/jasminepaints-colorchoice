/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import Controller.Util;
import Controller.VisualizerController;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author DELL-PC
 */
public class FileWriter {

    public void saveObject(Object obj, File file, String name) {
        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(file + "/" + name);
            try (ObjectOutputStream oOut = new ObjectOutputStream(fOut)) {
                oOut.writeObject(obj);
                oOut.close();
            }
            fOut.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Object getObject(File f, String name) {
        if (f == null) {
            return null;
        }
        File file = new File(f + "/" + name);
        try {
            ObjectInputStream oIn;
            Object o;
            try (FileInputStream fIn = new FileInputStream(file)) {
                oIn = new ObjectInputStream(fIn);
                o = oIn.readObject();
                fIn.close();
            }
            oIn.close();
            return o;
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(FileWriter.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;

    }

    public void saveImage(BufferedImage image) {
        try {
              File file = Util.getFilePath();
//            BufferedImage bi = image;
              File outputfile = new File(file + "/" + "Default.jpg");
//            ImageIO.write(bi, "jpg", outputfile);
              BufferedImage bi = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
              bi.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
              ImageIO.write(bi, "jpg", outputfile);

            BufferedImage drawImage = VisualizerController.panel.getDrawingCanvas().getPreviewImage();
            if (drawImage.getWidth() < drawImage.getHeight()) {
                drawImage = Util.resize(drawImage, drawImage.getWidth(), drawImage.getWidth());
//                    drawImage = (BufferedImage) drawImage.getScaledInstance(drawImage.getWidth(), drawImage.getWidth(), java.awt.Image.SCALE_SMOOTH);
            }
            File thumbnailOutput = new File(file + "/" + "thumbnail.jpg");
            int type = drawImage.getType() == 0 ? BufferedImage.TYPE_INT_RGB : drawImage.getType();
            BufferedImage resizeImageJpg = resizeImage(drawImage, type, 300, 300);
            ImageIO.write(resizeImageJpg, "jpg", thumbnailOutput);
        } catch (Exception e) {
            Logger.getLogger(FileWriter.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private  BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();

        return resizedImage;
    }
}
