/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.awt.Color;

/**
 *
 * @author DELL-PC
 */
public class ColorBoard {

    public static Color getComplementaryColor(Color c) {
          Color col = new Color(c.getRGB());
    int red = 255 - col.getRed();
    int green = 255 - col.getGreen();
    int  blue= 255 - col.getBlue();
    return new Color(red,green,blue,255);
    }
}
