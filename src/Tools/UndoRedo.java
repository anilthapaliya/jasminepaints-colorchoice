/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author DELL-PC
 */
public class UndoRedo {
    
    private int colorIndex;
    private int polygonIndex;
    private int brushIndex;
    private int groupIndex;
    private int deletedShadeIndex;
    private int negativeShadeIndex;
    
    private final HashMap<ToolsModel, Integer> toolsIndex;
    
    private final ArrayList<ToolsModel> toolsStack;

    public UndoRedo() {
        colorIndex = polygonIndex = brushIndex = groupIndex = -1;
        
        toolsStack = new ArrayList<>();
        toolsIndex = new HashMap<>();
    }
    
    public ArrayList<ToolsModel> getToolsStack() {
        return toolsStack;
    }

    public int getPolygonIndex() {
        return polygonIndex;
    }

    public void setPolygonIndex(int polygonIndex) {
        this.polygonIndex = polygonIndex;
    }

    public int getBrushIndex() {
        return brushIndex;
    }

    public void setBrushIndex(int brushIndex) {
        this.brushIndex = brushIndex;
    }

    public int getGroupIndex() {
        return groupIndex;
    }

    public void setGroupIndex(int groupIndex) {
        this.groupIndex = groupIndex;
    }

    public int getColorIndex() {
        return colorIndex;
    }

    public void setColorIndex(int colorIndex) {
        this.colorIndex = colorIndex;
    }

    public HashMap<ToolsModel, Integer> getToolsIndex() {
        return toolsIndex;
    }

    public int getDeletedShadeIndex() {
        return deletedShadeIndex;
    }

    public void setDeletedShadeIndex(int deletedShadeIndex) {
        this.deletedShadeIndex = deletedShadeIndex;
    }

    public int getNegativeShadeIndex() {
        return negativeShadeIndex;
    }

    public void setNegativeShadeIndex(int negativeShadeIndex) {
        this.negativeShadeIndex = negativeShadeIndex;
    }
    
}
