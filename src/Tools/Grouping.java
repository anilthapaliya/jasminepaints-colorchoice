/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL-PC
 */
public class Grouping implements Serializable, ToolsModel {

    private final List<ToolsModel> toolsStack;

    private boolean visible;

    private String ID;

    public Grouping() {
        
        toolsStack = new ArrayList<>();
        visible = true;
    }

    public void addToStack(ToolsModel toolsModel) {
        getToolsStack().add(toolsModel);
        
    }

    public List<ToolsModel> getToolsStack() {
        return toolsStack;
    }

    public void remove(ToolsModel toolsModel) {
        getToolsStack().remove(toolsModel);
    }

    public boolean isToolAvailable(ToolsModel toolsModel) {
//        model.getID().toLowerCase().equals(toolsModel.getID().toLowerCase()
        for (ToolsModel model : getToolsStack()) {
            if (model.equals(toolsModel) || model.getID().toLowerCase().equals(toolsModel.getID().toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void setID(String ID) {
        this.ID = ID;
    }

    @Override
    public void setColor(Color c) {
    }

    @Override
    public Color getColor() {
        return Color.BLACK;
    }

    @Override
    public void setVisible(boolean b) {
        visible = b;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public ToolsModel getShape() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
