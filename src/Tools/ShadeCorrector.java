package Tools;

import Controller.VisualizerController;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ShadeCorrector {

    private final VisualizerController controller;

    public ShadeCorrector(VisualizerController controller) {
        this.controller = controller;
    }

    public void revalidate(Number oldValue, Number newValue) {
        if (oldValue.intValue() == newValue.intValue()) {
            return;
        }
        
        ArrayList<ToolsModel> selectedShade = new ArrayList<>();
        selectedShade = getController().panel.getDrawingCanvas().getTempSelectionList();
        ArrayList<Grouping> grouping = new ArrayList<>();
        grouping = getController().panel.getDrawingCanvas().getGroupingShadesList();
        boolean isThroughShift = getController().panel.getDrawingCanvas().isThroughShift;
      
        if(grouping.isEmpty()){
            applyShadeCorrectorToNonGroupedShades(selectedShade, oldValue, newValue);
        }
        else{
            for(Grouping group : grouping){
                if(isThroughShift && group.isToolAvailable(selectedShade.get(selectedShade.size() - 1))){
                    /* Appply Shade Corrector To All Group Members */
                    applyShadeCorrectorToAllGroupMembers(group, selectedShade.get(0).getColor(), oldValue, newValue);
                }
                else if(isThroughShift && !group.isToolAvailable(selectedShade.get(selectedShade.size() - 1))){
                    applyShadeCorrectorToNonGroupedShades(selectedShade, oldValue, newValue);
                }
                else if(!isThroughShift){
                    for(ToolsModel model : getController().panel.getDrawingCanvas().getTempSelectionList()){
                        Color c = model.getColor();
                        if (model instanceof CustomArea) {
                            ((CustomArea) model).setOriginalColor(c);
                        } else if (model instanceof CustomBrush) {
                            ((CustomBrush) model).setOriginalColor(c);
                        }
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), newValue.intValue()));
                    }
                }
            
            }
        }
        
        /* OLD CODE */
//        for (ToolsModel model : group) {
//            System.out.println("*-*-*-*-*: " + group.size());
//            Color c = model.getColor();
//            if (model instanceof CustomArea) {
//                ((CustomArea) model).setOriginalColor(c);
//            } else if (model instanceof CustomBrush) {
//                ((CustomBrush) model).setOriginalColor(c);
//            }
//            model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), newValue.intValue()));
//            System.out.println("********Color Lagdai Chha!");
//        }
        
//        for (ToolsModel model : getController().panel.getDrawingCanvas().getTempSelectionList()) {
//            Color c = model.getColor();
//            if (model instanceof CustomArea) {
//                ((CustomArea) model).setOriginalColor(c);
//            } else if (model instanceof CustomBrush) {
//                ((CustomBrush) model).setOriginalColor(c);
//            }
//            model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), newValue.intValue()));
//        }
        
        VisualizerController.panel.getDrawingCanvas().repaint();
    }
    
    private void applyShadeCorrectorToAllGroupMembers(Grouping group, Color color, Number oldValue, Number newValue){
        for(ToolsModel model : group.getToolsStack()){
            if(model instanceof CustomArea)
                ((CustomArea) model).setOriginalColor(color);
            else if(model instanceof CustomBrush)
                ((CustomBrush) model).setOriginalColor(color);
            model.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), newValue.intValue()));
        }
        
    }
    
    private void applyShadeCorrectorToNonGroupedShades(ArrayList<ToolsModel> tModel, Number oldValue, Number newValue){
        for(ToolsModel model : tModel){
            Color color = model.getColor();
            if(model instanceof CustomArea)
                ((CustomArea) model).setOriginalColor(color);
            else if(model instanceof CustomBrush)
                ((CustomBrush) model).setOriginalColor(color);
            model.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), newValue.intValue()));
        }
    }

    public VisualizerController getController() {
        return controller;
    }
}
