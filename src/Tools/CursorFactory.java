/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.HashMap;
import javax.swing.ImageIcon;

/**
 *
 * @author DELL-PC
 */
public class CursorFactory {

    private final HashMap<CursorType, Cursor> cursorList;

    public CursorFactory() {
        cursorList = new HashMap<>();
        cursorList.put(CursorType.DEFAULT, createCursor(CursorType.DEFAULT));
        cursorList.put(CursorType.POLYGON, createCursor(CursorType.POLYGON));
        cursorList.put(CursorType.BRUSH, createCursor(CursorType.BRUSH));
        cursorList.put(CursorType.SELECTION, createCursor(CursorType.SELECTION));
        cursorList.put(CursorType.COLORFILL, createCursor(CursorType.COLORFILL));
        cursorList.put(CursorType.NEGATIVETOOL, createCursor(CursorType.NEGATIVETOOL));
        cursorList.put(CursorType.NEGATIVEBRUSH, createCursor(CursorType.NEGATIVEBRUSH));
        cursorList.put(CursorType.PICKONE, createCursor(CursorType.PICKONE));
    }

    public Cursor create(CursorType cursorType) {
        return cursorList.get(cursorType);
    }

    private Cursor createCursor(CursorType cursorType) {
        ImageIcon icon = null;
        String id = null;
        Point point = null;
        switch (cursorType) {
            case DEFAULT:
                Cursor cur = Cursor.getDefaultCursor();
                DrawingCanvas.paintBucket=false;
                return cur;
            case NEGATIVETOOL:
                icon = new ImageIcon(getClass().getResource("/Controller/images/polygon_sub.png"));
                id = "custom_negative";
                point = new Point(1, 5);
                DrawingCanvas.paintBucket=false;
                break;
            case NEGATIVEBRUSH:
                icon = new ImageIcon(getClass().getResource("/Controller/images/eraser.png"));
                id = "custom_negative";
                point = new Point(10, 10);
                DrawingCanvas.paintBucket=false;
                break;
            case POLYGON:
                icon = new ImageIcon(getClass().getResource("/Controller/images/polygon_add.png"));
                id = "custom_cursor_polygon";
                point = new Point(1, 3);
                DrawingCanvas.paintBucket=false;
                break;
            case BRUSH:
                icon = new ImageIcon(getClass().getResource("/Controller/images/brush.png"));
                id = "custom_cursor_brush";
                point = new Point(5, 20);
                DrawingCanvas.paintBucket=false;
                break;
            case SELECTION:
                icon = new ImageIcon(getClass().getResource("/Controller/images/selection.png"));
                id = "custom_cursor_selection";
                point = new Point(15, 10);
                DrawingCanvas.paintBucket=false;
                break;
            case COLORFILL:
                icon = new ImageIcon(getClass().getResource("/Controller/images/color_buket.png"));
                id = "custom_cursor_fill";
                point = new Point(20, 10);
                DrawingCanvas.paintBucket=true;
                break;
            case PICKONE:
                icon = new ImageIcon(getClass().getResource("/Controller/images/pick_one.png"));
                id = "custom_pick_one";
                point = new Point(10, 10);
                DrawingCanvas.paintBucket = true;
                break;
        }

        return Toolkit.getDefaultToolkit().createCustomCursor(
                icon.getImage(),
                point, id);
    }
}
