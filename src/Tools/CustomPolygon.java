/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Controller.Util;
import static Tools.DrawingCanvas.SHADE_DEFAULT_COLOR;
import java.awt.Color;
import java.awt.Polygon;
import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author DELL-PC
 */
public class CustomPolygon extends Polygon implements Serializable, ToolsModel{

    private Color color;
    private String ID;
    
    private int[] xPoints;
    private int[] yPoints;
    private boolean visible;
    
    
    public CustomPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        super(xPoints,yPoints,nPoints);
        this.xPoints = xPoints;
        this.yPoints = yPoints;
        this.color = (Util.getShadeColor() != null)?Util.getShadeColor():SHADE_DEFAULT_COLOR;
        this.ID = UUID.randomUUID().toString();
        visible = true;
    }
    
    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public void setID(String ID) {
        this.ID = ID;
    }

    public int[] getxPoints() {
        return xPoints;
    }

    public void setxPoints(int[] xPoints) {
        this.xPoints = xPoints;
    }

    public int[] getyPoints() {
        return yPoints;
    }

    public void setyPoints(int[] yPoints) {
        this.yPoints = yPoints;
    }

    @Override
    public void setVisible(boolean b) {
        visible = b;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public ToolsModel getShape() {
        return this;
    }

      
    
   
}
