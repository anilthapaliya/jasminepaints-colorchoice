/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Controller.Util;
import static Tools.DrawingCanvas.SHADE_DEFAULT_COLOR;
import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.io.IOException;
import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author DELL-PC
 */
public class CustomArea extends Area implements ToolsModel, Serializable {

    private Color color;
    private Color originalColor;
    private String ID;
    private ToolsModel modifiedShape;
    private ToolsModel originalShape;
    private boolean visible;

    public CustomArea() {
        this.color = (Util.getShadeColor() != null)?Util.getShadeColor():SHADE_DEFAULT_COLOR;
        this.ID = UUID.randomUUID().toString();
        visible = true;
        this.originalColor = this.color;
    }

    public CustomArea(CustomPolygon shape) {
        super(shape);
        this.color = shape.getColor();
        this.ID = shape.getID();
        this.modifiedShape = (ToolsModel) shape;
        visible = true;
        this.originalColor = this.color;
    }

    public CustomArea(CustomArea shape) {
        super(shape);
        this.color = shape.getColor();
        this.ID = shape.getID();
        this.modifiedShape = shape;
        visible = shape.isVisible();
        this.ID = shape.getID();
        this.originalColor = this.color;
    }

    public CustomArea(GeneralPath shape) {
        super(shape);
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String getID() {
        return ID;
    }

    private void writeObject(java.io.ObjectOutputStream out)
            throws IOException {
        Shape sh = AffineTransform.getTranslateInstance(0, 0).
                createTransformedShape(this);
        CustomAreaSaveObject b = new CustomAreaSaveObject(sh);
        b.setColor(color);
        b.setID(ID);
        b.setVisible(visible);
        out.writeObject(b);
    }

    /**
     * Reads object in from in.
     *
     * @param in Input
     * @throws IOException if I/O errors occur while writing to the underlying
     * OutputStream
     * @throws ClassNotFoundException if the class of a serialized object could
     * not be found.
     */
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        CustomAreaSaveObject b = (CustomAreaSaveObject) in.readObject();
        this.setColor(b.getColor());
        this.setOriginalColor(b.getColor());
        this.setID(b.getID());
        this.setVisible(b.isVisible());
        add(new Area(b.getCustomShape()));
        this.setOriginalShape(new CustomArea(this));
    }

    @Override
    public void setVisible(boolean b) {
        visible = b;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public void setID(String id) {
        this.ID = id;
    }

    @Override
    public ToolsModel getShape() {
        return this;
    }

    public void setShape(ToolsModel shape) {
        this.modifiedShape = shape;
    }

    public ToolsModel getOriginalShape() {
        return originalShape;
    }

    public void setOriginalShape(ToolsModel originalShape) {
        this.originalShape = originalShape;
    }

    public Color getOriginalColor() {
        return originalColor;
    }

    public void setOriginalColor(Color originalColor) {
        this.originalColor = originalColor;
    }

    public class CustomAreaSaveObject implements ToolsModel, Serializable {

        private Color color;

        private GeneralPath path;

        private String ID;

        private boolean visible;

        public CustomAreaSaveObject() {
        }

        public CustomAreaSaveObject(Shape area) {
            path = new GeneralPath(area);
        }

        public GeneralPath getCustomShape() {
            return path;
        }

        @Override
        public void setColor(Color c) {
            this.color = c;
        }

        @Override
        public Color getColor() {
            return color;
        }

        @Override
        public void setVisible(boolean b) {
            visible = b;
        }

        @Override
        public boolean isVisible() {
            return visible;
        }

        @Override
        public void setID(String id) {
            this.ID = id;
        }

        @Override
        public String getID() {
            return ID;
        }

        @Override
        public ToolsModel getShape() {
            return null;
        }
    }

}
