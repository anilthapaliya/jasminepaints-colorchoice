/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.awt.Color;
import java.io.Serializable;

/**
 *
 * @author DELL-PC
 */
public interface ToolsModel extends Serializable {

    public void setColor(Color color);

    public Color getColor();

    public void setVisible(boolean b);

    public boolean isVisible();

    public void setID(String id);

    public String getID();

    public ToolsModel getShape();

}
