package Tools;

import Controller.Util;
import Controller.VisualizerController;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javax.swing.JLayeredPane;

public class MainPane extends JLayeredPane {

    private static final long serialVersionUID = 1L;

    public final BufferedImage img;

    public DrawingCanvas drawingCanvas;
    public PolygonDrawingCanvas polygonDrawingCanvas;

    @SuppressWarnings("unchecked")
    public MainPane(BufferedImage img, VisualizerController controller) throws Exception {

        this.img = img;

        setOpaque(false);
        drawingCanvas = new DrawingCanvas(controller);
        drawingCanvas.setBackground(Color.GRAY);
        drawingCanvas.setOpaque(false);
        drawingCanvas.setSize(15000, 15000);

        
        polygonDrawingCanvas = new PolygonDrawingCanvas(getDrawingCanvas());
        polygonDrawingCanvas.setBackground(new Color(0, 0, 0, 0));
        polygonDrawingCanvas.setSize(15000, 15000);

        
        drawingCanvas.setBackground(Color.GRAY);
        drawingCanvas.setPolygonDrawingCanvas(polygonDrawingCanvas);
        setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
        this.add(drawingCanvas, new Integer(0));
        this.setDoubleBuffered(true);
    }

    public void removePolygonDrawingLayer() {
        if (this.getComponentCount() == 2) {
            this.remove(polygonDrawingCanvas);
        }
    }

    public void addPolygonDrawingLayer() {
        if (this.getComponentCount() != 2) {
            this.add(polygonDrawingCanvas, new Integer(1));
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.scale(Util.getScale(), Util.getScale());
        int w = (int) (img.getWidth() * Util.getScale());
        int h = (int) (img.getHeight() * Util.getScale());
        setPreferredSize(new Dimension(w, h));
//        setBackground(Color.GRAY);
    }

    public final DrawingCanvas getDrawingCanvas() {
        return drawingCanvas;
    }
}
