package Tools;

import Controller.Util;
import Controller.VisualizerController;
import static Tools.DrawingCanvas.MIN_POINTS;
import static Tools.DrawingCanvas.SHADE_DEFAULT_COLOR;
import Utility.Utility;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PolygonDrawingCanvas extends JPanel implements Serializable, MouseListener, MouseMotionListener {

    private Graphics2D g2d;

    private final DrawingCanvas drawingCanvas;
    private Shape selectionAreaRectangle;
    private List<Color> tempColor = new ArrayList<>();

    public PolygonDrawingCanvas(DrawingCanvas drawingCanvas) {
        this.drawingCanvas = drawingCanvas;
        this.setDoubleBuffered(true);
        setOpaque(false);

    }

    @Override
    public void addNotify() {
        super.addNotify();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void removeNotify() {
        removeMouseListener(this);
        removeMouseMotionListener(this);
        super.removeNotify();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        if (drawingCanvas.getCurrentShape() != null) {

            g2d.setColor(Color.BLACK);
            g2d.setStroke(getDrawingCanvas().getLineStroke());
//            g2d.setStroke(new BasicStroke(40));
            g2d.draw(drawingCanvas.getCurrentShape());
            if (drawingCanvas.getCurrentPoint() != null && drawingCanvas.getEndPoint() != null) {
                g2d.draw(new Line2D.Float(drawingCanvas.getEndPoint(), drawingCanvas.getCurrentPoint()));
            }
            for (EndPointRect e : drawingCanvas.getTempRectList()) {
                e.setPad(DrawingCanvas.getRECT_PAD());
                e.adjustCenter();
                g2d.setColor(e.getColor());
                g2d.fill(e);
            }
        } else if (drawingCanvas.getCurrentBrush() != null && drawingCanvas.isEnableBrush()) {
            if (drawingCanvas.getStartPoint() != null && drawingCanvas.getEndPoint() != null) {
                g2d.setStroke(drawingCanvas.getBrushStroke());
                if (drawingCanvas.isEnableNegative()) {
                    g2d.setColor(new Color(255, 255, 255, 150));
                } else {
                    g2d.setColor((Util.getShadeColor() != null) ? Util.getShadeColor() : SHADE_DEFAULT_COLOR);
                }
                g2d.draw(drawingCanvas.getCurrentBrush());
            }
        }
        g2d.dispose();
    }

//    public void line(Graphics2D g2d) {
//        Point e = drawingCanvas.getEndPoint();
//        Point c = drawingCanvas.getCurrentPoint();
//        int w = c.x - e.x;
//        int h = c.x - e.y;
//        double m = h / (double) w;
//        double j = e.y;
//        for (int i = e.x; i <= c.x; i++) {
////            putpixel(i, (int) j, color);
//            g2d.setColor(Color.gray);
//            g2d.draw(new Line2D.Float(e.x, e.y, c.x, c.y));
//            j += m;
//        }
//    }

    @Override
    public void mouseClicked(MouseEvent e) {

        System.out.println("Mouse Clicked Called");
        if (SwingUtilities.isLeftMouseButton(e)) {

            if (drawingCanvas.isEnableShading()) {
//                Point p = Utility.getScaledPoint(e.getPoint());
                Point p = Utility.getScaledPoint(e.getPoint());
                if (e.getClickCount() == 2) {
                    if (drawingCanvas.getTempRectList().size() > MIN_POINTS && drawingCanvas.getCurrentShape() != null) {
                        drawingCanvas.getCurrentShape().closePath();
                        drawingCanvas.resetDrawingPolygon();
                        tempColor.clear();
                        repaint();
                        return;
                    }
                }
                if (drawingCanvas.getStartPoint() == null) {
                    drawingCanvas.setStartPoint(p);
                    tempColor.add(drawingCanvas.getSelectionColor());
                } else if (new EndPointRect(drawingCanvas.getStartPoint(), drawingCanvas.getRECT_PAD()).intersects(new EndPointRect(p, drawingCanvas.getRECT_PAD()))) {
                    if (drawingCanvas.getTempRectList().size() < MIN_POINTS) {
                        tempColor.clear();
                        return;
                    }

                    drawingCanvas.getCurrentShape().closePath();

                    drawingCanvas.resetDrawingPolygon();
                    repaint();
                    return;
                }
                drawingCanvas.setEndPoint(p);
                if (drawingCanvas.getCurrentShape() == null) {
                    drawingCanvas.setCurrentShape(new Path2D.Float());
                    drawingCanvas.getCurrentShape().moveTo(p.x, p.y);
                } else {
                    drawingCanvas.getCurrentShape().lineTo(p.x, p.y);
                }
                drawingCanvas.getTempRectList().add(new EndPointRect(p, ColorBoard.getComplementaryColor(drawingCanvas.getRobot().getPixelColor(p.x, p.y)), DrawingCanvas.getRECT_PAD()));
                repaint();
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

        if (drawingCanvas.isEnableShading()) {
            if (drawingCanvas.getStartPoint() != null) {
                drawingCanvas.setCurrentPoint(Utility.getScaledPoint(e.getPoint()));
                this.repaint();
            }

        }
    }

    public Graphics2D getG2d() {
        return g2d;
    }

    public DrawingCanvas getDrawingCanvas() {
        return drawingCanvas;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (!drawingCanvas.isEnableShading() && !drawingCanvas.isEnableBrush()) {
                if (!drawingCanvas.getTempRectList().isEmpty()) {
                    for (int i = 0; i < drawingCanvas.getTempRectList().size(); i++) {
                        EndPointRect er = drawingCanvas.getTempRectList().get(i);
                        Point p = er.getXy();
                        if (new EndPointRect(Utility.getScaledPoint(e.getPoint()), drawingCanvas.getRECT_PAD()).intersects(new EndPointRect(p, drawingCanvas.getRECT_PAD()))) {
                            drawingCanvas.setCurrentIndex(i);
                            drawingCanvas.setCurrentResizingRectPoint(p);
                            drawingCanvas.setEnableDragg(true);
                            drawingCanvas.setDragDelta(Utility.getScaledPoint(e.getPoint()));
                            return;
                        }
                    }
                }
            } else if (drawingCanvas.isEnableBrush()) {
                drawingCanvas.setStartPoint(Utility.getScaledPoint(e.getPoint()));
                Path2D.Double path = new Path2D.Double();
                drawingCanvas.setCurrentBrush(path);
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        drawingCanvas.setEnableDragg(false);
        if (drawingCanvas.isEnableBrush()) {
            if (drawingCanvas.getEndPoint() == null) {
                return;
            }

            if (!drawingCanvas.isEnableNegative()) {
                CustomBrush brushAlt = new CustomBrush(drawingCanvas.getBrushStroke().createStrokedShape(drawingCanvas.getCurrentBrush()));
                brushAlt.setOriginalShape(new CustomBrush(brushAlt));
                drawingCanvas.trimShade(brushAlt);
                drawingCanvas.getShadeList().add(brushAlt);
                VisualizerController.redoUndoManager.addToStack(brushAlt);
                VisualizerController.bottomController.addRecentUsedShade(brushAlt.getColor());
                VisualizerController.bottomController.addShadeSelectedInImage(brushAlt.getColor(), brushAlt.getID());
            } else {
                CustomBrush brush = new CustomBrush(drawingCanvas.getBrushStroke().createStrokedShape(drawingCanvas.getCurrentBrush()));
                brush.setOriginalShape(new CustomBrush(brush));
                drawingCanvas.excludeNegativeShade(brush);
            }

            drawingCanvas.setStartPoint(null);
            drawingCanvas.setEndPoint(null);
            drawingCanvas.setCurrentBrush(null);
            VisualizerController.panel.removePolygonDrawingLayer();
            repaint();
            VisualizerController.panel.addPolygonDrawingLayer();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (drawingCanvas.isEnableDragg()) {
            if (drawingCanvas.getCurrentShape() != null && drawingCanvas.getTempRectList() != null && drawingCanvas.isEnableDragg()) {
                if (drawingCanvas.getCurrentResizingRectPoint() == null) {
                    return;
                }
                Point XY = drawingCanvas.getDraggedPoint(Utility.getScaledPoint(e.getPoint()));
                EndPointRect er = drawingCanvas.getTempRectList().get(drawingCanvas.getCurrentIndex());
                er.setXy(XY);
                Color colorPix = drawingCanvas.getRobot().getPixelColor(XY.x, XY.y);
                System.out.println(colorPix);
                er.setColor(ColorBoard.getComplementaryColor(colorPix));
                drawingCanvas.setCurrentShape(Utility.createPathFromPoints(drawingCanvas.getTempRectList(), true));
                repaint();
            }
        } else if (drawingCanvas.isEnableBrush()) {
            drawingCanvas.setEndPoint(Utility.getScaledPoint(e.getPoint()));
            Path2D.Double path = drawingCanvas.getCurrentBrush();
            path.moveTo(drawingCanvas.getStartPoint().x, drawingCanvas.getStartPoint().y);
            path.lineTo(drawingCanvas.getEndPoint().x, drawingCanvas.getEndPoint().y);
            drawingCanvas.setCurrentBrush(path);
            drawingCanvas.setStartPoint(drawingCanvas.getEndPoint());
            if (drawingCanvas.isEnableNegative()) {
            }
            repaint();
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {

            System.out.println("delete button pressed");
            if (deleteRecentPolygonDrawingPoint()) {
                repaint();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            VisualizerController.panel.drawingCanvas.isEscapse = true;
            VisualizerController.headerController.onResetHandler(null);
            VisualizerController.panel.drawingCanvas.isEscapse = false;
            System.out.println("Esc button pressed");
        }
    }

    public Shape getSelectionAreaRectangle() {
        return selectionAreaRectangle;
    }

    public void setSelectionAreaRectangle(Shape selectionAreaRectangle) {
        this.selectionAreaRectangle = selectionAreaRectangle;
    }

    private boolean deleteRecentPolygonDrawingPoint() {
        if (getDrawingCanvas().isEnableShading() && getDrawingCanvas().getTempRectList().size() > 1) {
            System.out.println("Succesfully Deleted " + getDrawingCanvas().getTempRectList().get(getDrawingCanvas().getTempRectList().size() - 1));
            getDrawingCanvas().getTempRectList().remove(getDrawingCanvas().getTempRectList().size() - 1);
            getDrawingCanvas().getCurrentShape().reset();
            getDrawingCanvas().setCurrentShape(Utility.createPathFromPoints(getDrawingCanvas().getTempRectList(), false));
            EndPointRect endPoint = getDrawingCanvas().getTempRectList().get(getDrawingCanvas().getTempRectList().size() - 1);
            getDrawingCanvas().setEndPoint(new Point(endPoint.getXy()));

            return true;
        }
        return false;
    }
}
