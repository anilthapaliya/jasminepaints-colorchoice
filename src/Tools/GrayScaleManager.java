/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Controller.Util;
import Controller.VisualizerController;
import Utility.Utility;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.awt.Graphics;
/**
 *
 * @author DELL-PC
 */
public class GrayScaleManager {

    private final VisualizerController controller;

    private final BufferedImage grayScaledImage;
    private final HashMap<ToolsModel, BufferedImage> grayScaleportionList;

    public GrayScaleManager(VisualizerController drawingCanvas) {
        this.controller = drawingCanvas;
        grayScaleportionList = new HashMap<>();

        grayScaledImage = toGray(Utility.bufferedImageDeepCopy(Util.getImg()));
    }

    public void grayScaleSelectedPortion(Graphics2D g, Shape selectedArea) {
        g.setClip((Shape) selectedArea);
        g.drawImage(grayScaledImage, 0, 0, null);  
        //g.fill((Shape) selectedArea);
        g.setClip(null);

//            g.setComposite(AlphaComposite.SrcAtop);
//            g.drawImage(grayScaledImage,0,0,null);
            
        
//                g.drawImage(grayScaledImage, 0, 0, null);
//                g.setComposite(AlphaComposite.Clear);
//                
//                g.setComposite(AlphaComposite.Src);
//                g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//                g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//                g.setPaint(new GradientPaint(0, 0, Color.RED, 0, grayScaledImage.getHeight(), Color.YELLOW));
//                g.fill((Shape) selectedArea);
//                
//                g.setComposite(AlphaComposite.SrcAtop);
//                
//                g.drawImage(grayScaledImage, 0, 0, null);
//        g.setComposite(AlphaComposite.Src);
//        g.fill((Shape) selectedArea);
//        g.setComposite(AlphaComposite.SrcAtop);
//        g.drawImage(grayScaledImage,0,0,null);
    }

    private BufferedImage toGray(BufferedImage image) {
    /// BEGIN PATCH
            BufferedImage grayImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
            Graphics gi = grayImage.getGraphics();
            gi.drawImage(image, 0, 0, null);
            gi.dispose();
            return grayImage;
    /// END PATCH
    }
    public VisualizerController getController() {
        return controller;
    }
    public HashMap<ToolsModel, BufferedImage> getGrayScaleportionList() {
        return grayScaleportionList;
    }
    public BufferedImage  getGrayImage(){
        return grayScaledImage;
    }
}
