/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.awt.Color;

/**
 *
 * @author DELL-PC
 */
public class Negative implements ToolsModel{
    
    private final ToolsModel negativeShade;
    private final ToolsModel originalShade;
    private boolean visible;
    
    public Negative(ToolsModel negativeShade, ToolsModel originalShade) {
        this.negativeShade = negativeShade;
        this.originalShade = originalShade;
        this.visible = true;
    }
    
    public ToolsModel getNegativeShade() {
        return negativeShade;
    }
    
    @Override
    public void setColor(Color color) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Color getColor() {
        return negativeShade.getColor();
    }

    @Override
    public void setVisible(boolean b) {
        visible = b;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    public ToolsModel getOriginalShade() {
        return originalShade;
    }

    @Override
    public void setID(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getID() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ToolsModel getShape() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}