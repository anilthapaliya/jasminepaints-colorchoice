/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Controller.VisualizerController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class RedoUndoManager {

    private final UndoRedo undoRedo;
    private int currentIndex;
    private int undoOrRedo;

    private boolean undoInitialClick;
    private boolean redoInitialClick;

    private final DrawingCanvas panel;

    public RedoUndoManager(DrawingCanvas panel) {
        this.panel = panel;
        undoRedo = new UndoRedo();
        currentIndex = 0;
        undoInitialClick = false;
        redoInitialClick = false;
    }

    public void addToStack(ToolsModel tools) {
        if (getCurrentIndex() != 0) {
            resetRedo();
        }

        getUndoRedo().getToolsStack().add(tools);
        if (VisualizerController.headerController.undoBtn.isDisable()) {
            VisualizerController.headerController.undoBtn.setDisable(false);
        }

    }

    public void onShadeDelete(ArrayList<ToolsModel> modelSelectionList) {
        HashMap<ToolsModel, Integer> deletedShadeMap = new HashMap<>();
        modelSelectionList.stream().map((model) -> {
            deletedShadeMap.put(model, panel.getShadeList().indexOf(model));
            return model;
        }).forEach((model) -> {
            VisualizerController.bottomController.updateShadeSelectedImage(model, "Delete");
        });
        addToStack(new DeletedShade(deletedShadeMap));
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public boolean redo() {
        setUndoOrRedo(-1);
        if (isRedoInitialClick()) {
            setCurrentIndex(getCurrentIndex() - getUndoOrRedo());
            setUndoInitialClick(true);
            setRedoInitialClick(false);
        }
        return update();
    }

    public boolean undo() {
        setUndoOrRedo(1);
        setRedoInitialClick(true);
        if (isUndoInitialClick()) {
            setCurrentIndex(getCurrentIndex() - getUndoOrRedo());
            setUndoInitialClick(false);
        }
        return update();
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public boolean update() {

        boolean retrn = true;

        ArrayList<ToolsModel> toolsStack = getUndoRedo().getToolsStack();

        if (getCurrentIndex() == toolsStack.size() - 1 && getUndoOrRedo() == 1) {
            retrn = false;
        }
        if ((getCurrentIndex() == 2 || getCurrentIndex() == 1) && getUndoOrRedo() == -1) {
            retrn = false;
        }

        setCurrentIndex(getCurrentIndex() + getUndoOrRedo());

        revalidate(toolsStack);
        return retrn;
    }

    private void revalidate(ArrayList<ToolsModel> toolsStack) {
        ToolsModel toolsModel = toolsStack.get(toolsStack.size() - (getCurrentIndex()));

        if (toolsModel instanceof CustomColor) {
            CustomColor customColor = (CustomColor) toolsModel;
            getUndoRedo().setColorIndex(getUndoRedo().getPolygonIndex() + getUndoOrRedo());

            if (customColor.isVisible()) {
                customColor.setVisible(false);
                executeColorUndo(customColor);
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Remove");
                System.out.println("Color undo done");
            } else {
                customColor.setVisible(true);
                executeColorRedo(customColor);
              VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Add");
                System.out.println("Color redo done");
            }

            panel.repaint();

        } else if (toolsModel instanceof CustomArea) {

            getUndoRedo().setPolygonIndex(getUndoRedo().getPolygonIndex() + getUndoOrRedo());

            if (toolsModel.isVisible()) {
                toolsModel.setVisible(false);

                getUndoRedo().getToolsIndex().put(toolsModel, panel.getShadeList().indexOf(toolsModel));
                panel.getShadeList().remove(toolsModel);
                panel.unTrimShade(toolsModel);
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Remove");
                System.out.println("Polygon undo done");
            } else {
                System.out.println("polygon redo index " + getUndoRedo().getToolsIndex().get(toolsModel));
                toolsModel.setVisible(true);
                panel.trimShade(toolsModel);
                panel.getShadeList().add(toolsModel);
                getUndoRedo().getToolsIndex().remove(toolsModel);
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Add");
                System.out.println("Polygon redo done");
            }

            panel.repaint();
        } else if (toolsModel instanceof CustomBrush) {

            System.out.println("Brush undo");
            getUndoRedo().setBrushIndex(getUndoRedo().getBrushIndex() + getUndoOrRedo());

            if (toolsModel.isVisible()) {
                toolsModel.setVisible(false);
                getUndoRedo().getToolsIndex().put(toolsModel, panel.getShadeList().indexOf(toolsModel));
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Remove");
                panel.getShadeList().remove(toolsModel);
                panel.unTrimShade(toolsModel);
                System.out.println("Brush undo done");
            } else {
                System.out.println("Brush redo index " + getUndoRedo().getToolsIndex().get(toolsModel));
                toolsModel.setVisible(true);
                panel.trimShade(toolsModel);
                panel.getShadeList().add(toolsModel);
                getUndoRedo().getToolsIndex().remove(toolsModel);
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Add");
                System.out.println("Brush redo done");
            }

            System.out.println("Brush Index " + getUndoRedo().getBrushIndex());
            panel.repaint();

        } else if (toolsModel instanceof Grouping) {
            System.out.println("Group undo");

            Grouping grouping = (Grouping) toolsModel;

            getUndoRedo().setGroupIndex(getUndoRedo().getGroupIndex() + getUndoOrRedo());

            if (grouping.isVisible()) {
                grouping.setVisible(false);
                getUndoRedo().getToolsIndex().put(toolsModel, panel.getGroupingShadesList().indexOf(grouping));
                panel.getGroupingShadesList().remove(grouping);
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Remove");
                System.out.println("Grouping undo done");
            } else {
                grouping.setVisible(true);
                panel.getGroupingShadesList().add((Grouping) toolsModel);
                getUndoRedo().getToolsIndex().remove(toolsModel);
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Add");
                System.out.println("Grouping redo done");
            }

            System.out.println("Group index " + getUndoRedo().getGroupIndex());
            panel.repaint();
        } else if (toolsModel instanceof DeletedShade) {
            System.out.println("Deleted shade undo");

            DeletedShade deletedShade = (DeletedShade) toolsModel;

            getUndoRedo().setDeletedShadeIndex(getUndoRedo().getDeletedShadeIndex() + getUndoOrRedo());

            if (deletedShade.isVisible()) {
                deletedShade.setVisible(false);
                executeDeleteShadeUndo(deletedShade);

                System.out.println("Deleted shade undo done");
            } else {
                deletedShade.setVisible(true);
                executeDeleteShadeRedo(deletedShade);

                System.out.println("Deleted shade redo done");
            }
            System.out.println("Deleted shade index " + getUndoRedo().getDeletedShadeIndex());
            panel.repaint();
        } else if (toolsModel instanceof Negative) {
            System.out.println("Negative shade undo");

            Negative negativeShade = (Negative) toolsModel;

            getUndoRedo().setNegativeShadeIndex(getUndoRedo().getNegativeShadeIndex() + getUndoOrRedo());

            if (negativeShade.isVisible()) {
                negativeShade.setVisible(false);
                panel.includeNegativeShade(negativeShade.getNegativeShade(), negativeShade.getOriginalShade());
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Remove");
                System.out.println("Negative shade undo done");
            } else {
                negativeShade.setVisible(true);
                panel.deductShade(negativeShade.getOriginalShade(), negativeShade.getNegativeShade());
                VisualizerController.bottomController.updateShadeSelectedImage(toolsModel, "Add");
                System.out.println("Negative shade redo done");
            }

            System.out.println("Negative shade index " + getUndoRedo().getNegativeShadeIndex());
            panel.repaint();
        }

    }

    public UndoRedo getUndoRedo() {
        return undoRedo;
    }

    public int getUndoOrRedo() {
        return undoOrRedo;
    }

    public void setUndoOrRedo(int undoOrRedo) {
        this.undoOrRedo = undoOrRedo;
    }

    private void resetRedo() {
        System.out.println("Reset Redo executed");
        ArrayList<ToolsModel> toBeRemoved = new ArrayList<>();
        for (int i = getUndoRedo().getToolsStack().size() - getCurrentIndex(); i < getUndoRedo().getToolsStack().size(); i++) {
            ToolsModel model = getUndoRedo().getToolsStack().get(i);
            toBeRemoved.add(model);
            getUndoRedo().getToolsIndex().remove(model);
            System.out.println("remove num " + i);
        }

        getUndoRedo().getToolsStack().removeAll(toBeRemoved);

        setCurrentIndex(0);
        System.out.println("current index " + getCurrentIndex());
        setUndoInitialClick(false);

        VisualizerController.headerController.redoBtn.setDisable(true);
    }

    public boolean isUndoInitialClick() {
        return undoInitialClick;
    }

    public void setUndoInitialClick(boolean undoInitialClick) {
        this.undoInitialClick = undoInitialClick;
    }

    public boolean isRedoInitialClick() {
        return redoInitialClick;
    }

    public void setRedoInitialClick(boolean redoInitialClick) {
        this.redoInitialClick = redoInitialClick;
    }

    private void executeColorUndo(CustomColor customColor) {

        ToolsModel model = customColor.getShape();

        int index;

        if (model instanceof CustomArea || model instanceof CustomBrush) {
            index = panel.getShadeList().indexOf(model);
            getUndoRedo().getToolsIndex().put(customColor, index);
            panel.getShadeList().get(index).setColor(customColor.getColor());
            System.out.println("polygon color undo" + customColor.getColor());

        } else if (model instanceof Grouping) {
            index = panel.getGroupingShadesList().indexOf((Grouping) model);
            getUndoRedo().getToolsIndex().put(customColor, index);
            panel.applyColorToGroupMembers((Grouping) model, customColor.getGroupColorList());

            System.out.println("group color undo"+ customColor.getColor());
        }

    }

    private void executeColorRedo(CustomColor customColor) {

        ToolsModel model = customColor.getShape();

        int index;

        if (model instanceof CustomArea || model instanceof CustomBrush) {
            index = panel.getShadeList().indexOf(model);
            getUndoRedo().getToolsIndex().remove(customColor);
            panel.getShadeList().get(index).setColor(customColor);
            System.out.println("polygon color redo");
        } else if (model instanceof Grouping) {
            index = panel.getGroupingShadesList().indexOf((Grouping) model);
            getUndoRedo().getToolsIndex().remove(customColor);
            panel.applyColorToGroupMembers(panel.getGroupingShadesList().get(index), customColor);
            System.out.println("group color redo");
        }

    }

    private void executeDeleteShadeUndo(DeletedShade deletedShade) {
        HashMap<ToolsModel, Integer> deletedShades = deletedShade.getToolsList();

        for (Map.Entry<ToolsModel, Integer> entrySet : deletedShades.entrySet()) {
            ToolsModel key = entrySet.getKey();
            Integer value = entrySet.getValue();

            System.out.println("Adding deleted shade");
            key.setVisible(true);

            if (key instanceof CustomArea || key instanceof CustomBrush) {
                panel.trimShade(key);
                if (panel.getShadeList().size() <= value) {
                    panel.getShadeList().add(key);
                } else {
                    panel.getShadeList().add(value, key);
                }
            } 

            VisualizerController.bottomController.updateShadeSelectedImage(key, "Add");

        }

    }

    private void executeDeleteShadeRedo(DeletedShade deletedShade) {

        HashMap<ToolsModel, Integer> deletedShades = deletedShade.getToolsList();

        for (Map.Entry<ToolsModel, Integer> entrySet : deletedShades.entrySet()) {
            ToolsModel key = entrySet.getKey();

            panel.unTrimShade(key);
            panel.getShadeList().remove(key);
            VisualizerController.bottomController.updateShadeSelectedImage(key, "Remove");
        }
    }


}
