package Tools;

import java.awt.Color;
import java.awt.Point;

public class EndPointRect extends java.awt.Rectangle {

    private static final long serialVersionUID = -2176386226051046238L;

    private int pad;

    private Point xy;

    private Color color;

    public EndPointRect(Point p, int pad) {
        this.pad = pad;
        this.x = p.x;
        this.y = p.y;
        this.xy = p;
        this.width = 2 * pad;
        this.height = 2 * pad;

    }

    public EndPointRect(Point p, Color color, int pad) {
        this.pad = pad;
        this.x = p.x;
        this.y = p.y;
        this.xy = p;
        this.width = 2 * pad;
        this.height = 2 * pad;
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Point getXy() {
        return xy;
    }

    public void setXy(Point xy) {
        this.xy = xy;
        this.x = xy.x;
        this.y = xy.y;
    }

    public int getPad() {
        return pad;
    }

    public void setPad(int pad) {
        this.pad = pad;
        this.width = 2 * pad;
        this.height = 2 * pad;
    }

    public void adjustCenter() {
        this.x = this.xy.x - pad;
        this.y = this.xy.y - pad;
    }
}
