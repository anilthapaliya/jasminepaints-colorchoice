/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Controller.Util;
import Controller.VisualizerController;
import Utility.FileWriter;
import Utility.Utility;
import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.Shape;
import java.awt.Transparency;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.animation.ScaleTransition;
import javafx.scene.control.Alert;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 *
 * @author DELL-PC
 */
public class DrawingCanvas extends JPanel implements MouseListener, MouseMotionListener, Serializable {

    private Graphics2D g2d;
    private final BufferedImage editorImage;

    public static final int MIN_POINTS = 3;
    public static final String SHADE_ARRAY_NAME = "sarea.crd";
    public static final String GROUP_ARRAY_NAME = "group.crd";

    private Point startPoint;
    private Point endPoint;
    private Point currentPoint;
    private Path2D currentShape;
    private Path2D.Double currentBrush;
    private final ArrayList<EndPointRect> tempRectList;
    private final ArrayList<Grouping> groupingShadesList;
    private final ArrayList<ToolsModel> tempSelectionList;
    private final ArrayList<ToolsModel> shadeList;

    private final BufferedImage img;
    private boolean enableDragg;

    private Point dragDelta = new Point();
    private Point currentResizingRectPoint;
    private int currentIndex;

    private boolean enableShading = false;
    private boolean enableBrush = false;
    private boolean enableNegative = false;
    private boolean hoverOut = false;

    public static final int HOVER_ALPHA = 80; //210;
    public static final int DEFAULT_ALPHA = 190;
    private static final int SELECTION_ALPHA = 230;
    private static final int NULL_ALPHA = 0;
    private static final int NULL_HOVER_ALPHA = 205;
    private static final int UNKNOWN_ALPHA = 211;

    public static int RECT_PAD = 3;

    public static final Color SHADE_DEFAULT_COLOR = new Color(106, 112, 120, DEFAULT_ALPHA);
    private BasicStroke brushStroke;
    private BasicStroke lineStroke;

    private boolean isCtrlDown = false;
    public boolean isShiftDown = false;
    public boolean isThroughShift = false;

    // get rectangle size while Zooming
    public static int getRECT_PAD() {
        return (int) (RECT_PAD / Util.getScale());
    }
    public boolean isEscapse = false;
    private final VisualizerController controller;
    private final FileWriter fileWriter;

    private final GrayScaleManager grayScaleManager;
    private PolygonDrawingCanvas polygonDrawingCanvas;

    private final Robot robot = new Robot();

    private boolean loaded = false;
    public static boolean paintBucket = false;
    /**/
    private BufferedImage masked;

    //private final HashMap<ToolsModel, BufferedImage> grayScaleportionList;
/**/
    public DrawingCanvas(VisualizerController controller) throws Exception {
        System.out.println("DrawingCanvas");
        this.img = Util.getImg();
        this.controller = controller;

        fileWriter = new FileWriter();
        tempRectList = new ArrayList<>();
        tempSelectionList = new ArrayList<>();
        grayScaleManager = new GrayScaleManager(controller);

        if(img.getType()==0){
            editorImage = new BufferedImage(img.getWidth(),img.getHeight(),5);
        }else{
            editorImage = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
        }
        Object obj = fileWriter.getObject(Util.getFilePath(), SHADE_ARRAY_NAME);
        if (obj != null) {
            shadeList = (ArrayList<ToolsModel>) obj;
        } else {
            shadeList = new ArrayList<>();
        }

        obj = fileWriter.getObject(Util.getFilePath(), GROUP_ARRAY_NAME);
        if (obj != null) {
            groupingShadesList = (ArrayList<Grouping>) obj;
            updateGroupingList();
        } else {
            groupingShadesList = new ArrayList<>();
        }

        this.setDoubleBuffered(true);

        setCursor(VisualizerController.cursorFactory.create(CursorType.DEFAULT));

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        {
            Graphics2D g2d = this.editorImage.createGraphics();
            g2d.drawImage(img, 0, 0, null);

            Composite defComposite = g2d.getComposite();
            g2d.setComposite(defComposite);
/// BEGIN PATCH
            boolean usePaintShaderListNew = true;
            if (usePaintShaderListNew) {
                paintShaderListNew(g2d, shadeList);
            } else {
/// END PATCH                
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

                shadeList.forEach((shape) -> {
                    g2d.setColor(shape.getColor());

                    if (shape.getColor().getAlpha() != NULL_ALPHA) {
                        grayScaleManager.grayScaleSelectedPortion(g2d, (Shape) shape);
                    }
                    g2d.fill((Shape) shape);
                    if (shape.getColor().getAlpha() == SELECTION_ALPHA) {
                        g2d.setStroke(new BasicStroke(1));
                        g2d.setColor(Color.red.brighter().brighter().brighter());
                        g2d.draw((Shape) shape);
                    }
                });
/// BEGIN PATCH
            }
/// END PATCH                

            if (!isEnableShading() && !isEnableBrush()) {
                if (startPoint != null && endPoint != null) {
                    g2d.setColor(Color.black.brighter().brighter());
                    g2d.setStroke(getLineStroke());
                    int x = startPoint.x, y = startPoint.y;
                    if (endPoint.x > startPoint.x && endPoint.y > startPoint.y) ; else if (endPoint.x > startPoint.x && endPoint.y < startPoint.y) {
                        y = endPoint.y;
                    } else if (endPoint.x < startPoint.x && endPoint.y > startPoint.y) {
                        x = endPoint.x;
                    } else {
                        x = endPoint.x;
                        y = endPoint.y;
                    }
                    g2d.drawRect(x, y, Math.abs(startPoint.x - endPoint.x), Math.abs(endPoint.y - startPoint.y));
                }
            }
            g2d.dispose();
        }

        g.drawImage(editorImage, 0, 0, null);
        if (!loaded) {
            Platform.runLater(() -> {
                VisualizerController.leftController.onFitOnScreenHandle(null);
                loaded = true;
            });
        }
        g.dispose();

    }

    /*Added methods for the changes applymask method*/
    public static BufferedImage applyMask(BufferedImage sourceImage, BufferedImage maskImage, int method) {

        System.out.println("I am in applymask");

        BufferedImage maskedImage = null;
        if (sourceImage != null) {
            System.out.println("I am in applymask in if case");

            int width = maskImage.getWidth(null);
            int height = maskImage.getHeight(null);

            maskedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D mg = maskedImage.createGraphics();

            int x = (width - sourceImage.getWidth(null)) / 2;
            int y = (height - sourceImage.getHeight(null)) / 2;
            mg.drawImage(sourceImage, 0, 0, null);
            mg.setComposite(AlphaComposite.getInstance(method));
            // This is a crap line When you uncomment this the shades can't be redrawn.

            mg.drawImage(maskImage, 0, 0, null);
            mg.dispose();
        }

        return maskedImage;

    }

    /*----Changes till here --------------*/
    public Color getSelectionColor() {
        try {
            PointerInfo pointer;
            pointer = MouseInfo.getPointerInfo();
            Point coord = pointer.getLocation();
            Robot robot = new Robot();
            Color color = robot.getPixelColor((int) coord.getX(), (int) coord.getY());
            return color;
        } catch (AWTException ex) {
            Logger.getLogger(DrawingCanvas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Color.BLACK;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void removeNotify() {
        removeMouseListener(this);
        removeMouseMotionListener(this);
        super.removeNotify();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (!isEnableBrush() && !isEnableShading() && !isCtrlDown && !isShiftDown) {
                for (int i = getShadeList().size() - 1; i >= 0; i--) {
                    ToolsModel model = getShadeList().get(i);
                    Shape shape = (Shape) model.getShape();
                    if (shape.contains(Utility.getScaledPoint(e.getPoint())) || shape.intersects(new EndPointRect(Utility.getScaledPoint(e.getPoint()), getRECT_PAD()))) {
                        if (Util.getShadeColor() == null) {
                            break;
                        }

                        Color modelColor = new Color(model.getColor().getRed(), model.getColor().getGreen(), model.getColor().getBlue());
                        Color selectedColor = new Color(Util.getShadeColor().getRed(), Util.getShadeColor().getGreen(), Util.getShadeColor().getBlue());

                        if(getCursor() == Cursor.getDefaultCursor()){
                            break;
                        }
                        
                        if (modelColor.equals(selectedColor)) {
                            break;
                        }
                        getTempSelectionList().clear();
                        if (!setColorToGroupedShades(model, Util.getShadeColor())) {
                            VisualizerController.redoUndoManager.addToStack(new CustomColor(Util.getShadeColor(), model));
                            model.setColor(Util.getShadeColor());
                            VisualizerController.bottomController.addRecentUsedShade(model.getColor());
                            VisualizerController.bottomController.addShadeSelectedInImage(model.getColor(), model.getID());
                        }
                        repaint();
                        return;
                    }
                }
            }
            
            if(!isShiftDown && !isCtrlDown){
                for(Grouping group : getGroupingShadesList()){
                    for(ToolsModel model : group.getToolsStack()){
                        Color c = model.getColor();
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
                    }
                }
            }
            
            int counter = 0;
            for (int i = getShadeList().size() - 1; i >= 0; i--) {
                ToolsModel model = getShadeList().get(i);
                Shape shape = (Shape) model.getShape();
                if (shape.contains(Utility.getScaledPoint(e.getPoint()))) {
                    System.out.println("Contains point on shade");
                    if (isCtrlDown) {
                        isThroughShift = false;
                        toogleSelectedShadeSelection(model);
                    } //Single Selection Else come here;
                    
                    /* Select whole group of shades from one selected shade */
                    if(isShiftDown){
                        isThroughShift = true;
                        toogleSelectedShadeSelection(model);
                        getTempRectList().clear();
                        getTempSelectionList().add(model);
                        selectGroupMemberShadesFromOneShade(model);
                    }
                    /*  */
                    
                    repaint();
                    break;
                }
                counter++;
            }

            if (counter == getShadeList().size()) {
                if (!getTempSelectionList().isEmpty()) {
                    setSelectionShadeColorToDefault();
                    getTempSelectionList().clear();
                    repaint();
                    System.out.println("removed selection on all");
                }
            }

        }
    }
    
    public void selectGroupMemberShadesFromOneShade(ToolsModel model){
        clearAllShadesFirst();
        for(Grouping group : getGroupingShadesList()){
            if(group.isToolAvailable(model)){
                for(ToolsModel tool : group.getToolsStack()){
                    getTempSelectionList().add(tool);
                    Color color = tool.getColor();
                    tool.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), SELECTION_ALPHA));
                }
            }
            else{
                Color color = model.getColor();
                model.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), SELECTION_ALPHA));
            }
        }
        
    }
    
    public void clearAllShadesFirst(){
        /* Clears a shade not belonging to any group. */
        for(ToolsModel model : getShadeList()){
            for(Grouping group : getGroupingShadesList()){
                if(!group.isToolAvailable(model)){
                    Color color = model.getColor();
                    model.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), DEFAULT_ALPHA));                    
                }
            }
        }
        /* Clears shades belonging to a group. */
        for(Grouping group : getGroupingShadesList()){
            for(ToolsModel tool : group.getToolsStack()){
                    Color color = tool.getColor();
                    tool.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), DEFAULT_ALPHA));
                }
        }
    }

    public void doneButtonCalled() {
        if (!tempRectList.isEmpty()) {
            System.out.println("done button called");
            if (!isEnableShading()) {
                VisualizerController.panel.removePolygonDrawingLayer();
                CustomArea area = new CustomArea(Utility.createLineFromPoints(getTempRectList()));
                area.setOriginalShape(new CustomArea(area));
                if (isEnableNegative()) {
                    excludeNegativeShade(area);
                } else {
                    trimShade(area);
                    getShadeList().add(area);
                    VisualizerController.redoUndoManager.addToStack(area);
                    VisualizerController.bottomController.addRecentUsedShade(area.getColor());
                    VisualizerController.bottomController.addShadeSelectedInImage(area.getColor(), area.getID());
                }
                resetListToDraw();
                VisualizerController.headerController.disableDoneResetButton(true);
                repaint();
                VisualizerController.panel.addPolygonDrawingLayer();
            }
        }
    }

    public boolean groupButtonCalled() {
        if (isEnableShading() || isEnableBrush()) {
            return false;
        }
        if (!tempSelectionList.isEmpty()) {
            if (groupingController()) {
                VisualizerController.bottomController.showTrayNotification("Grouping", "Successfully Grouped.");
            }
            repaint();
            return true;
        }
        return false;
    }

    public boolean unGroupButtonCalled(boolean clear) {
        if (!tempSelectionList.isEmpty() && !isEnableShading() && !isEnableBrush()) {
            removeShadesFromGrouping(getTempSelectionList());
            resetColorToUngroupedShades(getTempSelectionList());
            if (clear) {
                getTempSelectionList().clear();
            }
            VisualizerController.bottomController.showTrayNotification("UnGrouping", "Successfully UnGrouped.");
            repaint();
            return true;
        }
        return false;
    }

    public boolean deleteShadeButtonCalled() {
        if (!tempSelectionList.isEmpty() && !isEnableShading() && !isEnableBrush()) {
            resetColorToUngroupedShades(getTempSelectionList());
            VisualizerController.redoUndoManager.onShadeDelete(getTempSelectionList());
            removeShadesFromGrouping(getTempSelectionList());
            removeShadesPermanently(getTempSelectionList());
            for (ToolsModel model : getTempSelectionList()) {
                unTrimShade(model);
                VisualizerController.bottomController.updateShadeSelectedImage(model, "Remove");
            }
            getTempSelectionList().clear();
            VisualizerController.bottomController.showTrayNotification("Delete", "Shade Deleted Successfully");
            repaint();
            return true;
        }
        return false;
    }

    public void resetButtonCalled() {
        if ((!tempRectList.isEmpty() && !isEnableShading()) || isCtrlDown || isEscapse) {
            resetListToDraw();
            VisualizerController.headerController.disableDoneResetButton(true);
            repaint();
        }
    }

    private void resetListToDraw() {
        setStartPoint(null);
        setCurrentPoint(null);
        setEndPoint(null);

        setCurrentShape(null);
        getTempRectList().clear();
        setEnableShading(true);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (!isEnableShading() && !isEnableBrush()) {
            setStartPoint(Utility.getScaledPoint(e.getPoint()));
            setEnableDragg(true);

        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (!isEnableShading() && !isEnableBrush()) {
            if (startPoint != null && endPoint != null) {
                checkSelectionAreaShades(makeRectangle(startPoint.x, startPoint.y, endPoint.x, endPoint.y));
                setStartPoint(null);
                setEndPoint(null);
                setEnableDragg(false);
                repaint();
            }
        }
    }

    private Rectangle2D.Float makeRectangle(int x1, int y1, int x2, int y2) {
        return new Rectangle2D.Float(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x1 - x2), Math.abs(y1 - y2));
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (isEnableDragg() && !isEnableShading() && !isEnableBrush()) {
            setEndPoint(Utility.getScaledPoint(e.getPoint()));
            repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (isEnableShading()) {
            if (startPoint != null) {
                setCurrentPoint(Utility.getScaledPoint(e.getPoint()));
                repaint();
            }

        } else if (!isEnableShading() && !isEnableBrush()) {
            if (getCurrentShape() == null && getCurrentBrush() == null
                    && !getShadeList().isEmpty()) {
                shadeHover(Utility.getScaledPoint(e.getPoint()));
            }
        }
    }

    public boolean isEnableShading() {
        return enableShading;
    }

    public void setEnableShading(boolean enableShading) {
        this.enableShading = enableShading;
    }

    public void keyPressed(KeyEvent e) {
        if (e.isControlDown()) {
            if (getTempRectList().isEmpty()) {
                isCtrlDown = true;
                resetButtonCalled();
                VisualizerController.leftController.toggleButtonReset();
                disableAllTools();
                setCursor(VisualizerController.cursorFactory.create(CursorType.SELECTION));
            }
        } 
        else if(e.isShiftDown()){
            isShiftDown = true;
            setCursor(VisualizerController.cursorFactory.create(CursorType.PICKONE));
        }
    }

    public void keyReleased(KeyEvent e) {
        if (!e.isControlDown()) {
            setCursor(VisualizerController.cursorFactory.create(CursorType.DEFAULT));
            isCtrlDown = false;
        } else if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            getPolygonDrawingCanvas().keyReleased(e);
        }
        
        if(!e.isShiftDown()){
            isShiftDown = false;
            setCursor(VisualizerController.cursorFactory.create(CursorType.DEFAULT));
        }
    }

    private void shadeHover(Point p) {

        for (int j = getShadeList().size() - 1; j >= 0; j--) {
            ToolsModel shade = getShadeList().get(j);
            Shape shape = (Shape) shade.getShape();
            if ((shape).contains(p) || (shape).intersects(new EndPointRect(p, RECT_PAD))) {
                hoverOut = false;
                if (!getGroupingShadesList().isEmpty()) {
                    if (applyHoverToGroupingShades(shade)) {
                        repaint();
                        return;
                    }
                }
                setRemainingShadesToDefaultColor(shade);
                applyColorToShade(shade, ColorStyle.HOVER);
                repaint();
                return;
            }
            applyColorToShade(shade, ColorStyle.DEFAULT);
        }

        if (!hoverOut) {
            hoverOut = true;
            repaint();
        }

    }

    private void setRemainingShadesToDefaultColor(ToolsModel toolsModel) {

        getShadeList().stream().filter((model) -> !(model.equals(toolsModel) || model.getID().toLowerCase().equals(toolsModel.getID().toLowerCase()))).forEach((model) -> {
            applyColorToShade(model, ColorStyle.DEFAULT);
        });
    }

    public Point getDraggedPoint(Point point) {
        // get location of Window
        int thisX = (int) getCurrentResizingRectPoint().getX();
        int thisY = (int) getCurrentResizingRectPoint().getY();

        // Determine how much the mouse moved since the initial click
        int xMoved = (thisX + point.x) - (thisX + getDragDelta().x);
        int yMoved = (thisY + point.y) - (thisY + getDragDelta().y);

        // Move window to this position
        int X = thisX + xMoved;
        int Y = thisY + yMoved;

        return new Point(X, Y);
    }

    private void removeShadesFromGrouping(ArrayList<ToolsModel> toolsList) {
        getGroupingShadesList().stream().forEach((group) -> {
            toolsList.stream().filter((tool) -> (group.isToolAvailable(tool))).forEach((tool) -> {
                group.remove(tool);
            });
        });

        ArrayList<Grouping> toRemoveList = new ArrayList<>();
        getGroupingShadesList().stream().filter((group) -> (group.getToolsStack().isEmpty())).forEach((group) -> {
            toRemoveList.add(group);
        });

        if (!toRemoveList.isEmpty()) {
            getGroupingShadesList().removeAll(toRemoveList);
            System.out.println("Removed empty list");
        }

    }

    private HashMap<Grouping, ToolsModel> isSelectedShadesAlreadyAdded() {
        int avail = 0;
        HashMap<Grouping, ToolsModel> availableShade = new HashMap<>();

        for (Grouping group : getGroupingShadesList()) {
            for (ToolsModel p : getTempSelectionList()) {
                if (group.isToolAvailable(p)) {
                    if (avail == 0) {
                        availableShade.put(group, p);
                    }
                    avail++;
                }
            }

        }

        if (avail > 1) {
            availableShade.put(new Grouping(), new CustomArea());
        }

        return availableShade;
    }

    private void applyColorToShades(ArrayList<ToolsModel> toolsModel, ColorStyle style) {

        toolsModel.stream().forEach((model) -> {
            applyColorToShade(model, style);
        });

    }

    public void applyColorToShade(ToolsModel model, ColorStyle style) {

        Color c = model.getColor();
        if (null != style) {
            switch (style) {
                case HOVER:
                    if (c.getAlpha() == DEFAULT_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), HOVER_ALPHA));
                    } else if (c.getAlpha() == SELECTION_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), SELECTION_ALPHA));
                    } else if (c.getAlpha() == NULL_ALPHA || c.getAlpha() == NULL_HOVER_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), NULL_HOVER_ALPHA));
                    } else if (c.getAlpha() != HOVER_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), UNKNOWN_ALPHA));
                    }
                    break;
                case DEFAULT:
                    if (c.getAlpha() == HOVER_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
                        break;
                    } else if (c.getAlpha() == SELECTION_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), SELECTION_ALPHA));
                        break;
                    } else if (c.getAlpha() == NULL_HOVER_ALPHA || c.getAlpha() == NULL_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), NULL_ALPHA));
                        break;
                    } else if (c.getAlpha() == UNKNOWN_ALPHA) {
                        if (model instanceof CustomArea) {
                            model.setColor(((CustomArea) model).getOriginalColor());
                        } else if (model instanceof CustomBrush) {
                            model.setColor(((CustomBrush) model).getOriginalColor());
                        }
                    }
                    break;
                case SELECTION_DEFAULT:
                    if (c.getAlpha() == SELECTION_ALPHA) {
                        model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
                    }
                    break;
                default:
                    break;
            }
        }

    }

    private void resetColorToUngroupedShades(ArrayList<ToolsModel> modelList) {
        modelList.stream().forEach((model) -> {
            Color c = model.getColor();
            model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
        });
    }

    public Grouping getGroupingShadesList(ArrayList<ToolsModel> toolsList) {
        Grouping g = new Grouping();
        g.setID(getGroupingID());
        toolsList.stream().map((model) -> {
            Color c = model.getColor();
            model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
            return model;
        }).forEach((model) -> {
            g.addToStack(model);
        });
        return g;
    }

    private void regroupSelectedShades(HashMap<Grouping, ToolsModel> mAvailableShade) {

        Grouping g = null;
        ToolsModel p = null;

        for (Map.Entry<Grouping, ToolsModel> m : mAvailableShade.entrySet()) {
            g = m.getKey();
            p = m.getValue();
            break;
        }
        getGroupingShadesList().get(getGroupingShadesList().indexOf(g)).remove(p);
        Color c;
        Grouping newGroup = null;
        for (ToolsModel model : getTempSelectionList()) {
            c = model.getColor();

            model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
            newGroup = getGroupingShadesList().get(getGroupingShadesList().indexOf(g));
            newGroup.addToStack(model);
        }

        if (newGroup != null) {
            VisualizerController.redoUndoManager.addToStack(newGroup);
        }
    }

    private boolean applyHoverToGroupingShades(ToolsModel model) {
        for (Grouping grouping : getGroupingShadesList()) {
            if (grouping.isToolAvailable(model)) {
                applyHoverToSelectedGroup(grouping);
                return true;
            }
        }

        return false;
    }

    private void applyHoverToSelectedGroup(Grouping grouping) {

        List<ToolsModel> groupPolyMem = grouping.getToolsStack();

        groupPolyMem.stream().forEach((shade) -> {
            applyColorToShade(shade, ColorStyle.HOVER);
        });
    }

    private String getGroupingID() {
        return UUID.randomUUID().toString();
    }

    private void removeShadesPermanently(ArrayList<ToolsModel> toolsList) {
        getShadeList().removeAll(toolsList);
    }

    public boolean isEnableBrush() {
        return enableBrush;
    }

    public void setEnableBrush(boolean enableBrush) {
        this.enableBrush = enableBrush;
    }

    public void disableAllTools() {
        VisualizerController.panel.removePolygonDrawingLayer();
        setEnableShading(false);
        setEnableBrush(false);
    }

    public boolean isEnableNegative() {
        return enableNegative;
    }

    public void setEnableNegative(boolean enableNegative) {
        this.enableNegative = enableNegative;
    }

    public void saveObject() {
        File file = Util.getFilePath();
        if (file == null) {
            return;
        }
        fileWriter.saveObject(getShadeList(), file, SHADE_ARRAY_NAME);
        fileWriter.saveObject(getGroupingShadesList(), file, GROUP_ARRAY_NAME);
        fileWriter.saveImage(img);
    }

    public void resetDrawingPolygon() {
        setStartPoint(null);
        setCurrentPoint(null);
        setEndPoint(null);
        setEnableShading(false);
        VisualizerController.headerController.disableDoneResetButton(false);
    }

    private boolean setColorToGroupedShades(ToolsModel tool, Color shadeColor) {
        for (Grouping grouping : getGroupingShadesList()) {
            if (grouping.isToolAvailable(tool)) {
                applyColorToGroupMembers(grouping, shadeColor);
                return true;
            }
        }
        return false;
    }

    public void applyColorToGroupMembers(Grouping grouping, Color shadeColor) {
        List<ToolsModel> toolsList = grouping.getToolsStack();
        VisualizerController.redoUndoManager.addToStack(new CustomColor(shadeColor, grouping));
        toolsList.stream().forEach((model) -> {
            model.setColor(shadeColor);
            VisualizerController.bottomController.addRecentUsedShade(model.getColor());
            VisualizerController.bottomController.addShadeSelectedInImage(model.getColor(), model.getID());
        });
    }

    public void applyColorToGroupMembers(Grouping grouping, ArrayList<Color> shadeColor) {

        List<ToolsModel> toolsList = grouping.getToolsStack();

        for (int i = 0; i < toolsList.size(); i++) {
            System.out.println("group color " + shadeColor.get(i));
            ToolsModel model = toolsList.get(i);
            model.setColor(shadeColor.get(i));
        }
    }

    private boolean groupingController() {
        if (getTempSelectionList().size() > 1) {

            HashMap<Grouping, ToolsModel> nAvailableShade = isSelectedShadesAlreadyAdded();
            if (!nAvailableShade.isEmpty()) {
                if (nAvailableShade.size() == 1) {
                    regroupSelectedShades(nAvailableShade);
                    getTempSelectionList().clear();
                    return true;
                }
                unGroupButtonCalled(false);
                groupButtonCalled();
                
                return false;
            }

            Grouping grouping = getGroupingShadesList(getTempSelectionList());
            getGroupingShadesList().add(grouping);
            VisualizerController.redoUndoManager.addToStack(grouping);
            getTempSelectionList().clear();
            System.out.println("Grouped successfully");

        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning!");
            alert.setContentText("Select at least two shade for grouping");
            alert.showAndWait();
            applyColorToShades(getTempSelectionList(), ColorStyle.SELECTION_DEFAULT);
            getTempSelectionList().clear();
            return false;
        }

        return true;
    }

    public ArrayList<Grouping> getGroupingShadesList() {
        return groupingShadesList;
    }

    public VisualizerController getController() {
        return controller;
    }

    public void excludeNegativeShade(ToolsModel negativeShade) {
        Area area = new Area();
        for (ToolsModel model : getShadeList()) {
            area.add((Area) model);
            area.intersect(new Area((Shape) negativeShade));
            if (!area.isEmpty()) {
                if (model instanceof CustomArea) {

                    VisualizerController.redoUndoManager.addToStack(new Negative(negativeShade, new CustomArea((CustomArea) model)));

                } else if (model instanceof CustomBrush) {
                    VisualizerController.redoUndoManager.addToStack(new Negative(negativeShade, new CustomBrush((CustomBrush) model)));
                }
                Color c = model.getColor();
                ((Area) model).subtract(new Area((Shape) negativeShade));
                model.setColor(c);
//                area.reset();
//                area.add((Area) model);
                if (area.isEmpty()) {
                    VisualizerController.bottomController.updateShadeSelectedImage(model, "Remove");
                }
                area.reset();
            }
        }
    }

    public void deductShade(ToolsModel originalShade, ToolsModel negativeShade) {

        for (ToolsModel model : getShadeList()) {
            if (model.getID().toLowerCase().equals(originalShade.getID().toLowerCase())) {
                Color c = model.getColor();
                ((Area) model).subtract(new Area((Shape) negativeShade));
                model.setColor(c);
                if (((Area) model).isEmpty()) {
                    VisualizerController.bottomController.updateShadeSelectedImage(model, "Remove");
                }
                break;
            }
        }

    }

    public void trimShade(ToolsModel toolsModel) {
        System.out.println("I am at TrimShade");
        Area area = new Area();
        for (ToolsModel model : getShadeList()) {
            if (model.equals(toolsModel) || model.getID().equals(toolsModel.getID())) {
                continue;
            }
            area.add(new Area((Shape) model));

//            area.intersect(new Area((Shape) toolsModel));
//            if (!area.isEmpty()) {
//                ((Area) model).subtract(new Area((Shape) toolsModel));
//                area.reset();
//                area.add((Area) model);
//                if (area.isEmpty()) {
//                    System.out.println("Empty area....");
//                    VisualizerController.bottomController.updateShadeSelectedImage(model, "Remove");
//                }
//            }
            area.reset();
        }
    }

    public void unTrimShade(ToolsModel toolsModel) {
        System.out.println("I am at UNTrimShade");
        for (ToolsModel model : getShadeList()) {
            ToolsModel originalShape = (model instanceof CustomArea) ? ((CustomArea) model).getOriginalShape() : ((CustomBrush) model).getOriginalShape();

            Area area = new Area((Shape) originalShape);
            area.intersect(new Area((Shape) toolsModel));
            if (!area.isEmpty()) {
                if (new Area((Shape) model).isEmpty()) {
                    VisualizerController.bottomController.updateShadeSelectedImage(model, "Add");
                }
                ((Area) model).add(area);
                trimShade(model);
            }
        }
    }

    public void includeNegativeShade(ToolsModel negativeShade, ToolsModel originalShade) {

        for (ToolsModel model : getShadeList()) {
            if (model.getID().toLowerCase().equals(originalShade.getID().toLowerCase())) {
                Area orgShade = new Area((Shape) originalShade);
                if (new Area((Shape) model).isEmpty()) {
                    VisualizerController.bottomController.updateShadeSelectedImage(model, "Add");
                }
                ((Area) model).add((Area) negativeShade);
                ((Area) model).intersect(orgShade);
                break;
            }
        }

    }

    public void dispose() {
        System.out.println("dispose");
        setCurrentBrush(null);
        setCurrentShape(null);
        getGraphics().dispose();
        g2d = null;
    }

    public ArrayList<ToolsModel> getShadeList() {
        return shadeList;
    }

    private void updateGroupingList() {
        getGroupingShadesList().stream().forEach((grouping) -> {
            grouping.getToolsStack().stream().forEach((model) -> {
                getShadeList().stream().filter((toolsModel) -> (toolsModel.getID().toLowerCase().equals(model.getID().toLowerCase()))).forEach((toolsModel) -> {
                    getShadeList().set(getShadeList().indexOf(toolsModel), model);
                });
            });
        });
    }

    public BufferedImage getImg() {
        return img;
    }

    public Graphics2D getG2d() {
        return g2d;
    }

    public GrayScaleManager getGrayScaleManager() {
        return grayScaleManager;
    }

    public PolygonDrawingCanvas getPolygonDrawingCanvas() {
        return polygonDrawingCanvas;
    }

    public void setPolygonDrawingCanvas(PolygonDrawingCanvas polygonDrawingCanvas) {
        this.polygonDrawingCanvas = polygonDrawingCanvas;
    }

    public ArrayList<EndPointRect> getTempRectList() {
        return tempRectList;
    }

    public Path2D getCurrentShape() {
        return currentShape;
    }

    public Path2D.Double getCurrentBrush() {
        return currentBrush;
    }

    public Point getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public Point getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Point endPoint) {
        this.endPoint = endPoint;
    }

    public Point getCurrentPoint() {
        return currentPoint;
    }

    public void setCurrentPoint(Point currentPoint) {
        this.currentPoint = currentPoint;
    }

    public void setCurrentShape(Path2D currentShape) {
        this.currentShape = currentShape;
    }

    public Robot getRobot() {
        return robot;
    }

    public boolean isEnableDragg() {
        return enableDragg;
    }

    public void setEnableDragg(boolean enableDragg) {
        this.enableDragg = enableDragg;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public Point getCurrentResizingRectPoint() {
        return currentResizingRectPoint;
    }

    public void setCurrentResizingRectPoint(Point currentResizingRectPoint) {
        this.currentResizingRectPoint = currentResizingRectPoint;
    }

    public Point getDragDelta() {
        return dragDelta;
    }

    public void setDragDelta(Point dragDelta) {
        this.dragDelta = dragDelta;
    }

    public BasicStroke getBrushStroke() {
        return new BasicStroke((float) ((double) 8 / Util.getScale()));
    }

    public void setCurrentBrush(Path2D.Double currentBrush) {
        this.currentBrush = currentBrush;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    private void checkSelectionAreaShades(Rectangle2D.Float makeRectangle) {
        boolean alreadySelected;
        for (ToolsModel model : getShadeList()) {
            if (((Shape) model).intersects(makeRectangle)) {
                alreadySelected = applyDefaultColorToSelectedShades(model);
                if (alreadySelected) {
                    continue;
                }
                Color c = model.getColor();
                model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), SELECTION_ALPHA));
                getTempSelectionList().add(model);
            }
        }
    }

    public void shadeCorrector(int oldval, int newVal) {

    }

    public ArrayList<ToolsModel> getTempSelectionList() {
        return tempSelectionList;
    }

    public void updateStroke(boolean zoomType) {
        float bStroke;
        float lStroke;
        int pad;
        System.out.println("Stroke width: " + getBrushStroke().getLineWidth());
//        if (zoomType) {
//            //Zoom in
//            bStroke = getBrushStroke().getLineWidth() / 1.1f;
//            lStroke = getLineStroke().getLineWidth() / 1.1f;
//            pad = DrawingCanvas.RECT_PAD - 1;
//        } else {
//            //Zoom out
//            bStroke = getBrushStroke().getLineWidth() * 1.1f;
//            lStroke = getLineStroke().getLineWidth() * 1.1f;
//            pad = DrawingCanvas.RECT_PAD + 1;
//        }

//        if(pad > 2 && pad < 7)
//            DrawingCanvas.RECT_PAD = pad;
        System.out.println("rect pad: " + DrawingCanvas.RECT_PAD);
//        this.setBrushStroke(new BasicStroke(bStroke));
//        this.setLineStroke(new BasicStroke(lStroke));
        this.getPolygonDrawingCanvas().repaint();
    }

    public BufferedImage getPreviewImage() {
        return editorImage;
    }

    public BasicStroke getLineStroke() {
        return new BasicStroke((float) ((double) 1 / Util.getScale()));

    }

    private void toogleSelectedShadeSelection(ToolsModel model) {

        int i = 0;
        ToolsModel deleteShade = null;
        for (ToolsModel selectedShade : getTempSelectionList()) {
            if (selectedShade.equals(model) || selectedShade.getID().equals(model.getID())) {
                Color c = model.getColor();
                model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
                deleteShade = model;
                System.out.println("UnSelected");
                break;
            }
            i++;
        }

        if (i == getTempSelectionList().size()) {
            Color c = model.getColor();
            model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), SELECTION_ALPHA));
            getTempSelectionList().add(model);
            System.out.println("Selected");
        } else if (deleteShade != null) {
            getTempSelectionList().remove(deleteShade);
            System.out.println("Deleted from selection list");
        }

    }

    private boolean applyDefaultColorToSelectedShades(ToolsModel model) {
        boolean alreadySelected = false;
        for (ToolsModel selectionModel : getTempSelectionList()) {
            if (selectionModel.equals(model) || selectionModel.getID().equals(model.getID())) {
                Color c = model.getColor();
                model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
                getTempSelectionList().remove(model);
                alreadySelected = true;
                break;
            }
        }

        return alreadySelected;
    }

    public void setSelectionShadeColorToDefault() {
        for (ToolsModel model : getTempSelectionList()) {
            Color c = model.getColor();
            model.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), DEFAULT_ALPHA));
        }
    }

    public void addAvailableShadesColorToColorListOnLoad() {
        shadeList.stream().forEach((model) -> {
            VisualizerController.bottomController.addShadeSelectedInImage(model.getColor(), model.getID());
        });
    }

    /// BEGIN PATCH

    /**
     * Set up rendering options
     *
     * @param g2
     */
    private static void setupRenderingOptions(final Graphics2D g2) {
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    }

    /**
     * Prepare gray scale image for the specified area
     *
     * @param image
     * @param area
     */
    private void renderGrayScaleImage(BufferedImage image, final Area area) {
        final Rectangle2D bounds = area.getBounds2D();
        final double offsetX = bounds.getMinX();
        final double offsetY = bounds.getMinY();
        final int width = image.getWidth();
        final int height = image.getHeight();
        {
            final Graphics2D g2 = image.createGraphics();

            g2.setComposite(AlphaComposite.Clear);
            g2.fillRect(0, 0, width, height);

            final AffineTransform tx = new AffineTransform();
            tx.translate(-offsetX, -offsetY);
            g2.setTransform(tx);

            g2.setComposite(AlphaComposite.Src);
            setupRenderingOptions(g2);
            g2.setColor(Color.WHITE);
            g2.fill(area);

            g2.setComposite(AlphaComposite.SrcAtop);
            g2.drawImage(grayScaleManager.getGrayImage(), null, 0, 0);

            g2.dispose();
        }
    }

    /**
     * Prepare selection image for the specified area
     *
     * @param image
     * @param area
     * @param shaders
     * @param color
     * @param strokeWidth
     */
    private void renderSelectionImage(BufferedImage image, final Area area, final List<ToolsModel> shaders, final Color color, final float strokeWidth) {
        final Rectangle2D bounds = area.getBounds2D();
        final double offsetX = bounds.getMinX();
        final double offsetY = bounds.getMinY();
        final int width = image.getWidth();
        final int height = image.getHeight();
        {
            final Graphics2D g2 = image.createGraphics();

            g2.setComposite(AlphaComposite.Clear);
            g2.fillRect(0, 0, width, height);

            final AffineTransform tx = new AffineTransform();
            tx.translate(-offsetX, -offsetY);
            g2.setTransform(tx);

            setupRenderingOptions(g2);
            g2.setStroke(new BasicStroke(strokeWidth));
            g2.setColor(color);

            shaders.forEach(shader -> {
                final Color shaderColor = shader.getColor();
                final Shape shaderShape = (Shape) shader;
                g2.setComposite(AlphaComposite.Clear);
                g2.fill(shaderShape);
                if (shaderColor.getAlpha() == SELECTION_ALPHA) {
                    g2.setComposite(AlphaComposite.Src);
                    g2.draw(shaderShape);
                }
            });

            g2.dispose();
        }
    }

    /**
     * Draw image
     *
     * @param g2
     * @param image
     * @param offsetX
     * @param offsetY
     */
    private void drawImageWithOffsets(final Graphics2D g2, final BufferedImage image, final double offsetX, final double offsetY) {
        final AffineTransform oldTx = g2.getTransform();
        final AffineTransform tx = new AffineTransform();
        tx.translate(offsetX, offsetY);
        g2.setTransform(tx);
        g2.drawImage(image, null, 0, 0);
        g2.setTransform(oldTx);
    }

    /**
     * Return true if shader list contains at least one selected shader
     *
     * @param shaders
     * @return true if shader list contains at least one selected shader
     */
    private boolean isThereAnySelectedShader(final List<ToolsModel> shaders) {
        return shaders.stream().anyMatch(shader -> {
            return shader.getColor().getAlpha() == SELECTION_ALPHA;
        });
    }

    /**
     * Renderer Cache (helper class)
     */
    private static class RendererCache {

        /**
         * Shader Info (helper class)
         */
        static class ShaderInfo {

            Area area;
            Color color;

            ShaderInfo(final Area area, final Color color) {
                this.area = area;
                this.color = color;
            }
        }

        /**
         * Luminescence Group Item (helper class)
         */
        static class LuminescenceGroupItem {

            final Area area;
            final Color color;

            LuminescenceGroupItem(final Area area, final Color color) {
                this.area = area;
                this.color = color;
            }
        }
        // Shader area (union of all shader areas from shaderAreas)
        Area shaderListArea = new Area();
        // Rendered gray scale image
        BufferedImage grayScaleImage;
        // Rendered selection image
        BufferedImage selectionImage;
        // Luminescence group map (for each luminescence group we keep all items with different color and area)
        // Order is important: from dark to light colors
        Map<Integer, List<LuminescenceGroupItem>> luminescenceGroupMap;
        // Area changed flag 
        boolean areaChanged = false;
        // Color changed flag
        boolean colorChanged = false;
        // Shader info map
        Map<ToolsModel, ShaderInfo> shaderMap = new HashMap<>();

        /**
         * Detect and apply changes
         *
         * @param gc
         * @param shaders
         */
        void detectAndApplyChanges(final GraphicsConfiguration gc, final List<ToolsModel> shaders) {
            System.out.println("Detecting changes in the cache since the last update...");
            areaChanged = false;
            colorChanged = false;
            final Set<ToolsModel> oldShaderSet = new HashSet<>(shaderMap.keySet());
            final Set<ToolsModel> newShaderSet = new HashSet<>(shaders);
            // process deleted shaders
            {
                final Set<ToolsModel> deletedShaderSet = new HashSet<>(oldShaderSet);
                deletedShaderSet.removeAll(newShaderSet);
                deletedShaderSet.forEach(shader -> {
                    areaChanged = colorChanged = true;
                    final ShaderInfo shaderInfo = shaderMap.get(shader);
                    final Area shaderArea = shaderInfo.area;
                    shaderListArea.subtract(shaderArea);
                    shaderMap.remove(shader);
                });
            }
            // process added shaders
            {
                final Set<ToolsModel> addedShaderSet = new HashSet<>(newShaderSet);
                addedShaderSet.removeAll(oldShaderSet);
                addedShaderSet.forEach(shader -> {
                    areaChanged = colorChanged = true;
                    final Color shaderColor = shader.getColor();
                    final Shape shaderShape = (Shape) shader;
                    final Area shaderArea = new Area(shaderShape);
                    final ShaderInfo shaderInfo = new ShaderInfo(shaderArea, shaderColor);
                    shaderListArea.add(shaderArea);
                    shaderMap.put(shader, shaderInfo);
                });
            }
            // process existing shaders
            {
                // existingShaderSet contains intersection of oldShaderSet and newShaderSet
                final Set<ToolsModel> existingShaderSet = new HashSet<>(newShaderSet);
                existingShaderSet.retainAll(oldShaderSet);
                existingShaderSet.forEach(shader -> {
                    final ShaderInfo info = shaderMap.get(shader);
                    final Area oldShaderArea = info.area;
                    final Area newShaderArea = (Area) shader;
                    if (!oldShaderArea.equals(newShaderArea)) {
                        shaderListArea.subtract(oldShaderArea);
                        shaderListArea.add(info.area = new Area((Shape) shader));
                        areaChanged = true;
                    }
                    final Color oldShaderColor = info.color;
                    final Color newShaderColor = shader.getColor();
                    if (!oldShaderColor.equals(newShaderColor)) {
                        info.color = newShaderColor;
                        colorChanged = true;
                    }
                });
            }
            if (areaChanged) {
                System.out.println("Area has been changed since the last cache update");
                final Rectangle2D bounds2D = shaderListArea.getBounds2D();
                final int newWidth = (int) (bounds2D.getWidth() + 0.5);
                final int newHeight = (int) (bounds2D.getHeight() + 0.5);
                grayScaleImage = gc.createCompatibleImage(newWidth, newHeight, Transparency.TRANSLUCENT);
                selectionImage = gc.createCompatibleImage(newWidth, newHeight, Transparency.TRANSLUCENT);
            }
            if (colorChanged) {
                System.out.println("Color has been changed since the last cache update");
            }
            if (areaChanged || colorChanged) {
                luminescenceGroupMap = new TreeMap<>();
                final Map<Integer, Integer> colorMap = new TreeMap<>();
                for (int i = 0; i < shaders.size(); i++) {
                    final ToolsModel shader = shaders.get(i);
                    final Color shaderColor = shader.getColor();
                    if (!colorMap.containsKey(shaderColor.getRGB())) {
                        colorMap.put(shaderColor.getRGB(), i);
                    }
                }
                colorMap.forEach((groupRGB, groupShaderListStartIndex) -> {
                    final Color groupColor = new Color(groupRGB, true);
                    final Area groupArea = new Area();
                    for (int i = groupShaderListStartIndex; i < shaders.size(); i++) {
                        final ToolsModel shader = shaders.get(i);
                        final Color shaderColor = shader.getColor();
                        final ShaderInfo shaderInfo = shaderMap.get(shader);
                        if (groupColor.equals(shaderColor)) {
                            groupArea.add(shaderInfo.area);
                        } else {
                            groupArea.subtract(shaderInfo.area);
                        }
                    }
                    if (!groupArea.isEmpty()) {
                        final Integer luminescence = calcLuminescence(groupColor);
                        List<LuminescenceGroupItem> list = luminescenceGroupMap.get(luminescence);
                        if (list == null) {
                            luminescenceGroupMap.put(luminescence, list = new ArrayList<>());
                        }
                        list.add(new LuminescenceGroupItem(groupArea, groupColor));
                    }
                });
            }
        }

        /**
         * Calculate the c color luminescence
         *
         * @param c - color
         * @return color luminescence
         */
        static int calcLuminescence(final Color c) {
            return (int) Math.sqrt(
                    c.getRed() * c.getRed() * 0.299
                    + c.getGreen() * c.getGreen() * 0.587
                    + c.getBlue() * c.getBlue() * 0.114);
        }
    }
    // renderer cache
    private final RendererCache rendererCache = new RendererCache();

    /**
     * Paint shader list (new version)
     *
     * @param g
     * @param shaders
     */
    private void paintShaderListNew(final Graphics g, final List<ToolsModel> shaders) {
        System.out.println("Painting shader list...");
        if (shaders == null || shaders.isEmpty()) {
            System.out.println("Shader list is empty...");
            return;
        }
        System.out.println("" + shaders.size());

        final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        final GraphicsDevice gs = ge.getDefaultScreenDevice();
        final GraphicsConfiguration gc = gs.getDefaultConfiguration();

        rendererCache.detectAndApplyChanges(gc, shaders);

        final Rectangle2D bounds = rendererCache.shaderListArea.getBounds2D();
        final double offsetX = bounds.getMinX();
        final double offsetY = bounds.getMinY();

        final Graphics2D g2 = (Graphics2D) g;
        if (rendererCache.areaChanged) {
            System.out.println("Rendering grayscale image...");
            renderGrayScaleImage(rendererCache.grayScaleImage, rendererCache.shaderListArea);
        }
        System.out.println("Drawing gray scale image...");
        drawImageWithOffsets(g2, rendererCache.grayScaleImage, offsetX, offsetY);

        System.out.println("Drawing shader list...");
        {
            // painting shader groups in order from dark to light
            setupRenderingOptions(g2);
            rendererCache.luminescenceGroupMap.forEach((luminescence, list) -> {
                list.forEach(item -> {
                    g2.setColor(item.color);
                    g2.fill(item.area);
                });
            });
        }

        if (isThereAnySelectedShader(shaders)) {
            if (rendererCache.areaChanged || rendererCache.colorChanged) {
                System.out.println("Rendering selection image...");
                renderSelectionImage(rendererCache.selectionImage, rendererCache.shaderListArea, shaders, Color.red.brighter().brighter().brighter(), 1.0f);
            }
            System.out.println("Drawing selection image...");
            drawImageWithOffsets(g2, rendererCache.selectionImage, offsetX, offsetY);
        }
    }
    /// END PATCH

}
