/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.awt.Color;
import java.util.HashMap;

/**
 *
 * @author DELL-PC
 */
public class DeletedShade implements ToolsModel{

    private HashMap<ToolsModel, Integer> toolsList;
    private boolean visible;

    public DeletedShade(HashMap<ToolsModel, Integer> toolsList) {
        this.toolsList = toolsList;
        visible = true;
    }    
    
    @Override
    public void setColor(Color color) {
    }

    @Override
    public Color getColor() {
        return Color.white;
    }

    @Override
    public void setVisible(boolean b) {
        this.visible = b;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    public HashMap<ToolsModel, Integer> getToolsList() {
        return toolsList;
    }

    public void setToolsList(HashMap<ToolsModel, Integer> toolsList) {
        this.toolsList = toolsList;
    }

    @Override
    public void setID(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getID() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ToolsModel getShape() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
