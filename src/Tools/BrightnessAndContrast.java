/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import Utility.Utility;
import java.awt.image.BufferedImage;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;

/**
 *
 * @author DELL-PC
 */
public class BrightnessAndContrast {

    private final BufferedImage workingImage;
    private BufferedImage finalImage;
    private final ImageView imageView;

    private final Brightness brightness;
    private final Contrast contrast;

    public BrightnessAndContrast(BufferedImage image, ImageView imageView) {
        this.imageView = imageView;
        brightness = new Brightness();
        contrast = new Contrast();

        workingImage = Utility.bufferedImageDeepCopy(image);
        finalImage = workingImage;
    }

    public void revalidateBrightness(Number oldValue, Number newValue) {
        if (oldValue.intValue() == newValue.intValue()) {
            return;
        }

        finalImage = getBrightness().changeBrightness(workingImage, newValue.intValue());
        imageView.setImage(SwingFXUtils.toFXImage(finalImage,null));
        
    }

    public void revalidateContrast(Number oldValue, Number newValue) {
        System.out.println("contrast");
        finalImage = getContrast().changeContrast(workingImage, newValue.floatValue());
        imageView.setImage(SwingFXUtils.toFXImage(finalImage,null));

    }

    public BufferedImage getImage() {
        return finalImage;
    }

    public Brightness getBrightness() {
        return brightness;
    }

    public Contrast getContrast() {
        return contrast;
    }

}
