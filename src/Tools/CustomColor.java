/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL-PC
 */
public class CustomColor extends Color implements ToolsModel {

    private Color color;
    private final ToolsModel toolsModel;
    private ArrayList<Color> groupColorList;
    private boolean visible;

    public CustomColor(Color c, ToolsModel toolsModel) {
        super(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
        this.color = new Color(toolsModel.getColor().getRed(),toolsModel.getColor().getGreen(),toolsModel.getColor().getBlue(),toolsModel.getColor().getAlpha());
        this.toolsModel = toolsModel;
        visible = true;
    }

    public CustomColor(Color c, Grouping grouping) {
        super(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
        this.toolsModel = (ToolsModel) grouping;
        groupColorList = new ArrayList<>();
        visible = true;
        addOldGroupColorToList(grouping);
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setVisible(boolean b) {
        visible = b;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public ToolsModel getShape() {
        return toolsModel;
    }

    public ArrayList<Color> getGroupColorList() {
        return groupColorList;
    }

    private void addOldGroupColorToList(Grouping grouping) {
        List<ToolsModel> groupingList = grouping.getToolsStack();

        for (ToolsModel model : groupingList) {
            System.out.println("group color: " + model.getColor());
            groupColorList.add(model.getColor());
        }
    }

    @Override
    public void setID(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getID() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ToolsModel getToolsModel() {
        return toolsModel;
    }

}
