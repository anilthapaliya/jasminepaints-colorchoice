/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Controller.sub.HeaderController.getVisualizerController;
import Main.Main;
import Tools.DrawingCanvas;
import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.swing.JDialog;

public class SaveImageController implements Initializable {

    @FXML
    private ComboBox categoryComboBox;
    @FXML
    private ComboBox workshopComboBox;
    @FXML
    private TextField ImageTextField;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;

    private ObservableList<String> category = FXCollections.observableArrayList();
    private ObservableList<String> workshop = FXCollections.observableArrayList();
    private String imageDirectory;
    private DrawingCanvas panel;
    private JDialog window;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        category.add("Interior");
        category.add("Exterior");
        categoryComboBox.setItems(category);
        workshopComboBox.setItems(workshop);
        categoryComboBox.valueProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            switch (newValue.toString()) {
                case "Interior":
                    File interior = new File(Util.getSaveInterior());
                    String[] directoriesInterior = interior.list((File current, String name)
                            -> new File(current, name).isDirectory());
                    imageDirectory = Util.getSaveInterior();
                    workshop.clear();
                    workshop.addAll(directoriesInterior);
                    break;
                case "Exterior":
                    File exterior = new File(Util.getSaveExterior());
                    String[] directoriesExterior = exterior.list((File current, String name)
                            -> new File(current, name).isDirectory());
                    imageDirectory = Util.getSaveExterior();
                    workshop.clear();
                    workshop.addAll(directoriesExterior);
                    break;
            }

        });
    }

    @FXML
    private void saveActionHandle(ActionEvent event) {
        Alert error = new Alert(Alert.AlertType.INFORMATION);
        DialogPane dialogPane = error.getDialogPane();
        dialogPane.getStylesheets().add(getClass().getResource("css/DarkTheme.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialog");
        if(categoryComboBox.getSelectionModel().getSelectedItem() != null) {
           if (!workshopComboBox.getEditor().getText().isEmpty()) {
               String workshopText = (workshopComboBox.getEditor().getText()).trim();
               if(ImageTextField.getText().length() > 15){
                   error.setContentText("The Image name must be within 15 characters.");
                   error.showAndWait();
               }
               else if (!ImageTextField.getText().isEmpty()) {
                 File imageWorkShop = new File(imageDirectory + "/" + workshopText);
                 if (!imageWorkShop.exists()) {
                     imageWorkShop.mkdir();
                 }
                 
                 try{
                     File f = new File(imageDirectory + "/" + workshopText + "/" + ImageTextField.getText());
                     System.out.println(imageDirectory + "/" + workshopText + "/" + ImageTextField.getText());
                     if(f.exists()){
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Confirmation");
                        alert.setContentText("File already exists. Do you want to overwrite the file?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.CANCEL) {
                        //GlobalValues.setImagePath(null);
                        //Util.setFilePath(null);
                            return;
                        }
                    else{
                        File imageToSave = new File(imageWorkShop + "/" + ImageTextField.getText() + "/");
                        //imageToSave.mkdir();
                         Util.setFilePath(imageToSave);
                        saveImage(imageToSave);
                    }
                 }
                 else{
                     System.out.println("Path Doesn't Exist.");
                     File imageToSave = new File(imageWorkShop + "/" + ImageTextField.getText() + "/");
                     imageToSave.mkdir();
                     Util.setFilePath(imageToSave);
                     saveImage(imageToSave);
                 }
                 
                }catch(Exception e){
                    e.printStackTrace();
                }
                } else {
                    error.setContentText("Please Set the name of Image");
                    error.showAndWait();
                }
            } else {
                Alert workshopError = new Alert(Alert.AlertType.WARNING);
                error.setContentText("Please select or give name of workshop");
                error.showAndWait();
            }
        } else {
            error.setContentText("Please select Category");
            error.showAndWait();
        }
    }
    
    private void saveImage(File imageToSave){
        Alert error = new Alert(Alert.AlertType.INFORMATION);
        DialogPane dialogPane = error.getDialogPane();
        if (imageToSave.exists()) {
            panel.saveObject();
            Util.setFilePath(null);
            //GlobalValues.setImagePath(null);
            error.setContentText("Image Saved Successfully: " + ImageTextField.getText());
            Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(1), (ActionEvent event1) -> {
            error.setResult(ButtonType.YES);
            error.hide();
            ((Stage) ImageTextField.getScene().getWindow()).close();
            }));
            idlestage.setCycleCount(1);
            idlestage.play();
            error.showAndWait();
            
            if(GlobalValues.isBackActive()){
            int val = GlobalValues.getCameFrom();
                    if(val == 0){
                        loadWindow("/Controller/SampleSelection.fxml", "Sample Images", 0);
                    }
                    else if(val == 1){
                        loadWindow("/Controller/ImageSelection.fxml", "Saved Images", 0);
                    }
                    else{
                        //loadWindow("/Controller/welcome.fxml", "Colour Visualizer - Jasmine Paints", 1);
                        loadHomeWindow();;
                    }
                    GlobalValues.setBackActive(false);
            }
        }
    }

    private void loadHomeWindow(){
        
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/welcome.fxml"));
            Parent root = (Parent) loader.load();
            WelcomeController controller = (WelcomeController) loader.getController();
            Image image = new Image("/Controller/images/bg.jpg");
            controller.imageView.setImage(image);

            controller.imageView.fitWidthProperty().bind(stage.widthProperty());
            controller.imageView.fitHeightProperty().bind(stage.heightProperty());
            
            Platform.runLater(()->{
                ObservableList<Screen> screens = Screen.getScreensForRectangle(new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight()));
                Rectangle2D bounds = screens.get(0).getVisualBounds();
                stage.setX(bounds.getMinX());
                stage.setX(bounds.getMinY());
                stage.setWidth(bounds.getWidth());
                stage.setHeight(bounds.getHeight());
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                controller.imageView.fitHeightProperty().bind(scene.heightProperty());
                stage.sizeToScene();
                stage.setMaximized(true);
                stage.setResizable(true);
            });
            
            stage.setTitle("Jasmine Paints - Color Choice");
            stage.show();
            stage.getIcons().add(new Image(Main.class.getResource("logo.png").toExternalForm()));
            getVisualizerController().dispose();
            stage.setOnCloseRequest((WindowEvent event) -> {
                System.exit(0);
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
    private void loadWindow(String resource, String title, int checkVal){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource(resource));
            Parent root = (Parent) loader.load();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setTitle(title);
            stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
            
            Platform.runLater(()->{
                ObservableList<Screen> screens = Screen.getScreensForRectangle(new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight()));
                Rectangle2D bounds = screens.get(0).getVisualBounds();
                stage.setX(bounds.getMinX());
                stage.setX(bounds.getMinY() - 10);
                stage.setWidth(bounds.getWidth());
                stage.setHeight(bounds.getHeight() - 10);              
                
                stage.setScene(scene);
                stage.sizeToScene();
                stage.setMaximized(true);
                stage.setResizable(true);
            });
            stage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {
                @Override
                    public void handle(WindowEvent window) {
                        getVisualizerController().dispose();
                    }
                });
            stage.show();
            //GlobalValues.setCameFrom(-1);  /* Reset value */
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    private void cancelButtonHandle(ActionEvent event) {
        ((Stage) ImageTextField.getScene().getWindow()).close();
    }

  
    public void setPanel(DrawingCanvas panel) {
        this.panel = panel;
    }
    
    public JDialog getWindow() {
        return window;
    }

    public void setWindow(JDialog window) {
        this.window = window;
    }
    
}
