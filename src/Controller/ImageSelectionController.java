/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Main;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * FXML Controller class
 *
 * @author dinesh
 */
public class ImageSelectionController implements Initializable {

    @FXML
    private BorderPane borderPane;
    @FXML
    private TreeView interiorTreeView;
    @FXML
    private TreeView exteriorTreeView;
    @FXML
    private Button btnDelFolder;
    @FXML
    private Button btnHelp;
    @FXML
    private Button backButton;
    @FXML
    private HBox hboxPanelContainer;
    @FXML
    private TabPane categoryTab;
    private String imagePath = "";
    private String directory = "";
    private final TilePane tile = new TilePane();
    
    boolean isFolderSelected = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        fillInteriorAndExterior();
        tile.setPadding(new Insets(15, 15, 15, 15));
        tile.setHgap(10);
        tile.setVgap(10);
        tile.setPrefColumns(4);
        tile.setStyle("-fx-background-color: rgba(255, 215, 0, 0.1);");
        
        /* Check for folder selection */
        categoryTab.setOnMouseClicked((event) -> {
            isFolderSelected = false;
        });
        interiorTreeView.setOnMouseClicked((event) -> {
                    isFolderSelected = true;
                    GlobalValues.setSelectedTab(0);
        });
        exteriorTreeView.setOnMouseClicked((event) -> {
                    isFolderSelected = true;
                    GlobalValues.setSelectedTab(1);
        });
        
        /* Set Image Selection */
        SingleSelectionModel tab = categoryTab.getSelectionModel();
        MultipleSelectionModel modelInt = interiorTreeView.getSelectionModel();
        MultipleSelectionModel modelExt = exteriorTreeView.getSelectionModel();
        tab.select(GlobalValues.getSelectedTab());
        
        if(GlobalValues.getSelectedTab() == 0)
            modelInt.select(GlobalValues.getSelectedIndex());
        else
            modelExt.select(GlobalValues.getSelectedIndex());

        updateItem();
        
        SingleSelectionModel<Tab> selectionModel = categoryTab.getSelectionModel();
        interiorTreeView.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            GlobalValues.setSelectedIndex(interiorTreeView.getSelectionModel().getSelectedIndex());
            imagePath = Util.getSaveInterior();
            TreeItem treeItem = (TreeItem) newValue;
            directory = "/" + treeItem.getValue();
            UpdateTilePane();
        });
        exteriorTreeView.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            GlobalValues.setSelectedIndex(exteriorTreeView.getSelectionModel().getSelectedIndex());
            imagePath = Util.getSaveExterior();
            TreeItem treeItem = (TreeItem) newValue;
            directory = "/" + treeItem.getValue();
            UpdateTilePane();
//            System.out.println("Exterior"+directory);
        });
        categoryTab.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                updateItem();
            }
        });
        
        ScrollPane scrollPane = new ScrollPane();
        hboxPanelContainer.getChildren().add(new ScrollPane(tile));
        backButton.setOnAction((ActionEvent event) -> {
            try {
                Stage stage = new Stage();
                ((Node) (event.getSource())).getScene().getWindow().hide();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/welcome.fxml"));
                Parent root = (Parent) loader.load();
                WelcomeController controller = (WelcomeController) loader.getController();
                Image image = new Image("/Controller/images/bg.jpg");
                controller.imageView.setImage(image);

                //controller.imageView.fitWidthProperty().bind(stage.widthProperty());
                //controller.imageView.fitHeightProperty().bind(stage.heightProperty());
                Scene scene = new Scene(root);
                stage.setScene(scene);
                
                Platform.runLater(()->{
                    ObservableList<Screen> screens = Screen.getScreensForRectangle(new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight()));
                    Rectangle2D bounds = screens.get(0).getVisualBounds();
                    stage.setX(bounds.getMinX());
                    stage.setX(bounds.getMinY() - 10);
                    stage.setWidth(bounds.getWidth());
                    stage.setHeight(bounds.getHeight() - 10);
                
                    stage.setScene(scene);
                    controller.imageView.fitHeightProperty().bind(scene.heightProperty());
                    controller.imageView.fitWidthProperty().bind(scene.widthProperty());
                    stage.sizeToScene();
                    stage.setMaximized(true);
                    stage.setResizable(true);
                });
                
                stage.setTitle("Jasmine Paints - Color Choice");
                stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
                stage.show();
                stage.setOnCloseRequest((javafx.stage.WindowEvent event1) -> {
                    System.exit(0);
                });

            } catch (Exception ex) {
                Logger.getLogger(ImageSelectionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        showHelp();
    }
    
    public void showHelp(){
        
        btnHelp.setOnAction((ActionEvent event) -> {
            try{
                Stage currStage = (Stage) btnHelp.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/WebViewHelp.fxml"));
                Parent root = (Parent) loader.load();
                WebViewHelpController controller = (WebViewHelpController) loader.getController();
                
                Stage newStage = new Stage();
                Scene scene = new Scene(root);
                newStage.getIcons().add(new Image(Main.class.getResource("logo.png").toExternalForm()));
                newStage.initModality(Modality.APPLICATION_MODAL);
                newStage.initOwner(currStage);
                newStage.setScene(scene);            
                newStage.setTitle("Help");
                newStage.setResizable(false);
                newStage.setFullScreen(false);
                newStage.centerOnScreen();
                newStage.show();
            }
            catch(Exception e){ }
        });
        
    }
    
    public void fillInteriorAndExterior(){
        
        //Add Item in Interior Tree
        imagePath = Util.getSaveInterior();
        File interior = new File(Util.getSaveInterior());
        String[] directoriesInterior = interior.list((File current, String name)
                -> new File(current, name).isDirectory());
        TreeItem<String> rootInterior = new TreeItem<>("Interior");
        rootInterior.setExpanded(true);
        for (String d1 : directoriesInterior) {
            TreeItem<String> item = new TreeItem<>(d1);
            rootInterior.getChildren().add(item);
        }
        interiorTreeView.setRoot(rootInterior);
        //Add Item in exterior Tree Item
        File exterior = new File(Util.getSaveExterior());
        String[] directoriesExterior = exterior.list((File current, String name)
                -> new File(current, name).isDirectory());
        TreeItem<String> rootExterior = new TreeItem<>("Exterior");
        rootExterior.setExpanded(true);
        for (String d1 : directoriesExterior) {
            TreeItem<String> item = new TreeItem<>(d1);
            rootExterior.getChildren().add(item);
        }
        exteriorTreeView.setRoot(rootExterior);
        
    }

    public void UpdateTilePane() {
        
        File folder = new File(imagePath + directory);
        File[] directories = folder.listFiles(File::isDirectory);
        if (directories != null) {
            tile.getChildren().clear();
            for (File d : directories) {
                String originalPath = d + "/thumbnail.jpg";
                File f = new File(originalPath);
                if(f.exists())
                    tile.getChildren().addAll(createImageView(new File(originalPath), d.getName()));
            }
        }

    }

    private VBox createImageView(final File imageFile, String title) {
        try {
            VBox vBox = new VBox();
            AnchorPane anchorpane = new AnchorPane();
            Label caption = new Label();
            BufferedImage thumbnail = ImageIO.read(imageFile);
            ImageView imageView = new ImageView(SwingFXUtils.toFXImage(thumbnail, null));
            imageView.setFitWidth(150);
            imageView.setPreserveRatio(true);
            thumbnail.flush();
            imageView.setStyle("-fx-border-width:4;-fx-border-color:red green yellow blue;-fx-effect: dropshadow( one-pass-box , black , 8 , 0.0 , 2 , 0 );");
            final ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(100), imageView);
            scaleTransition.setToX(1.2f);
            scaleTransition.setToY(1.2f);
            final ScaleTransition scaleTransitionAnti = new ScaleTransition(Duration.millis(100), imageView);
            scaleTransitionAnti.setToX(1f);
            scaleTransitionAnti.setToY(1f);
            vBox.setId(imageFile.getParentFile().getName());
            imageView.setId(imageFile.getParentFile().getName());
            imageView.setOnMouseClicked((MouseEvent event) -> {
                System.out.println(imageFile.getParentFile().getParent() + "/" + imageView.getId() + "/Default.jpg");
                try {
                    BufferedImage bufferedImage = ImageIO.read(new File(imageFile.getParentFile().getParent() + "/" + imageView.getId() + "/Default.jpg"));
                    Util.setImg(bufferedImage);
                    Util.setFilePath(imageFile.getParentFile());
                    GlobalValues.setImagePath(imageFile.getParentFile());
                    bufferedImage.flush();

                    VisualizerController frame = new VisualizerController();
                    frame.setTitle("Jasmine Paints - Color Choice");
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.setIconImage(new ImageIcon(getClass().getResource("/Controller/images/logo.png")).getImage());
                    frame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowOpened(java.awt.event.WindowEvent e) {
                            Platform.runLater(() -> {
                                (((Node) event.getSource()).getScene().getWindow()).hide();
                            });
                        }

                    });
                    frame.setVisible(true);
                    frame.pack();
                    frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);

                } catch (IOException | HeadlessException e) {
                    Logger.getLogger(ImageSelectionController.class.getName()).log(Level.SEVERE, null, e);
                }
            });
            imageView.setOnMouseEntered((MouseEvent event) -> {
                scaleTransition.play();
                scaleTransitionAnti.stop();
            });
            imageView.setOnMouseExited((MouseEvent event) -> {
                scaleTransition.stop();
                scaleTransitionAnti.play();
            });
            Hyperlink btnDelete = new Hyperlink();
            btnDelete.setText("Delete");
            
            btnDelete.setStyle("-fx-base-color:red;-fx-outline:none");

            btnDelete.setOnAction((ActionEvent event) -> {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Confirmation Dialog");
                alert.setContentText("Are you sure want to delete?");

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    File folder = new File(imagePath + directory + "\\" + caption.getText());
                    Util.deleteFolder(folder);
                    updateItem();
                } else {

                }
            });
            
            anchorpane.getChildren().addAll(caption, btnDelete);
            AnchorPane.setLeftAnchor(caption, 0.0);
            AnchorPane.setRightAnchor(btnDelete, 0.0);

            caption.setText(title);
            caption.setTextAlignment(TextAlignment.CENTER);
            caption.setFont(new Font(15));
            vBox.getChildren().addAll(imageView, anchorpane);
            
            btnDelFolder.setOnAction((event) -> {
                if(isFolderSelected){
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Confirmation");
                alert.setHeaderText("");
                String dirName = directory.substring(1, directory.length());
                alert.setContentText("Are you sure want to delete the selected folder and the containing items?\n"
                        + "Selected Folder: " + dirName);
                Stage stage = (Stage) btnDelFolder.getScene().getWindow();
                stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toString()));
                
                ButtonType btnYes = new ButtonType("Yes");
                ButtonType btnNo = new ButtonType("No");
                alert.getButtonTypes().setAll(btnYes, btnNo);
                
                Optional<ButtonType> result = alert.showAndWait();
                if(result.get() == btnYes){
                    deleteFolder(new File(imagePath + directory));
                    fillInteriorAndExterior();
                    tile.getChildren().clear();
                    UpdateTilePane();
                }
                else{
                    alert.close();
                }
                }
                else{
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("");
                    alert.setContentText("Please select a folder.");
                    alert.showAndWait();
                }
                
            });

            return vBox;
        } catch (Exception ex) {
            Logger.getLogger(ImageSelectionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void deleteFolder(File file){
        
        if(file == null)
            return;
        
        File[] contents = file.listFiles();
        if(contents != null){
            for(File f : contents){
                deleteFolder(f);
            }
        }
        file.delete();
        
    }

    public void updateItem() {
        switch (categoryTab.getSelectionModel().getSelectedIndex()) {
            case 0:
                imagePath = Util.getSaveInterior();
                if (interiorTreeView.getSelectionModel().getSelectedItem() != null) {
                    TreeItem treeItem = (TreeItem) (interiorTreeView.getSelectionModel().getSelectedItem());
                    directory = "/" + treeItem.getValue();
                    UpdateTilePane();
                }
                break;
            case 1:
                imagePath = Util.getSaveExterior();
                if (exteriorTreeView.getSelectionModel().getSelectedItem() != null) {
                    TreeItem treeItem1 = (TreeItem) (exteriorTreeView.getSelectionModel().getSelectedItem());
                    directory = "/" + treeItem1.getValue();
                    UpdateTilePane();
                }
                break;
        }
    }
}
