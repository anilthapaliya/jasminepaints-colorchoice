package Controller;

import Main.Main;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import org.apache.commons.io.FileUtils;

/**
 * FXML Controller class
 *
 * @author medinesh
 */
public class SampleImageController implements Initializable {

    @FXML
    private BorderPane borderPane;
    @FXML
    private TreeView interiorTreeView;
    @FXML
    private TreeView exteriorTreeView;
    @FXML
    private Button backButton;
    @FXML
    private Button btnHelp;
    @FXML
    private HBox hboxPanelContainer;
    @FXML
    private TabPane categoryTab;
    private String imagePath = "";
    private String directory = "";
    private final TilePane tile = new TilePane();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        imagePath = "data/jasminepaint/Interior";
        extractFile();
        File interior = new File(imagePath);
        String[] directoriesInterior = interior.list((File current, String name)
                -> new File(current, name).isDirectory());
        TreeItem<String> rootInterior = new TreeItem<>("Interior");
        rootInterior.setExpanded(true);
        for (String d1 : directoriesInterior) {
            TreeItem<String> item = new TreeItem<>(d1);
            rootInterior.getChildren().add(item);
        }
        interiorTreeView.setRoot(rootInterior);
        
        imagePath = "data/jasminepaint/Exterior";
        extractFile();
        File exterior = new File(imagePath);
        String[] directoriesExterior = exterior.list((File current, String name)
                -> new File(current, name).isDirectory());
        TreeItem<String> rootExterior = new TreeItem<>("Exterior");
        rootExterior.setExpanded(true);
        for (String d1 : directoriesExterior) {
            TreeItem<String> item = new TreeItem<>(d1);
            rootExterior.getChildren().add(item);
        }
        exteriorTreeView.setRoot(rootExterior);
        
        interiorTreeView.setOnMouseClicked((event) -> {
                    GlobalValues.setSelectedTab(0);
        });
        exteriorTreeView.setOnMouseClicked((event) -> {
                    GlobalValues.setSelectedTab(1);
        });
        
        /* Set Image Selection */
        SingleSelectionModel tab = categoryTab.getSelectionModel();
        MultipleSelectionModel modelInt = interiorTreeView.getSelectionModel();
        MultipleSelectionModel modelExt = exteriorTreeView.getSelectionModel();
        tab.select(GlobalValues.getSelectedTab());
        
        if(GlobalValues.getSelectedTab() == 0){
            imagePath = "data/jasminepaint/Interior";
            directory = GlobalValues.getDirectory();
            if(GlobalValues.getSelectedIndex() == 0)
                directory = "/Bathroom";
            modelInt.select(GlobalValues.getSelectedIndex());
        }
        else{
            imagePath = "data/jasminepaint/Exterior";
            directory = GlobalValues.getDirectory();
            modelExt.select(GlobalValues.getSelectedIndex());
        }

        UpdateTilePane();
        
        tile.setPadding(new Insets(15, 15, 15, 15));
        tile.setHgap(10);
        tile.setVgap(10);
        tile.setPrefColumns(4);
        tile.setStyle("-fx-background-color: rgba(255, 215, 0, 0.1);");
        SingleSelectionModel<Tab> selectionModel = categoryTab.getSelectionModel();
        interiorTreeView.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            GlobalValues.setSelectedIndex(interiorTreeView.getSelectionModel().getSelectedIndex());
            imagePath = "data/jasminepaint/Interior";
            TreeItem treeItem = (TreeItem) newValue;
            directory = "/" + treeItem.getValue();
            GlobalValues.setDirectory(directory);
            UpdateTilePane();

        });
        exteriorTreeView.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            GlobalValues.setSelectedIndex(exteriorTreeView.getSelectionModel().getSelectedIndex());
            imagePath = "data/jasminepaint/Exterior";
            TreeItem treeItem = (TreeItem) newValue;
            directory = "/" + treeItem.getValue();
            GlobalValues.setDirectory(directory);
            UpdateTilePane();
        });
        categoryTab.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                switch (categoryTab.getSelectionModel().getSelectedIndex()) {
                    case 0:
                        imagePath = "data/jasminepaint/Interior";
                        if (interiorTreeView.getSelectionModel().getSelectedItem() != null) {
                            TreeItem treeItem = (TreeItem) (interiorTreeView.getSelectionModel().getSelectedItem());
                            directory = "/" + treeItem.getValue();
                            UpdateTilePane();
                            System.out.println("Interior" + directory);
                        }
                        break;
                    case 1:
                        imagePath = "data/jasminepaint/Exterior";
                        if (exteriorTreeView.getSelectionModel().getSelectedItem() != null) {
                            TreeItem treeItem1 = (TreeItem) (exteriorTreeView.getSelectionModel().getSelectedItem());
                            directory = "/" + treeItem1.getValue();
                            UpdateTilePane();
                        }
                        break;
                }
            }
        });
        
        ScrollPane scrollPane = new ScrollPane();
        hboxPanelContainer.getChildren().add(new ScrollPane(tile));
        backButton.setOnAction((ActionEvent event) -> {
            try {
                ((Node) (event.getSource())).getScene().getWindow().hide();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/welcome.fxml"));
                Parent root = (Parent) loader.load();
                WelcomeController controller = (WelcomeController) loader.getController();
                Image image = new Image("/Controller/images/bg.jpg");
                controller.imageView.setImage(image);
                
                Stage stage = new Stage();
                Scene scene = new Scene(root);
                Platform.runLater(()->{
                    ObservableList<Screen> screens = Screen.getScreensForRectangle(new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight()));
                    Rectangle2D bounds = screens.get(0).getVisualBounds();
                    stage.setX(bounds.getMinX());
                    stage.setX(bounds.getMinY() - 10);
                    stage.setWidth(bounds.getWidth());
                    stage.setHeight(bounds.getHeight() - 10);
                    
                    stage.setScene(scene);
                    controller.imageView.fitHeightProperty().bind(scene.heightProperty());
                    controller.imageView.fitWidthProperty().bind(scene.widthProperty());
                    stage.sizeToScene();
                    stage.setMaximized(true);
                    stage.setResizable(true);
                });
                stage.setTitle("Jasmine Paints - Color Choice");
                stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
                stage.setOnCloseRequest((WindowEvent event1) -> {
                    System.exit(0);
                });
                stage.show();
            } catch (Exception ex) {
                Logger.getLogger(SampleImageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        showHelp();
        
    }

    public void extractFile() {
        if (!new File(imagePath).exists()) {
            try {
                URL input = getClass().getResource("lib.zip");
                File dest = new File("data/lib.zip");
                FileUtils.copyURLToFile(input, dest);
                UnzipUtility unzipFile = new UnzipUtility();
                unzipFile.unzip(dest.getPath(), dest.getParentFile().getPath());
            } catch (IOException ex) {
                Logger.getLogger(SampleImageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void showHelp(){
        
        btnHelp.setOnAction((ActionEvent event) -> {
            try{
                Stage currStage = (Stage) btnHelp.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/WebViewHelp.fxml"));
                Parent root = (Parent) loader.load();
                WebViewHelpController controller = (WebViewHelpController) loader.getController();
                
                Stage newStage = new Stage();
                Scene scene = new Scene(root);
                newStage.getIcons().add(new Image(Main.class.getResource("logo.png").toExternalForm()));
                newStage.initModality(Modality.APPLICATION_MODAL);
                newStage.initOwner(currStage);
                newStage.setScene(scene);            
                newStage.setTitle("Help");
                newStage.setResizable(false);
                newStage.setFullScreen(false);
                newStage.centerOnScreen();
                newStage.show();
            }
            catch(Exception e){ }
        });
        
    }

    public void UpdateTilePane() {
        File folder = new File(imagePath + directory);
        File[] directories = folder.listFiles(File::isDirectory);
        if (directories != null) {
            tile.getChildren().clear();
            for (File d : directories) {
                
                String originalPath = d + "/thumbnail.jpg";
                
                tile.getChildren().addAll(createImageView(new File(originalPath),d.getName()));
            }
        }

    }

    private VBox createImageView(final File imageFile, String title) {
        try {
            VBox vBox = new VBox();
           Label caption = new Label(); 
            javafx.scene.image.Image image = new javafx.scene.image.Image(new FileInputStream(imageFile), 150, 0, true, true);
            ImageView imageView = new ImageView(image);
            imageView.setStyle("-fx-border-width:4;-fx-border-color:red green yellow blue;-fx-effect: dropshadow( one-pass-box , black , 8 , 0.0 , 2 , 0 );");
//            imageView.setFitHeight(150);
//            imageView.setFitWidth(150);
            imageView.setSmooth(true);
           
            final ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(100), imageView);
            scaleTransition.setToX(1.2f);
            scaleTransition.setToY(1.2f);
            final ScaleTransition scaleTransitionAnti = new ScaleTransition(Duration.millis(100), imageView);
            scaleTransitionAnti.setToX(1f);
            scaleTransitionAnti.setToY(1f);
            
            imageView.setId(imageFile.getParentFile().getName());
            imageView.setOnMouseClicked((MouseEvent event) -> {
                System.out.println(imageFile.getParentFile().getParent() + "/" + imageView.getId() + "/Default.jpg");
                try {
                    BufferedImage bufferedImage = ImageIO.read(new File(imageFile.getParentFile().getParent() + "/" + imageView.getId() + "/Default.jpg"));
                    Util.setImg(bufferedImage);
                    Util.setFilePath(imageFile.getParentFile());
                    
                    /* Changed Code */
                    GlobalValues.setImagePath(imageFile.getParentFile());
                    
                    bufferedImage.flush();
                    VisualizerController frame = new VisualizerController();
                    frame.setTitle("Jasmine Paints - Color Choice");
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    frame.setIconImage(new ImageIcon(getClass().getResource("/Controller/images/logo.png")).getImage());
                    frame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowOpened(java.awt.event.WindowEvent e) {
                            Platform.runLater(() -> {
                                (((Node) event.getSource()).getScene().getWindow()).hide();
                            });
                        }
                    });
                    frame.setVisible(true);
                    frame.pack();
                    frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
                } catch (IOException | HeadlessException e) {
                    Logger.getLogger(SampleImageController.class.getName()).log(Level.SEVERE, null, e);
                }
            });
            vBox.setOnMouseEntered((MouseEvent event) -> {
                scaleTransition.play();
                scaleTransitionAnti.stop();
                caption.setVisible(false);
            });
            vBox.setOnMouseExited((MouseEvent event) -> {
                scaleTransition.stop();
                scaleTransitionAnti.play();
                caption.setVisible(true);
            });
            
            //caption.setText(title);
            caption.setTextAlignment(TextAlignment.CENTER);
            caption.setFont(new Font(15));
            
            vBox.getChildren().addAll(imageView, caption);
//            return imageView;
            return vBox;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SampleImageController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
