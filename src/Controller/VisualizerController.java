package Controller;

import Controller.sub.BottomController;
import Controller.sub.HeaderController;
import Controller.sub.LeftController;
import Controller.sub.RightController;
import Tools.CursorFactory;
import Tools.MainPane;
import Tools.RedoUndoManager;
import Tools.ShadeCorrector;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

public class VisualizerController extends JFrame {

    private JFXPanel headerPanel;
    private JFXPanel leftPanel;
    private JFXPanel rightPanel;
    private JFXPanel bottomPanel;

    public static RedoUndoManager redoUndoManager;
    public static ShadeCorrector shadeCorrector;
    public static MainPane panel;

    public static HeaderController headerController;
    public static LeftController leftController;
    public static RightController rightController;
    public static BottomController bottomController;
    public static JScrollPane scrollPane;
    public static CursorFactory cursorFactory;

    public VisualizerController() throws HeadlessException {
        try {
            headerPanel = new JFXPanel();
            leftPanel = new JFXPanel();
            rightPanel = new JFXPanel();
            bottomPanel = new JFXPanel();

            cursorFactory = new CursorFactory();
            panel = new MainPane(Util.getImg(), this);
            scrollPane = new JScrollPane(panel);
            scrollPane.setViewportBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
            scrollPane.getViewport().setBackground(Color.GRAY);
            scrollPane.setBackground(Color.GRAY);
            panel.setBackground(Color.GRAY);
            shadeCorrector = new ShadeCorrector(this);
            redoUndoManager = new RedoUndoManager(panel.getDrawingCanvas());
            panel.setBackground(Color.BLUE);

            add(headerPanel, BorderLayout.NORTH);
            add(leftPanel, BorderLayout.WEST);
            add(scrollPane, BorderLayout.CENTER);
            add(rightPanel, BorderLayout.EAST);
            add(bottomPanel, BorderLayout.SOUTH);

            HeaderController.setVisualizerController(this);
            LeftController.setVisualizerController(this);
            RightController.setVisualizerController(this);
            BottomController.setVisualizerController(this);

            // create JavaFX scene
            Platform.runLater(() -> {
                createScene();
                panel.getDrawingCanvas().addAvailableShadesColorToColorListOnLoad();
            });

        } catch (Exception ex) {
            Logger.getLogger(VisualizerController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void centerContent() {
        Rectangle bounds = scrollPane.getViewport().getViewRect();
        Dimension size = scrollPane.getViewport().getViewSize();

        int x = (size.width - bounds.width) / 2;
        int y = (size.height - bounds.height) / 2;
//        int w = (int) scrollPane.getViewport().getSize().getWidth() / 2;
//        int h = (int) scrollPane.getViewport().getSize().getHeight() / 2;

        System.out.println("Center Point is {" + x + "," + y + "}");
        this.scrollPane.getViewport().setViewPosition(new Point(x, y));
        scrollPane.updateUI();
        scrollPane.repaint();
    }

    private void createScene() {
        try {
            System.out.println("Started Loading ...");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/header.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            headerPanel.setScene(scene);
            headerController = fxmlLoader.<HeaderController>getController();
            
            fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/lefttoolbar.fxml"));
            root = (Parent) fxmlLoader.load();
            leftPanel.setScene(new Scene(root));
            leftController = fxmlLoader.<LeftController>getController();
            
            fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/rightcolorpanel.fxml"));
            root = (Parent) fxmlLoader.load();
            rightPanel.setScene(new Scene(root));
            rightController = fxmlLoader.<RightController>getController();
            
            fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/bottompanel.fxml"));
            root = (Parent) fxmlLoader.load();
            bottomPanel.setScene(new Scene(root));
            bottomController = fxmlLoader.<BottomController>getController();

            System.out.println("Finished  Loading ...");

        } catch (IOException ex) {
            Logger.getLogger(VisualizerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
