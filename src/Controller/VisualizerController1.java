//package Controller;
//
//import Database.FamilyModel;
//import Database.ShadesModel;
//import Report.PrintPreview;
//import Tools.CustomBrush;
//import Tools.ColorStyle;
//import Tools.CursorType;
//import Tools.CustomArea;
//import Tools.CustomColor;
//import Tools.DrawingCanvas;
//import Tools.Grouping;
//import Tools.MainPane;
//import Tools.RedoUndoManager;
//import Tools.ShadeCorrector;
//import Tools.ToolsModel;
//import java.awt.MouseInfo;
//import java.awt.PointerInfo;
//import java.awt.Robot;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.net.URL;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Optional;
//import java.util.ResourceBundle;
//import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javafx.animation.KeyFrame;
//import javafx.animation.ScaleTransition;
//import javafx.animation.Timeline;
//import javafx.application.Platform;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.embed.swing.SwingNode;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.fxml.FXMLLoader;
//import javafx.fxml.Initializable;
//import javafx.geometry.Bounds;
//import javafx.geometry.Insets;
//import javafx.geometry.Orientation;
//import javafx.scene.Node;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.scene.chart.PieChart;
//import javafx.scene.control.Alert;
//import javafx.scene.control.Button;
//import javafx.scene.control.ButtonType;
//import javafx.scene.control.ComboBox;
//import javafx.scene.control.Label;
//import javafx.scene.control.ListView;
//import javafx.scene.control.MenuItem;
//import javafx.scene.control.RadioButton;
//import javafx.scene.control.ScrollPane;
//import javafx.scene.control.Slider;
//import javafx.scene.control.Toggle;
//import javafx.scene.control.ToggleButton;
//import javafx.scene.control.ToggleGroup;
//import javafx.scene.image.Image;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.MouseEvent;
//import javafx.scene.layout.Background;
//import javafx.scene.layout.BackgroundFill;
//import javafx.scene.layout.Border;
//import javafx.scene.layout.BorderPane;
//import javafx.scene.layout.CornerRadii;
//import javafx.scene.layout.GridPane;
//import javafx.scene.layout.HBox;
//import javafx.scene.layout.Pane;
//import javafx.scene.layout.StackPane;
//import javafx.scene.layout.VBox;
//import javafx.scene.paint.Color;
//import javafx.scene.paint.Paint;
//import javafx.scene.shape.Rectangle;
//import javafx.scene.transform.Scale;
//import javafx.stage.Modality;
//import javafx.stage.Stage;
//import javafx.util.Duration;
//import javax.imageio.ImageIO;
//import tray.animations.AnimationType;
//import tray.notification.TrayNotification;
//
//public class VisualizerController1 implements Initializable {
//
//    private final ObservableList<Rectangle> recentUseColor = FXCollections.observableArrayList();
//    private final ObservableList<Rectangle> shadeUsedColor = FXCollections.observableArrayList();
//    private final Set<Color> recentUsedColorSet = new HashSet<>();
//    private final ObservableList<String> autoCompleteCombobox = FXCollections.observableArrayList();
//    private String description;
//    @FXML
//    private MenuItem saveDialog;
//    @FXML
//    private Button doneButton;
//    @FXML
//    private Button resetButton;
//    @FXML
//    private ToggleGroup group1;
//    @FXML
//    private ToggleButton polygonToolBtn;
//    public MainPane panel;
//    public ShadeCorrector shadeCorrector;
//    public Scene rootScene;
//    @FXML
//    private VBox colorPanel;
//    @FXML
//    private Label selectedShade;
//    @FXML
//    private ToggleButton brushToolButton;
//    @FXML
//    private ToggleButton negativeToolButton;
//    @FXML
//    private ToggleButton zoomOutButton;
//    @FXML
//    private ToggleButton zoomInButton;
//    @FXML
//    private HBox hboxPanelContainer;
//    @FXML
//    private Button backButton;
//
//    private final SwingNode panelContainer = new SwingNode();
//    private final ScrollPane scrollPane = new ScrollPane();
//    @FXML
//    private BorderPane borderPane;
//    @FXML
//    private ListView<Rectangle> recentUsedShade;
//    @FXML
//    private ListView<Rectangle> shadeUsedInImage;
//    @FXML
//    private MenuItem saveAsDialog;
//    @FXML
//    private MenuItem printImage;
//    @FXML
//    private Button deleteShadeButton;
//    @FXML
//    private Button groupBtn;
//    @FXML
//    private Button unGroupBtn;
//    @FXML
//    private Button fitOnScreen;
//
//    private String selectedDataInGrid = null;
//    int loop = 0;
//    @FXML
//    private RadioButton shadeName;
//    @FXML
//    private ToggleGroup group2;
//    @FXML
//    private RadioButton shadeCode;
//    @FXML
//    private ComboBox<String> cmbAutoComplete;
//    @FXML
//    private Button btnSearch;
//    @FXML
//    private Label lblName;
//    @FXML
//    private Label lblCode;
//    @FXML
//    private Label lblPageNo;
//    @FXML
//    private Label lblShadeIn;
//    private StackPane imageHolder;
//    @FXML
//    private Button undoBtn;
//    @FXML
//    private Button redoBtn;
//    public RedoUndoManager redoUndoManager;
//    private BufferedImage img;
//    @FXML
//    private Slider slider;
//    @FXML
//    private ToggleButton btnBlack;
//    @FXML
//    private ToggleGroup groupColor;
//    @FXML
//    private ToggleButton btnWhite;
//    @FXML
//    private ToggleButton btnRemoveColor;
//    @FXML
//    private ToggleGroup group3;
//    @FXML
//    private Button shadeCorrectorBtn;
//    @FXML
//    private Button shadeCorrectorDoneBtn;
//    int zoomCount = 5;
//    @FXML
//    private Button helpButton;
//
//    public VisualizerController1() {
//
//    }
//
//    @Override
//    public void initialize(URL url, ResourceBundle rb) {
//        try {
//            img = Util.getImg();
//            disableDoneResetButton(true);
//            undoBtn.setDisable(true);
//            redoBtn.setDisable(true);
//
////            drawColorPanel();
//            zoomInButton.setOnAction((ActionEvent event) -> {
//                zoomInActionHandle();
//            });
//            zoomOutButton.setOnAction((ActionEvent event) -> {
//                zoomOutActionHandle();
//            });
//            backButton.setOnAction((ActionEvent event) -> {
//                try {
//                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//                    alert.setTitle("Confirmation Dialog");
//                    alert.setContentText("Do you want to save Any changes made?");
//
//                    Optional<ButtonType> result = alert.showAndWait();
//                    if (result.get() == ButtonType.OK) {
//                        onSaveImage(null);
//                    }
////                    panel.getDrawingCanvas().dispose();
////                    panel = null;
//                    ((Stage) panelContainer.getScene().getWindow()).close();
//                    Parent root = (Parent) FXMLLoader.load(getClass().getResource("/Controller/welcome.fxml"));
//                    Stage stage = new Stage();
//                    Scene scene = new Scene(root);
//                    stage.setScene(scene);
//                    stage.setTitle("Color Visualizer -Jasmine Paints!");
//                    stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
//                    stage.setResizable(false);
//                    stage.setMaximized(true);
//                    stage.show();
//                } catch (Exception ex) {
//                    Logger.getLogger(VisualizerController.class
//                            .getName()).log(Level.SEVERE, null, ex);
//                }
//            });
//            recentUsedShade.setItems(recentUseColor);
//            recentUsedShade.setOrientation(Orientation.HORIZONTAL);
//            recentUsedShade.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Rectangle> observable, Rectangle oldValue, Rectangle newValue) -> {
//                selectedShade.setBackground(new Background(new BackgroundFill(newValue.getFill(), CornerRadii.EMPTY, Insets.EMPTY)));
//            });
//            shadeUsedInImage.setItems(shadeUsedColor);
//            shadeUsedInImage.setOrientation(Orientation.HORIZONTAL);
//            shadeUsedInImage.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Rectangle> observable, Rectangle oldValue, Rectangle newValue) -> {
//                selectedShade.setBackground(new Background(new BackgroundFill(newValue.getFill(), CornerRadii.EMPTY, Insets.EMPTY)));
//            });
//            cmbAutoComplete.setItems(autoCompleteCombobox);
//            group2.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
//                cmbAutoComplete.getEditor().setText("");
//                cmbAutoComplete.getSelectionModel().clearSelection();
//                autoCompleteCombobox.clear();
//                List<ShadesModel> f = ShadesModel.findAll();
//                f.stream().forEach((f1) -> {
//                    autoCompleteCombobox.add(String.valueOf(shadeCode.isSelected() ? f1.code : f1.name));
//                });
//            });
//            btnSearch.setOnAction((ActionEvent e) -> {
//                if (group1.getSelectedToggle() != null) {
//                    group1.getSelectedToggle().setSelected(false);
//
//                }
//                toggleButtonPressed();
//                if (shadeCode.isSelected()) {
//                    if (!cmbAutoComplete.getEditor().getText().isEmpty()) {
//                        for (ShadesModel s : ShadesModel.shadesList) {
//                            if (s.code == null ? cmbAutoComplete.getEditor().getText() == null : s.code.equals(cmbAutoComplete.getEditor().getText())) {
//                                lblName.setText(s.name);
//                                lblCode.setText(String.valueOf(s.code));
//                                lblPageNo.setText(String.valueOf("Page No: " + s.page));
//                                lblShadeIn.setText(s.Na_in);
//                                description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
//                                Util.setShadeColor(new java.awt.Color(s.red, s.green, s.blue, DrawingCanvas.DEFAULT_ALPHA));
//                                selectedShade.setBackground(new Background(new BackgroundFill(awtToFXColor(new java.awt.Color(s.red, s.green, s.blue, 255)), CornerRadii.EMPTY, Insets.EMPTY)));
//                                break;
//                            }
//                        }
//                    }
//                } else if (shadeName.isSelected()) {
//                    if (!cmbAutoComplete.getEditor().getText().isEmpty()) {
//                        for (ShadesModel s : ShadesModel.shadesList) {
//                            if (s.name == null ? cmbAutoComplete.getEditor().getText() == null : s.name.equals(cmbAutoComplete.getEditor().getText())) {
//                                System.out.println("I Came Here");
//                                lblName.setText(s.name);
//                                lblCode.setText(String.valueOf(s.code));
//                                lblPageNo.setText(String.valueOf("Page No: " + s.page));
//                                lblShadeIn.setText(s.Na_in);
//                                description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
//                                Util.setShadeColor(new java.awt.Color(s.red, s.green, s.blue, DrawingCanvas.DEFAULT_ALPHA));
//                                selectedShade.setBackground(new Background(new BackgroundFill(awtToFXColor(new java.awt.Color(s.red, s.green, s.blue, 255)), CornerRadii.EMPTY, Insets.EMPTY)));
//                                break;
//                            }
//                        }
//                    }
//                }
//            });
//            scrollPane.setBorder(Border.EMPTY);
//            new AutoCompleteComboBoxListener<>(cmbAutoComplete);//making autoComplete Combobox
//        } catch (Exception ex) {
//            Logger.getLogger(VisualizerController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public void addRecentUsedShade(java.awt.Color c) {
//        Platform.runLater(() -> {
//            Color c1 = awtToFXColor(c);
//            if (!recentUsedColorSet.contains(c1)) {
//                Rectangle rec = new Rectangle(40, 50);
//                rec.setAccessibleHelp(String.valueOf(c.getRGB()));
//                applyMouseEventToRec(rec);
//                rec.setFill(c1);
//                recentUseColor.add(rec);
//                recentUsedColorSet.add(c1);
//            }
//        });
//    }
//
//    public ObservableList<Rectangle> getShadeUsedColor() {
//        return shadeUsedColor;
//    }
//
//    public void addShadeSelectedInImage(java.awt.Color c, String id) {
//        Platform.runLater(() -> {
//            if (id != null) {
//                boolean update = false;
//                for (Rectangle r : shadeUsedColor) {
//                    if (r.getId().equals(id)) {
//                        r.setFill(awtToFXColor(c));
//                        r.setAccessibleHelp(String.valueOf(c.getRGB()));
//                        r.setAccessibleRoleDescription(description);
//                        update = true;
//                        break;
//                    }
//                }
//                if (!update) {
//                    addRecInShade(c, id);
//                }
//            }
//        });
//
//    }
//
//    public void addRecInShade(java.awt.Color c, String id) {
//        Rectangle rec = new Rectangle(40, 50);
//        applyMouseEventToRec(rec);
//        rec.setAccessibleHelp(String.valueOf(c.getRGB()));
//        rec.setFill(awtToFXColor(c));
//        rec.setId(id);
//        description = null;
//        if (description == null) {
//            description = "Default Color";
//        }
//        rec.setAccessibleRoleDescription(description);
//        shadeUsedColor.add(rec);
//    }
//
//    public void removeRecentUsedShade(int index) {
//        Platform.runLater(() -> {
//            recentUseColor.remove(index);
//        });
//
//    }
//
//    public void removeShadeSelectedInImage(Rectangle index) {
//        Platform.runLater(() -> {
//            shadeUsedColor.remove(index);
//        });
//
//    }
//
//    public void updateShadeSelectedImage(ToolsModel toolModel, String operation) {
//        if (toolModel instanceof CustomArea || toolModel instanceof CustomBrush) {
//            if (null != operation) {
//                switch (operation) {
//                    case "Add":
//                        addRecInShade(toolModel.getColor(), toolModel.getID());
//                        break;
//                    case "Remove":
//                        for (Rectangle r : shadeUsedColor) {
//                            if (r.getId().equals(toolModel.getID())) {
//                                removeShadeSelectedInImage(r);
//                                break;
//                            }
//                        }
//                        break;
//                }
//            }
//        } else if (toolModel instanceof CustomColor) {
//
//            CustomColor customColor = (CustomColor) toolModel;
//
//            if (customColor.getToolsModel() instanceof Grouping) {
//
//            } else {
//                for (Rectangle r : shadeUsedColor) {
//                    if (r.getId().equals(customColor.getShape().getID())) {
//                        r.setFill(awtToFXColor(customColor.getShape().getColor()));
//                        break;
//                    }
//                }
//            }
//
//        }
//    }
//
//    @FXML
//    private void onPolygonToolHandle(MouseEvent event) {
//        negativeToolButton.setSelected(false);
//        toggleButtonPressed();
//    }
//
//    @FXML
//    private void onUngroupBtnHandle(MouseEvent event) {
//        panel.getDrawingCanvas().unGroupButtonCalled();
//    }
//
//    @FXML
//    private void onGroupBtnHandle(MouseEvent event) {
//        panel.getDrawingCanvas().groupButtonCalled();
//    }
//
//    public void setScene(Scene rootScene) {
//        this.rootScene = rootScene;
//
//        rootScene.setOnKeyPressed((javafx.scene.input.KeyEvent event) -> {
//            if (panel.getDrawingCanvas().getTempRectList().isEmpty()) {
//                panel.getDrawingCanvas().keyPressed(event);
//            }
//        });
//        rootScene.setOnKeyReleased((javafx.scene.input.KeyEvent event) -> {
//            panel.getDrawingCanvas().keyReleased(event);
//            if (event.isControlDown()) {
//                if (event.getCode() == KeyCode.ADD) {
//                    zoomInActionHandle();
//                } else if (event.getCode() == KeyCode.SUBTRACT) {
//                    zoomOutActionHandle();
//
//                } else if (event.getCode() == KeyCode.P) {
//                    printImageHandle();
//                } else if (event.getCode() == KeyCode.S) {
//                    onSaveImage(null);
//                } else if (event.isShiftDown()) {
//                    if (event.getCode() == KeyCode.S) {
//                        save();
//                    }
//                }
//            }
//        });
//    }
//
//    @FXML
//    private void onDeleteShadeHandle(ActionEvent event) {
//        panel.getDrawingCanvas().deleteShadeButtonCalled();
//    }
//
//    public void drawColorPanel() {
//        final PieChart pieChart = new PieChart();
//        final GridPane shade = new GridPane();
//        pieChart.setLegendVisible(false);
//        pieChart.setLabelLineLength(0);
//        pieChart.setStyle("-fx-text-fill:rgb(52,52,52);");
//        pieChart.setAnimated(false);
//        pieChart.setPrefSize(50, 50);
//        pieChart.setData(getChartData());
//
//        /**
//         * Getting data from database and changing color of pieChart
//         */
//        if (FamilyModel.familyList.size() > 0) {
//            FamilyModel.findAll();
//        }
//        final ObservableList<PieChart.Data> data = pieChart.getData();
//        List<FamilyModel> f = FamilyModel.getFamilyList();
//        for (int i = 0; i < data.size(); i++) {
//            java.awt.Color c = new java.awt.Color(f.get(i).red, f.get(i).green, f.get(i).blue, 255);
//            String hexColor = "#" + Integer.toHexString(c.getRGB()).substring(2);
//            data.get(i).getNode().setStyle("-fx-pie-color:" + hexColor + ";");
//            applyMouseEvents(data.get(i), pieChart);
//
//        }
//
//        pieChart.setOnMouseClicked((MouseEvent mouseEvent) -> {
//            int row;
//            int col;
//            int j;
//            shade.getChildren().clear();
//            if (selectedDataInGrid != null) {
//                List<ShadesModel> f1 = ShadesModel.findByFamily(selectedDataInGrid);
//                selectedDataInGrid = null;
//                row = 0;
//                col = 0;
//                j = 0;
//                for (ShadesModel f11 : f1) {
//                    j += 0.1;
//                    java.awt.Color color = new java.awt.Color(f11.red, f11.green, f11.blue);
//                    Color c = awtToFXColor(color);
//                    final Pane pane = new Pane();
//                    pane.setMinSize(30, 30);
//                    pane.setId("" + f11.sequence);
//                    pane.setStyle("-fx-border-color:white;");
//                    pane.setBackground(new Background(new BackgroundFill(c, CornerRadii.EMPTY, Insets.EMPTY)));
//                    applyMouseEventsTo(pane);
//                    col += j + 1 / 3;
//                    row += j + 1 % 3;
//                    shade.add(pane, col, row);
//                    if (row > 7) {
//                        row = 0;
//                        col += 1;
//                    }
//                }
//            }
//        });
//        shade.getStyleClass().add("grid");
//        ScrollPane shadeScrollPane = new ScrollPane(shade);
//        shadeScrollPane.getStyleClass().add("scroll-pane");
//        colorPanel.getChildren().addAll(pieChart, shadeScrollPane);
//        try {
//            final Robot robot = new Robot();
//            shade.setGridLinesVisible(true);
//            shade.setOnMouseClicked((MouseEvent event) -> {
//                PointerInfo pi = MouseInfo.getPointerInfo();
//                java.awt.Color c = robot.getPixelColor(pi.getLocation().x, pi.getLocation().y);
//                Util.setShadeColor(new java.awt.Color(c.getRed(), c.getGreen(), c.getBlue(), DrawingCanvas.DEFAULT_ALPHA));
//                System.out.println("alp:" + Util.getShadeColor().getAlpha());
//                double red = c.getRed() / 255.0;
//                double green = c.getGreen() / 255.0;
//                double blue = c.getBlue() / 255.0;
//                double opacity = c.getAlpha() / 255.0;
//                Color c1 = new Color(red, green, blue, opacity);
//                selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
//                toggleButtonReset();
//                panel.getDrawingCanvas().disableAllTools();
//                panel.getDrawingCanvas().setCustomCursor(CursorType.COLORFILL);
//
//            });
//
//        } catch (Exception ex) {
//            Logger.getLogger(VisualizerController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    @FXML
//    private void onBrushToolHandle(ActionEvent event) {
//        try {
//
//            negativeToolButton.setSelected(false);
//            toggleButtonPressed();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    @FXML
//    private void onNegativeToolButtonHandle(MouseEvent evt) {
//        if (polygonToolBtn.isSelected() || brushToolButton.isSelected()) {
//            negativeToolButton.setSelected(true);
//        } else {
//            negativeToolButton.setSelected(false);
//        }
//        toggleButtonPressed();
//    }
//
//    @FXML
//    public void onFitOnScreenHandle(ActionEvent event) {
//        changeCustomCursor();
//        panelContainer.setManaged(false);
//        // scaled image size
//        Bounds parent = panelContainer.getBoundsInParent();
//        double scaleX = Math.abs(scrollPane.getWidth() / parent.getWidth());
//        double scaleY = Math.abs(scrollPane.getHeight() / parent.getHeight());
//        double scaler = scaleX < scaleY ? scaleX : scaleY;
//        Scale scale1 = new Scale(scaler, scaler, 0, 0);
//        panelContainer.getTransforms().add(scale1);
//        parent = panelContainer.getBoundsInParent();
//        imageHolder.setPrefWidth(parent.getWidth());
//        imageHolder.setPrefHeight(parent.getHeight());
//
//        zoomCount = 5;
//
//    }
//
//    private ObservableList<PieChart.Data> getChartData() {
//        ObservableList<PieChart.Data> answer = FXCollections.observableArrayList();
//        if (FamilyModel.familyList.size() < 1) {
//            FamilyModel.findAll();
//        }
//        List<FamilyModel> f = FamilyModel.getFamilyList();
//        f.stream().forEach((f1) -> {
//            answer.addAll(new PieChart.Data(f1.name, 36));
//        });
//        return answer;
//    }
//
//    private void toggleButtonPressed() {
//        ToggleButton selectedButton = (ToggleButton) group1.getSelectedToggle();
//        panel.getDrawingCanvas().setCustomCursor(CursorType.DEFAULT);
//        if (negativeToolButton.isSelected() && selectedButton == polygonToolBtn) {
//            panel.getDrawingCanvas().resetButtonCalled();
//            panel.addPolygonDrawingLayer();
//            panel.getDrawingCanvas().setEnableNegative(true);
//            panel.getDrawingCanvas().setEnableShading(true);
//            panel.getDrawingCanvas().setEnableBrush(false);
//            shadeCorrectorVisibility(false);
//            panel.getDrawingCanvas().setCustomCursor(CursorType.POLYGON);
//        } else if (negativeToolButton.isSelected() && selectedButton == brushToolButton) {
//            panel.addPolygonDrawingLayer();
//            panel.getDrawingCanvas().setEnableNegative(true);
//            panel.getDrawingCanvas().setEnableShading(false);
//            panel.getDrawingCanvas().resetButtonCalled();
//            panel.getDrawingCanvas().setEnableBrush(true);
//            shadeCorrectorVisibility(false);
//            panel.getDrawingCanvas().setCustomCursor(CursorType.BRUSH);
//        } else if (selectedButton == polygonToolBtn) {
//            panel.addPolygonDrawingLayer();
//            panel.getDrawingCanvas().setEnableBrush(false);
//            panel.getDrawingCanvas().setEnableShading(true);
//            panel.getDrawingCanvas().setEnableNegative(false);
//            shadeCorrectorVisibility(false);
//            panel.getDrawingCanvas().setCustomCursor(CursorType.POLYGON);
//        } else if (selectedButton == brushToolButton) {
//            panel.addPolygonDrawingLayer();
//            panel.getDrawingCanvas().setEnableShading(false);
//            panel.getDrawingCanvas().resetButtonCalled();
//            panel.getDrawingCanvas().setEnableBrush(true);
//            panel.getDrawingCanvas().setEnableNegative(false);
//            shadeCorrectorVisibility(false);
//            panel.getDrawingCanvas().setCustomCursor(CursorType.BRUSH);
//        } else if (selectedButton == null) {
//            panel.removePolygonDrawingLayer();
//            panel.getDrawingCanvas().setEnableBrush(false);
//            panel.getDrawingCanvas().setEnableShading(false);
//            panel.getDrawingCanvas().setEnableNegative(false);
//            panel.getDrawingCanvas().resetButtonCalled();
//            shadeCorrectorVisibility(false);
//        } else {
//            negativeToolButton.setSelected(false);
//            panel.removePolygonDrawingLayer();
//            panel.getDrawingCanvas().setEnableNegative(false);
//            panel.getDrawingCanvas().setEnableShading(false);
//            panel.getDrawingCanvas().resetButtonCalled();
//            panel.getDrawingCanvas().setEnableBrush(false);
//            shadeCorrectorVisibility(false);
//
//        }
//    }
//
//    double getScaleXRatio() {
//        return panelContainer.getBoundsInParent().getWidth() / panelContainer.getContent().getWidth();
//    }
//
//    double getScaleYRatio() {
//        return panelContainer.getBoundsInParent().getHeight() / panelContainer.getContent().getHeight();
//    }
//
//    private void saveActionHandle() {
//        panel.getDrawingCanvas().saveObject();
//    }
//
//    private void applyMouseEvents(final PieChart.Data data, PieChart pieChart) {
//        final Node node = data.getNode();
//        final ScaleTransition scaleTransition
//                = new ScaleTransition(Duration.millis(1), node);
//        scaleTransition.setToX(1.8f);
//        scaleTransition.setToY(1.8f);
//        final ScaleTransition scaleTransitionAnti
//                = new ScaleTransition(Duration.millis(1), node);
//        scaleTransitionAnti.setToX(1f);
//        scaleTransitionAnti.setToY(1f);
//        node.setOnMouseEntered((MouseEvent arg0) -> {
//            scaleTransition.play();
//            scaleTransitionAnti.stop();
//        });
//        node.setOnMouseExited((MouseEvent arg0) -> {
//            scaleTransition.stop();
//            scaleTransitionAnti.play();
//        });
//
//        node.setOnMouseClicked((MouseEvent mouseEvent) -> {
//            selectedDataInGrid = data.getName();
//        });
//    }
//
//    private void applyMouseEventsTo(Pane pane) {
//        final ScaleTransition scaleTransition
//                = new ScaleTransition(Duration.millis(1), pane);
//        scaleTransition.setToX(2f);
//        scaleTransition.setToY(2f);
//        final ScaleTransition scaleTransitionAnti
//                = new ScaleTransition(Duration.millis(1), pane);
//        scaleTransitionAnti.setToX(1f);
//        scaleTransitionAnti.setToY(1f);
//        pane.setOnMouseEntered((MouseEvent arg0) -> {
//            scaleTransition.play();
//            scaleTransitionAnti.stop();
//        });
//        pane.setOnMouseExited((MouseEvent arg0) -> {
//            scaleTransition.stop();
//            scaleTransitionAnti.play();
//        });
//        pane.setOnMouseClicked((MouseEvent e) -> {
//            for (ShadesModel s : ShadesModel.shadesList) {
//                if (s.sequence == Integer.valueOf(pane.getId())) {
//                    lblName.setText(s.name);
//                    lblCode.setText(String.valueOf(s.code));
//                    lblPageNo.setText(String.valueOf("Page No: " + s.page));
//                    lblShadeIn.setText(s.Na_in);
//                    description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
//                }
//            }
//        });
//    }
//
//    @FXML
//    private void onUndoBtnHandle(ActionEvent event) {
//        System.out.println("Undo pressed");
//        boolean b = redoUndoManager.undo();
//        getRedoBtn().setDisable(false);
//        if (!b) {
//            getUndoBtn().setDisable(true);
//        }
//    }
//
//    @FXML
//    private void onRedoBtnHandle(ActionEvent event) {
//        System.out.println("Redo pressed");
//        boolean b = redoUndoManager.redo();
//        getUndoBtn().setDisable(false);
//        if (!b) {
//            getRedoBtn().setDisable(true);
//        }
//    }
//
//    public Button getUndoBtn() {
//        return undoBtn;
//    }
//
//    public void setUndoBtn(Button undoBtn) {
//        this.undoBtn = undoBtn;
//    }
//
//    public Button getRedoBtn() {
//        return redoBtn;
//    }
//
//    public void setRedoBtn(Button redoBtn) {
//        this.redoBtn = redoBtn;
//    }
//
//    private void applyMouseEventToRec(Rectangle rec) {
//        final ScaleTransition scaleTransition
//                = new ScaleTransition(Duration.millis(1), rec);
//        scaleTransition.setToX(2f);
//        scaleTransition.setToY(2f);
//        final ScaleTransition scaleTransitionAnti
//                = new ScaleTransition(Duration.millis(1), rec);
//        scaleTransitionAnti.setToX(1f);
//        scaleTransitionAnti.setToY(1f);
//        rec.setOnMouseEntered((MouseEvent arg0) -> {
//            scaleTransition.play();
//            scaleTransitionAnti.stop();
//            for (ToolsModel m : panel.getDrawingCanvas().getShadeList()) {
//                if (m.getID().equals(rec.getId())) {
//                    panel.getDrawingCanvas().applyColorToShade(m, ColorStyle.HOVER);
//                    break;
//                }
//
//            }
//            panel.repaint();
//        });
//        rec.setOnMouseExited((MouseEvent arg0) -> {
//            scaleTransition.stop();
//            scaleTransitionAnti.play();
//            for (ToolsModel m : panel.getDrawingCanvas().getShadeList()) {
//                if (m.getID().equals(rec.getId())) {
//                    panel.getDrawingCanvas().applyColorToShade(m, ColorStyle.DEFAULT);
//                    break;
//                }
//
//            }
//            panel.repaint();
//        });
//
//        rec.setOnMouseClicked((MouseEvent e) -> {
//            Platform.runLater(() -> {
//                try {
//                    if (group1.getSelectedToggle() != null) {
//                        group1.getSelectedToggle().setSelected(false);
//                    }
//                    toggleButtonPressed();
//                    java.awt.Color c = new java.awt.Color(Integer.parseInt(rec.getAccessibleHelp()));
//                    System.out.println(c);
//                    for (ShadesModel s : ShadesModel.findAll()) {
//                        if (s.red == c.getRed() && s.green == c.getGreen() && s.blue == c.getBlue()) {
//                            lblName.setText(s.name);
//                            lblCode.setText(String.valueOf(s.code));
//                            lblPageNo.setText(String.valueOf("Page No: " + s.page));
//                            lblShadeIn.setText(s.Na_in);
//                            description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
//                            rec.setAccessibleRoleDescription(description);
//                            Util.setShadeColor(new java.awt.Color(c.getRed(), c.getGreen(), c.getBlue(), DrawingCanvas.DEFAULT_ALPHA));
//                            break;
//                        }
//                    }
//                    selectedShade.setBackground(new Background(new BackgroundFill(awtToFXColor(c), CornerRadii.EMPTY, Insets.EMPTY)));
//                } catch (Exception ex) {
//                    Logger.getLogger(VisualizerController.class
//                            .getName()).log(Level.SEVERE, null, ex);
//                }
//            });
//        });
//    }
//
//    private void save() {
//        try {
//            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/SaveImage.fxml"));
//            Parent root = (Parent) fxmlLoader.load();
//            SaveImageController controller = fxmlLoader.<SaveImageController>getController();
//            controller.setPanel(panel.getDrawingCanvas());
//            Stage stage = new Stage();
//            stage.setScene(new Scene(root));
//            stage.setTitle("Save Image");
//            stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
//            stage.initModality(Modality.WINDOW_MODAL);
////            stage.initOwner(panelContainer.getScene().getWindow());
//            stage.showAndWait();
//        } catch (Exception ex) {
//            Logger.getLogger(VisualizerController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    @FXML
//    private void onSaveAsHandler(ActionEvent event) {
//        save();
//    }
//
//    public void changeCustomCursor() {
////        panelContainer.getScene().setCursor(new ImageCursor(new Image(getClass().getResource("/Controller/images/2.png").toExternalForm())));
//    }
//
//    @FXML
//    private void onSaveImage(ActionEvent event) {
//        Alert error = new Alert(Alert.AlertType.INFORMATION);
//        if (Util.getFilePath() != null) {
//            File imageToSave = Util.getFilePath();
//            if (!imageToSave.exists()) {
//                imageToSave.mkdirs();
//            }
//            if (imageToSave.exists()) {
//                panel.getDrawingCanvas().saveObject();
//                error.setContentText("Image Saved Successfully." + imageToSave.getName());
//                Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(1), (ActionEvent event1) -> {
//                    error.setResult(ButtonType.YES);
//                    error.hide();
//                }));
//                idlestage.setCycleCount(1);
//                idlestage.play();
//                error.showAndWait();
//            }
//        } else {
//            save();
//        }
//    }
//
//    TrayNotification notification;
//
//    public void showTrayNotification(String title, String message) {
//
//        if (notification != null) {
//            if (notification.isTrayShowing()) {
//                return;
//            }
//        }
//
//        notification = new TrayNotification();
//        notification.setTitle(title);
//        notification.setRectangleFill(Paint.valueOf("DD366C"));
//        notification.setImage(new Image(getClass().getResourceAsStream("/Controller/images/logo.png")));
//        notification.setMessage(message);
//        notification.setAnimationType(AnimationType.POPUP);
//        notification.showAndDismiss(Duration.seconds(2));
//    }
//
//    public void toggleButtonReset() {
//        if (group1.getSelectedToggle() != null) {
//            group1.getSelectedToggle().setSelected(false);
//        }
//        toggleButtonPressed();
//    }
//
//    public BufferedImage getImg() {
//        return img;
//    }
//
//    @FXML
//    private void onBlackColorSelection(ActionEvent event) {
//        Util.setShadeColor(new java.awt.Color(0, 0, 0, DrawingCanvas.DEFAULT_ALPHA));
//        double red = 0;
//        double green = 0;
//        double blue = 0;
//        double opacity = 1;
//        Color c1 = new Color(red, green, blue, opacity);
//        selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
//        toggleButtonReset();
//        panel.getDrawingCanvas().disableAllTools();
//        panel.getDrawingCanvas().setCustomCursor(CursorType.COLORFILL);
//    }
//
//    @FXML
//    private void onWhiteColorSelection(ActionEvent event) {
//        Util.setShadeColor(new java.awt.Color(254, 255, 255, DrawingCanvas.DEFAULT_ALPHA));
//        double red = 1;
//        double green = 1;
//        double blue = 1;
//        double opacity = 1;
//        Color c1 = new Color(red, green, blue, opacity);
//        selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
//        toggleButtonReset();
//        panel.getDrawingCanvas().disableAllTools();
//        panel.getDrawingCanvas().setCustomCursor(CursorType.COLORFILL);
//    }
//
//    @FXML
//    private void onRemoveColorSelection(ActionEvent event) {
//        Util.setShadeColor(new java.awt.Color(255, 255, 255, 0));
//        double red = 1;
//        double green = 1;
//        double blue = 1;
//        double opacity = 0;
//        Color c1 = new Color(red, green, blue, opacity);
//        selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
//        toggleButtonReset();
//        panel.getDrawingCanvas().disableAllTools();
//        panel.getDrawingCanvas().setCustomCursor(CursorType.COLORFILL);
//    }
//
//    @FXML
//    private void onShadeCorrectorBtnHandle(ActionEvent event) {
//        if (!panel.getDrawingCanvas().getTempSelectionList().isEmpty()) {
//            shadeCorrectorVisibility(true);
//        }
//    }
//
//    @FXML
//    private void onShadeCorrectorDoneBtnHandle(ActionEvent event) {
//        shadeCorrectorVisibility(false);
//    }
//
//    private void shadeCorrectorVisibility(boolean value) {
//        if (!value) {
//            if (!panel.getDrawingCanvas().getTempSelectionList().isEmpty()) {
//                panel.getDrawingCanvas().getTempSelectionList().clear();
//            }
//        }
//        slider.setVisible(value);
//        shadeCorrectorDoneBtn.setVisible(value);
//    }
//
////    public ShadeCorrector shadeCorrector {
////        return shadeCorrector;
////    }
//    @FXML
//    private void onHelpHandler(ActionEvent event) {
//        System.out.println("I need some help");
//    }
//
//    private void zoomInActionHandle() {
//        if (zoomCount > 10) {
//     r       showTrayNotification("Zoom In", "Cannot zoom-In anymore");
//            return;
//        }
//        panelContainer.setManaged(false);
//        double scalefactor = 1.1;
//        Scale scale = new Scale(panelContainer.getScaleX() * scalefactor, panelContainer.getScaleY() * scalefactor, 0, 0);
//
//        panel.getDrawingCanvas().updateStroke(true);
//
//        panelContainer.getTransforms().add(scale);
//        Bounds parent = panelContainer.getBoundsInParent();
//        imageHolder.setPrefWidth(parent.getWidth());
//        imageHolder.setPrefHeight(parent.getHeight());
//        System.out.println("Zoom in :" + parent.getWidth() + " " + parent.getHeight());
//        zoomCount++;
//    }
//
//    private void zoomOutActionHandle() {
//        if (zoomCount < 1) {
//            showTrayNotification("Zoom Out", "Cannot zoom-Out anymore");
//            return;
//        }
//        panelContainer.setManaged(false);
//        double scalefactor = 1 / 1.1;
//        Scale scale = new Scale(panelContainer.getScaleX() * scalefactor, panelContainer.getScaleY() * scalefactor, 0, 0);
//
//        panel.getDrawingCanvas().updateStroke(false);
//
//        panelContainer.getTransforms().add(scale);
//        Bounds parent = panelContainer.getBoundsInParent();
//        imageHolder.setPrefWidth(parent.getWidth());
//        imageHolder.setPrefHeight(parent.getHeight());
//        System.out.println("Zoom out :" + parent.getWidth() + " " + parent.getHeight());
//        zoomCount--;
//    }
//
//    private void printImageHandle() {
//        try {
//            File f = new File("data/drawnImage.jpg");
//            ImageIO.write(panel.getDrawingCanvas().getPreviewImage(), "jpg", f);
//            PrintPreview printPreview = new PrintPreview(getShadeUsedColor(), f.getAbsolutePath());
//        } catch (IOException ex) {
//            Logger.getLogger(VisualizerController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public void setPanel(MainPane panel) {
//        this.panel = panel;
//    }
//
//    public void setShadeCorrector(ShadeCorrector shadeCorrector) {
//        this.shadeCorrector = shadeCorrector;
//    }
//
//    public void setRedoUndoManager(RedoUndoManager redoUndoManager) {
//        this.redoUndoManager = redoUndoManager;
//    }
//
//    public void disableDoneResetButton(boolean value) {
//        doneButton.setDisable(value);
//        resetButton.setDisable(value);
//
//        deleteShadeButton.setDisable(!value);
//        groupBtn.setDisable(!value);
//        unGroupBtn.setDisable(!value);
//        shadeCorrectorBtn.setDisable(!value);
//
//    }
//}
