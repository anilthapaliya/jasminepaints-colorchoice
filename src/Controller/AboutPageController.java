package Controller;

import com.sun.deploy.uitoolkit.impl.fx.HostServicesFactory;
import com.sun.javafx.application.HostServicesDelegate;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.application.Application;
import javafx.stage.Stage;
import javax.swing.JDialog;

/**
 *
 * @author Anil
 */
public class AboutPageController extends Application implements Initializable {
    
    @FXML
    private Label txtAppVersion;

    @FXML
    private Label txtJasminePaints;

    @FXML
    private Hyperlink linkSalyani;
    
    private JDialog window;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
            linkSalyani.setOnAction((event) -> {
                try{
                    HostServicesDelegate hostServices = HostServicesFactory.getInstance(this);
                    hostServices.showDocument("www.salyani.com.np");
                }
                catch(Exception e){ e.printStackTrace(); }
            });
        
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public JDialog getWindow() {
        return window;
    }

    public void setWindow(JDialog window) {
        this.window = window;
    }
    
}
