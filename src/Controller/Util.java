package Controller;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;

public class Util {

    private static BufferedImage img;
    private static Color shadeColor;
    private static double scale = 1;
    private static File filePath;
    private static String saveInterior;
    private static String saveExterior;
    private static PageFormat pf;
    public static boolean applicationStart = false;
    public static String helpLink="E:\\NewVersion\\loksewadesktop\\Resources\\sarbajanik-kharid.pdf";
    public static double getScale() {
        return scale;
    }

    public static void setScale(double scale) {
        if (scale < 3 && scale > .25) {
            Util.scale = scale;
        }
    }

    public static BufferedImage getImg() {
        return img;
    }

    public static void setImg(BufferedImage imgs) {
        img = imgs;
    }

    public static Color getShadeColor() {
        return shadeColor;
    }

    public static void setShadeColor(Color shadeColor) {
        Util.shadeColor = shadeColor;
    }

    public static File getFilePath() {
        return filePath;
    }

    public static void setFilePath(File aFilePath) {
        filePath = aFilePath;
    }

    public static String getSaveInterior() {
        return saveInterior;
    }

    public static void setSaveInterior(String saveInterior) {
        Util.saveInterior = saveInterior;
    }

    public static String getSaveExterior() {
        return saveExterior;
    }

    public static void setSaveExterior(String saveExterior) {
        Util.saveExterior = saveExterior;
    }

    public static String getConnection() {

        URL inputUrl = Util.class.getResource("colour_visualizer.db");
        File dest = new File("data/colour_visualizer.db");
        if (!dest.exists()) {
            try {
                FileUtils.copyURLToFile(inputUrl, dest);
            } catch (IOException ex) {
                Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        return "jdbc:sqlite:" + dest.getPath();
    }

    public static PageFormat getPf() {
        return pf;
    }

    public static void setPf(PageFormat pf) {
        Util.pf = pf;
    }

    public static javafx.scene.paint.Color awtToFXColor(java.awt.Color awtColor) {

        int r = awtColor.getRed();
        int g = awtColor.getGreen();
        int b = awtColor.getBlue();
        return javafx.scene.paint.Color.rgb(r, g, b, 1);

    }

    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_RGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    public static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    public static WritableImage getImage(String name) {
        try {
            URL url = Util.class.getResource("/Controller/images/" + name + ".png");
            BufferedImage awtImg = ImageIO.read(url);
            return SwingFXUtils.toFXImage(awtImg, null);

        } catch (Exception ex) {
            Logger.getLogger(WelcomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void deleteFolderForce(File folder) {
        for (File file : folder.listFiles()) {
            try {
                file.setWritable(true);
                FileDeleteStrategy.FORCE.delete(file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void extractPrintImage(String filePath, JasperPrint print) {
        File file = new File(filePath);
        OutputStream ouputStream = null;
        try {
            ouputStream = new FileOutputStream(file);
            DefaultJasperReportsContext.getInstance();
            JasperPrintManager printManager = JasperPrintManager.getInstance(DefaultJasperReportsContext.getInstance());

            BufferedImage rendered_image = null;
            rendered_image = (BufferedImage) printManager.printPageToImage(print, 0, 1.6f);
            ImageIO.write(rendered_image, "jpg", ouputStream);
            ouputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
