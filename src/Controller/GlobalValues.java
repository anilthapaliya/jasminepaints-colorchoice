/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.File;
import javafx.stage.Stage;
 
/**
 *
 * @author Anil
 */
public class GlobalValues {
    private static File imagePath;
    private static int cameFrom;
    private static boolean backActive;
    private static Stage primaryStage;
    private static int selectedTab;     // For Displaying Items On Image Selection
    private static int selectedIndex;   // For Displaying Items On Image Selection
    private static String directory;    // For Displaying Items On Image Selection (SAMPLE IMAGES)
    
    public static void setImagePath(File path){
        imagePath = path;
    }
    
    public static File getImagePath(){
        return imagePath;
    }

    public static int getCameFrom() {
        return cameFrom;
    }

    public static void setCameFrom(int cameFrom) {
        GlobalValues.cameFrom = cameFrom;
    }

    public static void setPrimaryStage(Stage primaryStage) {
        GlobalValues.primaryStage = primaryStage;
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static boolean isBackActive() {
        return backActive;
    }

    public static void setBackActive(boolean backActive) {
        GlobalValues.backActive = backActive;
    }

    public static int getSelectedTab() {
        return selectedTab;
    }

    public static int getSelectedIndex() {
        return selectedIndex;
    }

    public static void setSelectedTab(int selectedTab) {
        GlobalValues.selectedTab = selectedTab;
    }

    public static void setSelectedIndex(int selectedIndex) {
        GlobalValues.selectedIndex = selectedIndex;
    }

    public static String getDirectory() {
        return directory;
    }

    public static void setDirectory(String directory) {
        GlobalValues.directory = directory;
    }
    
}

