package Controller.sub;

import Controller.Util;
import Controller.VisualizerController;
import static Controller.VisualizerController.panel;
import Database.FamilyModel;
import Database.ShadesModel;
import Tools.CursorType;
import Tools.DrawingCanvas;
import java.awt.MouseInfo;
import java.awt.PointerInfo;
import java.awt.Robot;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Your Name <dinesh katwal  at nepallink.net>
 */
public class RightController implements Initializable {

    public static VisualizerController getVisualizerController() {
        return visualizerController;
    }

    public static void setVisualizerController(VisualizerController aVisualizerController) {
        visualizerController = aVisualizerController;
    }

    @FXML
    private VBox colorPanel;

    private String selectedDataInGrid = null;

    private static VisualizerController visualizerController;
    int row;
    int col;
    int j;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        drawColorPanel();
    }

    public void drawColorPanel() {
        ImageView imageView = new ImageView(RightController.class.getResource("/Controller/images/logo.png").toExternalForm());
        imageView.translateYProperty().set(-5);
        
        final PieChart pieChart = new PieChart();
        final GridPane shade = new GridPane();
        pieChart.setLegendVisible(false);
        pieChart.setLabelLineLength(0);
        pieChart.setStyle("-fx-text-fill:rgb(52, 52, 52);");
        pieChart.setAnimated(false);
        pieChart.setPrefSize(50, 50);
        pieChart.translateYProperty().set(0);
        pieChart.setData(getChartData());

        shade.setStyle("-fx-background-fill:rgb(52, 52, 52);");

        if (FamilyModel.familyList.size() > 0) {
            FamilyModel.findAll();
        }
        
        final ObservableList<PieChart.Data> data = pieChart.getData();
        List<FamilyModel> f = FamilyModel.getFamilyList();
        for (int i = 0; i < data.size(); i++) {
            java.awt.Color c = new java.awt.Color(f.get(i).red, f.get(i).green, f.get(i).blue, 255);
            String hexColor = "#" + Integer.toHexString(c.getRGB()).substring(2);
            data.get(i).getNode().setStyle("-fx-pie-color:" + hexColor + ";");
            applyMouseEvents(data.get(i), pieChart);
        }
        
        pieChart.setOnMouseClicked((MouseEvent mouseEvent) -> {
            shade.getChildren().clear();
            if (selectedDataInGrid != null) {
                List<ShadesModel> f1 = ShadesModel.findByFamily(selectedDataInGrid);
                selectedDataInGrid = null;
                row = 0;
                col = 0;
                j = 0;
                String tempGroup = "";
                for (ShadesModel f11 : f1) {
                    if (!tempGroup.equals(f11.type)) {
                        final Label group = new Label();
                        group.setText(f11.type);
                        group.setPadding(new Insets(5, 5, 5, 5));
//                        group.setFont(Font.font(Font.getDefault(), FontWeight.BOLD,15));
                        group.setFont(Font.font("System", FontWeight.BOLD, 16));
//                        group.setPrefSize(30, 30);
                        tempGroup = f11.type;
                        row++;
                        shade.add(group, 1, row, 7, 1);
                        row += 1;
                        col = 0;
                    }
                    addShade(f11, shade);
                }
            }
        });
        
        shade.getStyleClass().add("grid");
        ScrollPane shadeScrollPane = new ScrollPane(shade);
        shadeScrollPane.getStyleClass().add("scroll-pane");
        shadeScrollPane.translateYProperty().set(-80);

        StackPane stackPane = new StackPane();
        stackPane.setPrefSize(30, 30);
        stackPane.getChildren().add(pieChart);
        stackPane.translateYProperty().set(-20);
        stackPane.setMinHeight(140);
        stackPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        StackPane.setAlignment(pieChart, Pos.CENTER);
        colorPanel.getChildren().addAll(imageView, stackPane, shadeScrollPane);
        
        try {
            final Robot robot = new Robot();
            shade.setGridLinesVisible(true);
            shade.setOnMouseClicked((MouseEvent event) -> {
                PointerInfo pi = MouseInfo.getPointerInfo();
                java.awt.Color c = robot.getPixelColor(pi.getLocation().x, pi.getLocation().y);
                Util.setShadeColor(new java.awt.Color(c.getRed(), c.getGreen(), c.getBlue(), DrawingCanvas.DEFAULT_ALPHA));
                double red = c.getRed() / 255.0;
                double green = c.getGreen() / 255.0;
                double blue = c.getBlue() / 255.0;
                double opacity = c.getAlpha() / 255.0;
                Color c1 = new Color(red, green, blue, opacity);
                VisualizerController.bottomController.selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
                VisualizerController.leftController.toggleButtonReset();
                panel.getDrawingCanvas().disableAllTools();
                VisualizerController.panel.getDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.COLORFILL));
            });

        } catch (Exception ex) {
            Logger.getLogger(VisualizerController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        
        shadeScrollPane.setMinViewportHeight(265);
        shadeScrollPane.translateYProperty().set(-15);
    }

    public void addShade(ShadesModel f11, GridPane shade) {
        j += 0.1;
        java.awt.Color color = new java.awt.Color(f11.red, f11.green, f11.blue);
        Color c = Util.awtToFXColor(color);
        final Pane pane = new Pane();

        pane.setMinSize(30, 30);
        pane.setId("" + f11.sequence);
        pane.setBackground(new Background(new BackgroundFill(c, CornerRadii.EMPTY, Insets.EMPTY)));
        applyMouseEventsTo(pane);
        row += j + 1 / 3;
        col += j + 1 % 3;
        System.out.println("Row:" + row + " Col:" + col);
        shade.add(pane, col, row);
        if (col > 6) {
            col = 0;
            row += 1;
        }
    }

    private void applyMouseEvents(final PieChart.Data data, PieChart pieChart) {
        final Node node = data.getNode();
        final ScaleTransition scaleTransition
                = new ScaleTransition(Duration.millis(1), node);
        scaleTransition.setToX(1.8f);
        scaleTransition.setToY(1.8f);
        final ScaleTransition scaleTransitionAnti
                = new ScaleTransition(Duration.millis(1), node);
        scaleTransitionAnti.setToX(1f);
        scaleTransitionAnti.setToY(1f);
        node.setOnMouseEntered((MouseEvent arg0) -> {
            scaleTransition.play();
            scaleTransitionAnti.stop();
        });
        node.setOnMouseExited((MouseEvent arg0) -> {
            scaleTransition.stop();
            scaleTransitionAnti.play();
        });

        node.setOnMouseClicked((MouseEvent mouseEvent) -> {
            selectedDataInGrid = data.getName();
        });
    }

    private void applyMouseEventsTo(Pane pane) {
        final ScaleTransition scaleTransition
                = new ScaleTransition(Duration.millis(1), pane);
        scaleTransition.setToX(2f);
        scaleTransition.setToY(2f);
        final ScaleTransition scaleTransitionAnti
                = new ScaleTransition(Duration.millis(1), pane);
        scaleTransitionAnti.setToX(1f);
        scaleTransitionAnti.setToY(1f);
        pane.setOnMouseEntered((MouseEvent arg0) -> {
            scaleTransition.play();
            scaleTransitionAnti.stop();
        });
        pane.setOnMouseExited((MouseEvent arg0) -> {
            scaleTransition.stop();
            scaleTransitionAnti.play();
        });
        pane.setOnMouseClicked((MouseEvent e) -> {
            for (ShadesModel s : ShadesModel.shadesList) {
                if (s.sequence == Integer.valueOf(pane.getId())) {
                    VisualizerController.bottomController.setValueOfColor(s);
                }
            }
        });
    }

    private ObservableList<PieChart.Data> getChartData() {
        ObservableList<PieChart.Data> answer = FXCollections.observableArrayList();
        if (FamilyModel.familyList.size() < 1) {
            FamilyModel.findAll();
        }
        List<FamilyModel> f = FamilyModel.getFamilyList();
        f.stream().forEach((f1) -> {
            answer.addAll(new PieChart.Data(f1.name, 36));
        });
        return answer;
    }
}
