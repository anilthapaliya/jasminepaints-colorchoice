/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.sub;

import Controller.Util;
import Controller.VisualizerController;
import Tools.CursorType;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javax.swing.JScrollBar;

public class LeftController implements Initializable {

    @FXML
    public ToggleButton brushToolButton;
    @FXML
    public ToggleGroup group1;
    @FXML
    public ToggleButton polygonToolBtn;
    @FXML
    public ToggleButton zoomOutButton;
    @FXML
    public ToggleButton zoomInButton;
    @FXML
    public ToggleButton fitOnScreen;
    @FXML
    public ToggleButton negativeToolButton;

    private static VisualizerController visualizerController;
    @FXML
    private ToggleGroup group2;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    public void onBrushToolHandle(ActionEvent event) {
        System.out.println("Brush Selected");
        negativeToolButton.setSelected(false);
        toggleButtonPressed();
    }

    @FXML
    public void onPolygonToolHandle(MouseEvent event) {
        negativeToolButton.setSelected(false);
        toggleButtonPressed();
    }

    @FXML
    public void onFitOnScreenHandle(ActionEvent event) {
        fitOnScreen.setSelected(false);
        double w1 = (double) VisualizerController.scrollPane.getViewport().getSize().width;
        double h1 = (double) VisualizerController.scrollPane.getViewport().getSize().height;

        double w2 = (double) VisualizerController.panel.img.getWidth();
        double h2 = (double) VisualizerController.panel.img.getHeight();
        System.out.println("W1=" + w1 + " h1=" + h1);
        System.out.println("W2=" + w2 + " h2=" + h2);
        double scaleX = Math.abs(w1 / w2);
        double scaleY = Math.abs(h1 / h2);

        double scaler = scaleX < scaleY ? scaleX : scaleY;

        Util.setScale(scaler);
        updateAfterScroll();
        System.out.println("FitOn Screen Selected");
    }

    public void toggleButtonPressed() {
        ToggleButton selectedButton = (ToggleButton) group1.getSelectedToggle();
        VisualizerController.panel.getDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.DEFAULT));
        if (negativeToolButton.isSelected() && selectedButton == polygonToolBtn) {
            VisualizerController.panel.getDrawingCanvas().resetButtonCalled();
            VisualizerController.panel.addPolygonDrawingLayer();
            VisualizerController.panel.getDrawingCanvas().setEnableNegative(true);
            VisualizerController.panel.getDrawingCanvas().setEnableShading(true);
            VisualizerController.panel.getDrawingCanvas().setEnableBrush(false);
            VisualizerController.headerController.shadeCorrectorVisibility(false);
            VisualizerController.panel.getDrawingCanvas().getPolygonDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.NEGATIVETOOL));
        } else if (negativeToolButton.isSelected() && selectedButton == brushToolButton) {
            VisualizerController.panel.addPolygonDrawingLayer();
            VisualizerController.panel.getDrawingCanvas().setEnableNegative(true);
            VisualizerController.panel.getDrawingCanvas().setEnableShading(false);
            VisualizerController.panel.getDrawingCanvas().resetButtonCalled();
            VisualizerController.panel.getDrawingCanvas().setEnableBrush(true);
            VisualizerController.headerController.shadeCorrectorVisibility(false);
            VisualizerController.panel.getDrawingCanvas().getPolygonDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.NEGATIVEBRUSH));
        } else if (selectedButton == polygonToolBtn) {
            VisualizerController.panel.addPolygonDrawingLayer();
            VisualizerController.panel.getDrawingCanvas().setEnableBrush(false);
            VisualizerController.panel.getDrawingCanvas().setEnableShading(true);
            VisualizerController.panel.getDrawingCanvas().setEnableNegative(false);
            VisualizerController.headerController.shadeCorrectorVisibility(false);
            VisualizerController.panel.getDrawingCanvas().getPolygonDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.POLYGON));
        } else if (selectedButton == brushToolButton) {
            VisualizerController.panel.addPolygonDrawingLayer();
            VisualizerController.panel.getDrawingCanvas().setEnableShading(false);
            VisualizerController.panel.getDrawingCanvas().resetButtonCalled();
            VisualizerController.panel.getDrawingCanvas().setEnableBrush(true);
            VisualizerController.panel.getDrawingCanvas().setEnableNegative(false);
            VisualizerController.headerController.shadeCorrectorVisibility(false);
            VisualizerController.panel.getDrawingCanvas().getPolygonDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.BRUSH));
        } else if (selectedButton == null) {
            VisualizerController.panel.removePolygonDrawingLayer();
            VisualizerController.panel.getDrawingCanvas().setEnableBrush(false);
            VisualizerController.panel.getDrawingCanvas().setEnableShading(false);
            VisualizerController.panel.getDrawingCanvas().setEnableNegative(false);
            VisualizerController.panel.getDrawingCanvas().resetButtonCalled();
            VisualizerController.headerController.shadeCorrectorVisibility(false);
        } else {
            negativeToolButton.setSelected(false);
            VisualizerController.panel.removePolygonDrawingLayer();
            VisualizerController.panel.getDrawingCanvas().setEnableNegative(false);
            VisualizerController.panel.getDrawingCanvas().setEnableShading(false);
            VisualizerController.panel.getDrawingCanvas().resetButtonCalled();
            VisualizerController.panel.getDrawingCanvas().setEnableBrush(false);
            VisualizerController.headerController.shadeCorrectorVisibility(false);
        }
    }

    public void toggleButtonReset() {
        if (group1.getSelectedToggle() != null) {
            group1.getSelectedToggle().setSelected(false);
        }
        toggleButtonPressed();
    }

    @FXML
    public void onZoomOutHandler(ActionEvent event) {
        zoomOutButton.setSelected(false);
        zoomOutActionPerformed();
    }

    @FXML
    public void onZoomInHandler(ActionEvent event) {
        zoomInButton.setSelected(false);
        zoomInActionPerformed();
    }

    public void zoomInActionPerformed() {
        Util.setScale(Util.getScale() * 1.1);
        
        updateAfterScroll();
    }

    public void zoomOutActionPerformed() {

        Util.setScale((1 / 1.1) * Util.getScale());

        updateAfterScroll();
    }

    public static VisualizerController getVisualizerController() {
        return visualizerController;
    }

    public static void setVisualizerController(VisualizerController aVisualizerController) {
        visualizerController = aVisualizerController;
    }

    public void updateAfterScroll() {
        
        VisualizerController.panel.repaint();
        JScrollBar v = VisualizerController.scrollPane.getVerticalScrollBar();
        v.setValue(10);
        JScrollBar h = VisualizerController.scrollPane.getHorizontalScrollBar();
        h.setValue(10);

        VisualizerController.scrollPane.updateUI();
        VisualizerController.scrollPane.repaint();
        v.setValue(0);
        h.setValue(0);
        VisualizerController.scrollPane.updateUI();
        VisualizerController.scrollPane.repaint();
//        visualizerController.centerContent();
    }

    @FXML
    private void onNegativeToolButtonHandle(ActionEvent event) {
        System.out.println("Negative tool Selected");
        if (polygonToolBtn.isSelected() || brushToolButton.isSelected()) {
            negativeToolButton.setSelected(true);

        } else {
            negativeToolButton.setSelected(false);
        }
        toggleButtonPressed();
    }
}
