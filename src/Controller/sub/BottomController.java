/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.sub;

import Controller.AutoCompleteComboBoxListener;
import Controller.Util;
import static Controller.Util.awtToFXColor;
import Controller.VisualizerController;
import static Controller.VisualizerController.panel;
import Database.ShadesModel;
import Tools.ColorStyle;
import Tools.CursorType;
import Tools.CustomArea;
import Tools.CustomBrush;
import Tools.CustomColor;
import Tools.DrawingCanvas;
import Tools.Grouping;
import Tools.ToolsModel;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.TrayNotification;

public class BottomController implements Initializable {

    public static void setVisualizerController(VisualizerController aThis) {
        visualizerController = aThis;
    }

    public static VisualizerController getVisualizerController() {
        return visualizerController;
    }

    @FXML
    public ListView<Rectangle> recentUsedShade;
    @FXML
    public ListView<Rectangle> shadeUsedInImage;
    @FXML
    public RadioButton shadeName;
    @FXML
    public ToggleGroup group2;
    @FXML
    public RadioButton shadeCode;
    @FXML
    public ComboBox<String> cmbAutoComplete;
    @FXML
    public Button btnSearch;
    @FXML
    public Label selectedShade;
    @FXML
    public Label lblName;
    @FXML
    public Label lblCode;
    @FXML
    public Label lblPageNo;
    @FXML
    public Label lblShadeIn;
    @FXML
    public ToggleButton btnBlack;
    @FXML
    public ToggleGroup groupColor;
    @FXML
    public ToggleButton btnWhite;
    @FXML
    public ToggleButton btnRemoveColor;
    TrayNotification notification;
    public String description;
    public final ObservableList<Rectangle> recentUseColor = FXCollections.observableArrayList();
    public final ObservableList<Rectangle> shadeUsedColor = FXCollections.observableArrayList();
    public final Set<Color> recentUsedColorSet = new HashSet<>();
    public final ObservableList<String> autoCompleteCombobox = FXCollections.observableArrayList();

    private static VisualizerController visualizerController;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        recentUsedShade.setItems(recentUseColor);
        Collections.sort(recentUseColor, comparatorMyObject_byDay);
        recentUsedShade.setOrientation(Orientation.HORIZONTAL);
        recentUsedShade.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Rectangle> observable, Rectangle oldValue, Rectangle newValue) -> {
            selectedShade.setBackground(new Background(new BackgroundFill(newValue.getFill(), CornerRadii.EMPTY, Insets.EMPTY)));
        });
        shadeUsedInImage.setItems(shadeUsedColor);
        shadeUsedInImage.setOrientation(Orientation.HORIZONTAL);
        shadeUsedInImage.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Rectangle> observable, Rectangle oldValue, Rectangle newValue) -> {
            selectedShade.setBackground(new Background(new BackgroundFill(newValue.getFill(), CornerRadii.EMPTY, Insets.EMPTY)));
        });
        cmbAutoComplete.setItems(autoCompleteCombobox);
        group2.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
            getData();
        });
        new AutoCompleteComboBoxListener<>(cmbAutoComplete);//making autoComplete Combobox
        getData();
    }

    Comparator<? super Rectangle> comparatorMyObject_byDay = (Rectangle o1, Rectangle o2) -> o2.getId().compareToIgnoreCase(o1.getId());

    public void getData() {
        cmbAutoComplete.getEditor().setText("");
        cmbAutoComplete.getSelectionModel().clearSelection();
        autoCompleteCombobox.clear();
        List<ShadesModel> f = ShadesModel.findAll();
        f.stream().forEach((f1) -> {
            autoCompleteCombobox.add(String.valueOf(shadeCode.isSelected() ? f1.code : f1.name));
        });
    }

    @FXML
    public void onBlackColorSelection(ActionEvent event) {
        Util.setShadeColor(new java.awt.Color(0, 0, 0, DrawingCanvas.DEFAULT_ALPHA));
        double red = 0;
        double green = 0;
        double blue = 0;
        double opacity = 1;

        Color c1 = new Color(red, green, blue, opacity);
        selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
        VisualizerController.leftController.toggleButtonReset();
        panel.getDrawingCanvas().disableAllTools();
        setColorName("Black Color", "", "", "");
        VisualizerController.panel.getDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.COLORFILL));
    }

    @FXML
    public void onWhiteColorSelection(ActionEvent event) {
        Util.setShadeColor(new java.awt.Color(254, 255, 255, DrawingCanvas.DEFAULT_ALPHA));
        double red = 1;
        double green = 1;
        double blue = 1;
        double opacity = 1;
        Color c1 = new Color(red, green, blue, opacity);
        selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
        VisualizerController.leftController.toggleButtonReset();
        panel.getDrawingCanvas().disableAllTools();
        setColorName("White Color", "", "", "");
        VisualizerController.panel.getDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.COLORFILL));
    }

    @FXML
    public void onRemoveColorSelection(ActionEvent event) {
        Util.setShadeColor(new java.awt.Color(255, 255, 255, 0));
        double red = 1;
        double green = 1;
        double blue = 1;
        double opacity = 0;
        Color c1 = new Color(red, green, blue, opacity);
        selectedShade.setBackground(new Background(new BackgroundFill(c1, CornerRadii.EMPTY, Insets.EMPTY)));
        VisualizerController.leftController.toggleButtonReset();
        panel.getDrawingCanvas().disableAllTools();
        setColorName("Remove Color", "", "", "");
        VisualizerController.panel.getDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.COLORFILL));
    }

    public void setColorName(String title, String code, String pageNo, String shade) {
        lblName.setText(title);
        lblCode.setText(code);
        lblPageNo.setText(pageNo);
        lblShadeIn.setText(shade);
        if ("Remove Color".equals(title) || "Black Color".equals(title)) {
            lblName.setTextFill(Color.web("#ffffff"));
        } else {

            lblName.setTextFill(Color.web("#343434"));
        }

    }

    @FXML
    public void onSearchHandler(ActionEvent event) {
        
        if (VisualizerController.leftController.group1.getSelectedToggle() != null) {
            VisualizerController.leftController.group1.getSelectedToggle().setSelected(false);
        }
        VisualizerController.leftController.toggleButtonPressed();
        if (shadeCode.isSelected()) {
            if (!cmbAutoComplete.getEditor().getText().isEmpty()) {
                for (ShadesModel s : ShadesModel.shadesList) {
                    if (s.code.equals(cmbAutoComplete.getEditor().getText())) {
                        lblName.setText(s.name);
                        lblCode.setText(String.valueOf(s.code));
                        lblPageNo.setText(String.valueOf("Page No: " + s.page));
                        lblShadeIn.setText(s.Na_in);
                        description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
                        Util.setShadeColor(new java.awt.Color(s.red, s.green, s.blue, DrawingCanvas.DEFAULT_ALPHA));
                        selectedShade.setBackground(new Background(new BackgroundFill(awtToFXColor(new java.awt.Color(s.red, s.green, s.blue, 255)), CornerRadii.EMPTY, Insets.EMPTY)));
                        break;
                    }
                }
            }
        } else if (shadeName.isSelected()) {
            if (!cmbAutoComplete.getEditor().getText().isEmpty()) {
                for (ShadesModel s : ShadesModel.shadesList) {
                    if (s.name == null ? cmbAutoComplete.getEditor().getText() == null : s.name.equals(cmbAutoComplete.getEditor().getText())) {
                        System.out.println("I Came Here");
                        lblName.setText(s.name);
                        lblCode.setText(String.valueOf(s.code));
                        lblPageNo.setText(String.valueOf("Page No: " + s.page));
                        lblShadeIn.setText(s.Na_in);
                        description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
                        Util.setShadeColor(new java.awt.Color(s.red, s.green, s.blue, DrawingCanvas.DEFAULT_ALPHA));
                        selectedShade.setBackground(new Background(new BackgroundFill(awtToFXColor(new java.awt.Color(s.red, s.green, s.blue, 255)), CornerRadii.EMPTY, Insets.EMPTY)));
                        break;
                    }
                }
            }
        }
        VisualizerController.panel.drawingCanvas.setCursor(VisualizerController.cursorFactory.create(CursorType.COLORFILL));
        getData();
    }

    public void addRecentUsedShade(java.awt.Color c) {
        Platform.runLater(() -> {
            Color c1 = awtToFXColor(c);
            if (!recentUsedColorSet.contains(c1)) {
                Rectangle rec = new Rectangle(40, 50);
                rec.setAccessibleHelp(String.valueOf(c.getRGB()));
                applyMouseEventToRec(rec);
                rec.setFill(c1);
                recentUseColor.add(rec);
                recentUsedColorSet.add(c1);
            }
        });

    }

    public void applyMouseEventToRec(Rectangle rec) {
        final ScaleTransition scaleTransition
                = new ScaleTransition(Duration.millis(1), rec);
        scaleTransition.setToX(2f);
        scaleTransition.setToY(2f);
        final ScaleTransition scaleTransitionAnti
                = new ScaleTransition(Duration.millis(1), rec);
        scaleTransitionAnti.setToX(1f);
        scaleTransitionAnti.setToY(1f);
        rec.setOnMouseEntered((MouseEvent arg0) -> {
            scaleTransition.play();
            scaleTransitionAnti.stop();
            for (ToolsModel m : panel.getDrawingCanvas().getShadeList()) {
                if (m.getID().equals(rec.getId())) {
                    panel.getDrawingCanvas().applyColorToShade(m, ColorStyle.HOVER);
                    break;
                }

            }
            panel.repaint();
        });
        rec.setOnMouseExited((MouseEvent arg0) -> {
            scaleTransition.stop();
            scaleTransitionAnti.play();
            for (ToolsModel m : panel.getDrawingCanvas().getShadeList()) {
                if (m.getID().equals(rec.getId())) {
                    panel.getDrawingCanvas().applyColorToShade(m, ColorStyle.DEFAULT);
                    break;
                }

            }
            panel.repaint();
        });

        rec.setOnMouseClicked((MouseEvent e) -> {
            Platform.runLater(() -> {
                try {
                    if (VisualizerController.leftController.group1.getSelectedToggle() != null) {
                        VisualizerController.leftController.group1.getSelectedToggle().setSelected(false);
                    }
                    VisualizerController.leftController.toggleButtonPressed();
                    VisualizerController.panel.getDrawingCanvas().setCursor(VisualizerController.cursorFactory.create(CursorType.COLORFILL));
                    java.awt.Color c = new java.awt.Color(Integer.parseInt(rec.getAccessibleHelp()));
                    System.out.println(c);
                    for (ShadesModel s : ShadesModel.findAll()) {
                        if (s.red == c.getRed() && s.green == c.getGreen() && s.blue == c.getBlue()) {
                            lblName.setText(s.name);
                            lblCode.setText(String.valueOf(s.code));
                            lblPageNo.setText(String.valueOf("Page No: " + s.page));
                            lblShadeIn.setText(s.Na_in);
                            description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
                            rec.setAccessibleRoleDescription(description);
                            Util.setShadeColor(new java.awt.Color(c.getRed(), c.getGreen(), c.getBlue(), DrawingCanvas.DEFAULT_ALPHA));
                            break;
                        }
                    }
                    selectedShade.setBackground(new Background(new BackgroundFill(awtToFXColor(c), CornerRadii.EMPTY, Insets.EMPTY)));
                } catch (Exception ex) {
                    Logger.getLogger(VisualizerController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            });
        });
    }

    public ObservableList<Rectangle> getShadeUsedColor() {
        return shadeUsedColor;
    }

    public void addShadeSelectedInImage(java.awt.Color c, String id) {
        Platform.runLater(() -> {
            if (id != null) {
                boolean update = false;
                for (Rectangle r : shadeUsedColor) {
                    if (r.getId().equals(id)) {
                        r.setFill(awtToFXColor(c));
                        r.setAccessibleHelp(String.valueOf(c.getRGB()));
                        r.setAccessibleRoleDescription(description);
                        update = true;
                        break;
                    }
                }
                if (!update) {
                    addRecInShade(c, id);
                }
            }
        });

    }

    public void addRecInShade(java.awt.Color c, String id) {
        Rectangle rec = new Rectangle(40, 50);
        applyMouseEventToRec(rec);
        rec.setAccessibleHelp(String.valueOf(c.getRGB()));
        rec.setFill(awtToFXColor(c));
        rec.setId(id);
        description = null;
        if (description == null) {
            description = "Default Color";
        }
        rec.setAccessibleRoleDescription(description);
        shadeUsedColor.add(rec);
    }

    public void removeRecentUsedShade(int index) {
        Platform.runLater(() -> {
            recentUseColor.remove(index);
        });

    }

    public void removeShadeSelectedInImage(Rectangle index) {
        Platform.runLater(() -> {
            shadeUsedColor.remove(index);
        });

    }

    public void updateShadeSelectedImage(ToolsModel toolModel, String operation) {
        if (toolModel instanceof CustomArea || toolModel instanceof CustomBrush) {
            if (null != operation) {
                switch (operation) {
                    case "Add":
                        addRecInShade(toolModel.getColor(), toolModel.getID());
                        break;
                    case "Remove":
                        for (Rectangle r : shadeUsedColor) {
                            if (r.getId().equals(toolModel.getID())) {
                                removeShadeSelectedInImage(r);
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (toolModel instanceof CustomColor) {

            CustomColor customColor = (CustomColor) toolModel;

            if (customColor.getToolsModel() instanceof Grouping) {

            } else {
                for (Rectangle r : shadeUsedColor) {
                    if (r.getId().equals(customColor.getShape().getID())) {
                        r.setFill(awtToFXColor(customColor.getShape().getColor()));
                        break;
                    }
                }
            }

        }

    }

    public void setValueOfColor(ShadesModel s) {
        lblName.setText(s.name);
        lblCode.setText(String.valueOf(s.code));
        lblPageNo.setText(String.valueOf("Page No: " + s.page));
        lblShadeIn.setText(s.Na_in);
        description = s.name + "\n" + s.code + "\n" + "Page No:" + s.page + "\n" + s.Na_in;
        lblName.setTextFill(Color.web("#343434"));
    }

    public void showTrayNotification(String title, String message) {

        if (notification != null) {
            if (notification.isTrayShowing()) {
                return;
            }
        }

        notification = new TrayNotification();
        notification.setTitle(title);
        notification.setRectangleFill(Paint.valueOf("DD366C"));
        notification.setImage(new Image(getClass().getResourceAsStream("/Controller/images/logo.png")));
        notification.setMessage(message);
        notification.setAnimationType(AnimationType.POPUP);
        notification.showAndDismiss(Duration.seconds(2));
    }
}
