/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.sub;

import Controller.AboutPageController;
import Controller.GlobalValues;
import Controller.SaveImageController;
import Controller.Util;
import Controller.VisualizerController;
import static Controller.VisualizerController.shadeCorrector;
import Controller.WebViewHelpController;
import Controller.WelcomeController;
import Main.Main;
import Report.PrintPreview;
import Tools.LoadingDialog;
import java.awt.KeyboardFocusManager;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Menu;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * FXML Controller class
 *
 * @author Your Name <dinesh katwal  at nepallink.net>
 */
public class HeaderController implements Initializable {

    public static VisualizerController getVisualizerController() {
        return visualizerController;
    }

    public static void setVisualizerController(VisualizerController aVisualizerController) {
        visualizerController = aVisualizerController;
    }

    @FXML
    public MenuItem saveDialog;
    @FXML
    public MenuItem saveAsDialog;
    @FXML
    public MenuItem printImage;
    @FXML
    public Button deleteShadeButton;
    @FXML
    public Button groupBtn;
    @FXML
    public Button unGroupBtn;
    @FXML
    public Button doneButton;
    @FXML
    public Button resetButton;
    @FXML
    public Button undoBtn;
    @FXML
    public Button redoBtn;
    @FXML
    public Button shadeCorrectorBtn;
    @FXML
    public Slider slider;
    @FXML
    public Button shadeCorrectorDoneBtn;
    @FXML
    public Button helpButton;
    @FXML
    private Button backButton;
    @FXML
    private MenuItem exportJpeg;
    @FXML
    private Menu menuHelp;
    @FXML
    private MenuItem userManualDialog;
    @FXML
    private MenuItem aboutDialog;
    
    private static VisualizerController visualizerController;
    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Image helpIcon = new Image(getClass().getResourceAsStream("/Controller/images/help.png"));
        ImageView helpView = new ImageView(helpIcon);
        menuHelp.setGraphic(helpView);
        
        backButton.setOnAction((ActionEvent event) -> {
            try {
                int dialogBtn = JOptionPane.showConfirmDialog(visualizerController, "Do you want to save the changes?");
                if(dialogBtn == JOptionPane.YES_OPTION){
                    VisualizerController.headerController.onSaveImage(null);
                }
                else if(dialogBtn == JOptionPane.NO_OPTION){
                    int val = GlobalValues.getCameFrom();
                    if(val == 0){
                        loadWindow("/Controller/SampleSelection.fxml", "Sample Images", 0);
                    }
                    else if(val == 1){
                        loadWindow("/Controller/ImageSelection.fxml", "Saved Images", 0);
                    }
                    else{
                        loadHomeWindow();
                    }
                }
                else{
                    return;
                }

                /*
                    ----- CameFrom Mapping -----
                    0           Came From SAMPLE IMAGES
                    1           Came From SAVED IMAGES
                    2           Came From NEW IMAGE
                    -1          ERROR
                
                    ----- BackActive Mapping -----
                    true    Back button pressed
                    false   Normal Save (Not through Back button)
                */
                
                GlobalValues.setBackActive(true);

            } catch (Exception ex) {
                Logger.getLogger(VisualizerController.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        slider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            System.out.println("new Value: " + newValue.intValue());
            System.out.println("Old Value: " + oldValue.intValue());

            if (oldValue.intValue() == newValue.intValue()) {
                return;
            }

            shadeCorrector.revalidate(oldValue, newValue);

        });
        disableDoneResetButton(true);
        undoBtn.setDisable(true);
        redoBtn.setDisable(true);
    }
    
    private void loadHomeWindow(){
        
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/welcome.fxml"));
            Parent root = (Parent) loader.load();
            WelcomeController controller = (WelcomeController) loader.getController();
            Image image = new Image("/Controller/images/bg.jpg");
            controller.imageView.setImage(image);

            controller.imageView.fitWidthProperty().bind(stage.widthProperty());
            controller.imageView.fitHeightProperty().bind(stage.heightProperty());
            
            Platform.runLater(()->{
                ObservableList<Screen> screens = Screen.getScreensForRectangle(new Rectangle2D(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight()));
                Rectangle2D bounds = screens.get(0).getVisualBounds();
                stage.setX(bounds.getMinX());
                stage.setX(bounds.getMinY());
                stage.setWidth(bounds.getWidth());
                stage.setHeight(bounds.getHeight());
                
                Scene scene = new Scene(root);
                stage.setScene(scene);
                controller.imageView.fitHeightProperty().bind(scene.heightProperty());
                stage.sizeToScene();
                stage.setMaximized(true);
                stage.setResizable(true);
            });
            
            stage.setTitle("Jasmine Paints - Color Choice");
            stage.show();
            getVisualizerController().dispose();
            stage.getIcons().add(new Image(Main.class.getResource("logo.png").toExternalForm()));
            stage.setOnCloseRequest((WindowEvent event) -> {
                System.exit(0);
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
    private void loadWindow(String resource, String title, int checkVal){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource(resource));
            Parent root = (Parent) loader.load();
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle(title);
            stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
            if(checkVal == 1){
                WelcomeController controller = (WelcomeController) loader.getController();
                controller.imageView.fitWidthProperty().bind(stage.widthProperty());
                controller.imageView.fitHeightProperty().bind(stage.heightProperty());
            }
            stage.setResizable(true);
            stage.setMaximized(true);
            stage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {
                @Override
                    public void handle(WindowEvent window) {
                        getVisualizerController().dispose();
                    }
                });
            stage.show();
            //((Stage) backButton.getScene().getWindow()).close();
            GlobalValues.setImagePath(null);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    public void onSaveImage(ActionEvent event) {
        Platform.runLater(() -> {
            Alert error = new Alert(Alert.AlertType.INFORMATION);
            
            /* Changed Code */
            File file = GlobalValues.getImagePath();
            if (file != null) {
                if (file.getAbsolutePath().contains("data")) {
                    save();
                    return;
                }
                File imageToSave = file;
                if (!imageToSave.exists()) {
                    imageToSave.mkdirs();
                }
                if (imageToSave.exists()) {
                    Util.setFilePath(file);
                    VisualizerController.panel.getDrawingCanvas().saveObject();
                    error.setContentText("Image Saved Successfully.");
                    Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(1), (ActionEvent event1) -> {
                        error.setResult(ButtonType.YES);
                        error.hide();
                        
                        if(GlobalValues.isBackActive()){
                            int val = GlobalValues.getCameFrom();
                            if(val == 0){
                                loadWindow("/Controller/SampleSelection.fxml", "Sample Images", 0);
                            }
                            else if(val == 1){
                                loadWindow("/Controller/ImageSelection.fxml", "Saved Images", 0);
                            }
                            else{
                                //loadWindow("/Controller/welcome.fxml", "Color Visualizer - Jasmine Paints!", 1);
                                loadHomeWindow();
                            }
                            GlobalValues.setBackActive(false);
                        }
                    }));
                    idlestage.setCycleCount(1);
                    idlestage.play();
                    error.showAndWait();
                }
            } else {
                save();
            }
            //GlobalValues.setImagePath(null);
        });
    }

    @FXML
    public void onSaveAsHandler(ActionEvent event) {
        save();
    }

    @FXML
    public void onDeleteShadeHandle(ActionEvent event) {
        VisualizerController.panel.getDrawingCanvas().deleteShadeButtonCalled();
    }

    @FXML
    public void onGroupBtnHandle(ActionEvent event) {
        System.out.println("group Button is clicked");
        VisualizerController.panel.getDrawingCanvas().groupButtonCalled();
    }

    @FXML
    public void onUngroupBtnHandle(ActionEvent event) {
        System.out.println("Ungroup Button is clicked");
        VisualizerController.panel.getDrawingCanvas().unGroupButtonCalled(true);
    }

    @FXML
    public void onUndoBtnHandle(ActionEvent event) {
        System.out.println("Undo Button is clicked");
        boolean b = VisualizerController.redoUndoManager.undo();
        redoBtn.setDisable(false);
        if (!b) {
            undoBtn.setDisable(true);
        }
    }

    @FXML
    public void onRedoBtnHandle(ActionEvent event) {
        System.out.println("On RedoBtn is clicked");
        boolean b = VisualizerController.redoUndoManager.redo();
        undoBtn.setDisable(false);
        if (!b) {
            redoBtn.setDisable(true);
        }
    }

    @FXML
    public void onShadeCorrectorBtnHandle(ActionEvent event) {
        System.out.println("Shade corrector is clicked");
        if (!VisualizerController.panel.getDrawingCanvas().getTempSelectionList().isEmpty()) {
            shadeCorrectorVisibility(true);
        }
    }

    @FXML
    public void onShadeCorrectorDoneBtnHandle(ActionEvent event) {
        System.out.println("Shade Corrector Done Button");
        shadeCorrectorVisibility(false);
    }

    @FXML
    public void onHelpHandler(ActionEvent event) {
        System.out.println("Help Button is clicked");
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/PdfViewer.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Controller.Controller helpController = fxmlLoader.<Controller.Controller>getController();
            SwingUtilities.invokeLater(() -> {
                JFXPanel content = new JFXPanel();
                content.setScene(new Scene(root));

                JDialog dialog = new JDialog(getVisualizerController());
                dialog.setTitle("Help");
                dialog.setIconImage(new ImageIcon(getClass().getResource("/Controller/images/logo.png")).getImage());
                dialog.setContentPane(content);
                dialog.setSize(820, 720);
                dialog.setModal(true);
                dialog.setLocationRelativeTo(null);
                dialog.setVisible(true);
                helpController.setWindow(dialog);
            });

        } catch (Exception ex) {
            Logger.getLogger(VisualizerController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void onPrintHandler(ActionEvent event) {
        printImageHandle();
    }

    @FXML
    public void onDoneHandler(ActionEvent event) {
        VisualizerController.panel.getDrawingCanvas().doneButtonCalled();
    }

    @FXML
    public void onResetHandler(ActionEvent event) {
        VisualizerController.panel.getDrawingCanvas().resetButtonCalled();
    }

    public void save() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/SaveImage.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            SaveImageController controller = fxmlLoader.<SaveImageController>getController();
            controller.setPanel(VisualizerController.panel.getDrawingCanvas());
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Save Image");
            stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (Exception ex) {

        }
    }

    public void shadeCorrectorVisibility(boolean value) {

        if (slider.isVisible()) {
            if (!VisualizerController.panel.getDrawingCanvas().getTempSelectionList().isEmpty()) {
//                VisualizerController.panel.getDrawingCanvas().setSelectionShadeColorToDefault();
                VisualizerController.panel.getDrawingCanvas().getTempSelectionList().clear();
            }
        }
        slider.setVisible(value);
        shadeCorrectorDoneBtn.setVisible(value);
    }

    public void printImageHandle() {
        LoadingDialog ld = new LoadingDialog(getVisualizerController(), true);
        new Thread(() -> {
            try {
                File f = new File("data/drawnImage.jpg");
                BufferedImage drawImage = VisualizerController.panel.getDrawingCanvas().getPreviewImage();
                if (drawImage.getWidth() < drawImage.getHeight()) {
                    drawImage = Util.resize(drawImage, drawImage.getWidth(), drawImage.getWidth());
//                    drawImage = (BufferedImage) drawImage.getScaledInstance(drawImage.getWidth(), drawImage.getWidth(), java.awt.Image.SCALE_SMOOTH);
                }
                ImageIO.write(drawImage, "jpg", f);
                PrintPreview printPreview = new PrintPreview(VisualizerController.bottomController.shadeUsedColor, f.getAbsolutePath(), ld);
            } catch (IOException ex) {
                Logger.getLogger(HeaderController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
        ld.setVisible(true);
    }

    public void disableDoneResetButton(boolean value) {
        doneButton.setDisable(value);
        resetButton.setDisable(value);
        deleteShadeButton.setDisable(!value);
        groupBtn.setDisable(!value);
        unGroupBtn.setDisable(!value);
        shadeCorrectorBtn.setDisable(!value);

    }

    @FXML
    public void onExportJpg(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            LoadingDialog ld = new LoadingDialog(getVisualizerController(), true);
            new Thread(() -> {
                try {
                    File f = new File("data/drawnImage.jpg");
                    BufferedImage drawImage = VisualizerController.panel.getDrawingCanvas().getPreviewImage();
                    if (drawImage.getWidth() < drawImage.getHeight()) {
                        drawImage = Util.resize(drawImage, drawImage.getWidth(), drawImage.getWidth());
//                    drawImage = (BufferedImage) drawImage.getScaledInstance(drawImage.getWidth(), drawImage.getWidth(), java.awt.Image.SCALE_SMOOTH);
                    }
                    ImageIO.write(drawImage, "jpg", f);
                    PrintPreview printPreview = new PrintPreview();
                    printPreview.ExportImage(VisualizerController.bottomController.shadeUsedColor, f.getAbsolutePath(), ld, file);
                } catch (IOException ex) {
                    Logger.getLogger(HeaderController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
            ld.setVisible(true);
        }

    }
    
    @FXML
    public void onUserManual(ActionEvent event){
        
        try {
//            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/PdfViewer.fxml"));
//            Parent root = (Parent) fxmlLoader.load();
//            Controller.Controller helpController = fxmlLoader.<Controller.Controller>getController();
//            SwingUtilities.invokeLater(() -> {
//                JFXPanel content = new JFXPanel();
//                content.setScene(new Scene(root));
//
//                JDialog dialog = new JDialog(getVisualizerController());
//                dialog.setTitle("Help");
//                dialog.setIconImage(new ImageIcon(getClass().getResource("/Controller/images/logo.png")).getImage());
//                dialog.setContentPane(content);
//                dialog.setSize(820, 720);
//                dialog.setModal(true);
//                dialog.setLocationRelativeTo(null);
//                dialog.setVisible(true);
//                helpController.setWindow(dialog);
//            });

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/WebViewHelp.fxml"));
                Parent root = (Parent) loader.load();
                WebViewHelpController controller = loader.<WebViewHelpController>getController();
                SwingUtilities.invokeLater(() ->{
                    JFXPanel content = new JFXPanel();
                    content.setScene(new Scene(root));
                    JDialog dialog = new JDialog(getVisualizerController());
                    dialog.setTitle("Help");
                    dialog.setIconImage(new ImageIcon(getClass().getResource("/Controller/images/logo.png")).getImage());
                    dialog.setContentPane(content);
                    dialog.setSize(1072, 670);
                    dialog.setResizable(false);
                    dialog.setModal(true);
                    dialog.setLocationRelativeTo(null);
                    dialog.setVisible(true);
                    controller.setWindow(dialog);
                });

        } catch (Exception ex) {
            Logger.getLogger(VisualizerController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @FXML
    public void onAbout(ActionEvent event){
        
        try{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/AboutPage.fxml"));
        Parent root = (Parent) loader.load();
        AboutPageController controller = loader.<AboutPageController>getController();
            SwingUtilities.invokeLater(() ->{
                JFXPanel content = new JFXPanel();
                content.setScene(new Scene(root));
                JDialog dialog = new JDialog(getVisualizerController());
                dialog.setTitle("About");
                dialog.setIconImage(new ImageIcon(getClass().getResource("/Controller/images/logo.png")).getImage());
                dialog.setContentPane(content);
                dialog.setSize(380, 315);
                dialog.setResizable(false);
                dialog.setModal(true);
                dialog.setLocationRelativeTo(null);
                dialog.setVisible(true);
                controller.setWindow(dialog);
                });
        }
        catch(Exception e){ }
        
    }
    
}
