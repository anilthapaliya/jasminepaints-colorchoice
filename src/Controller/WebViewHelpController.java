/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.media.MediaView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.swing.JDialog;

/**
 *
 * @author Anil
 */
public class WebViewHelpController implements Initializable{
    
    @FXML
    private Hyperlink linkGettingStarted;

    @FXML
    private Hyperlink linkPolygon;

    @FXML
    private Hyperlink linkDelShade;

    @FXML
    private Hyperlink linkOverview;

    @FXML
    private Hyperlink linkExportJPG;

    @FXML
    private Hyperlink linkPolygonTool;

    @FXML
    private Hyperlink linkGrouping;

    @FXML
    private Hyperlink linkShadeCorrector;

    @FXML
    private Hyperlink linkPrintPreview;

    @FXML
    private Hyperlink linkNewImage;

    @FXML
    private Hyperlink linkBrush;

    @FXML
    private Hyperlink linkFitOnScreen;

    @FXML
    private Hyperlink linkZoom;

    @FXML
    private Hyperlink linkSampleImage;

    @FXML
    private Label txtTitle;

    @FXML
    private Pane borderPaneBody;

    @FXML
    private WebView webView;
    
    @FXML
    private MediaView mediaView;
    
    @FXML
    private Button btnPlay;
    
    @FXML
    private Button btnPause;
    
    @FXML
    private Button btnStop;
    
    @FXML
    private HBox mediaButtons;
    
    @FXML
    private AnchorPane anchorPane;
    
    MediaPlayer mediaPlayer;
    private JDialog window;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        mediaView.setVisible(false);
        mediaButtons.setVisible(false);
        WebEngine engine = webView.getEngine();
        linkOverview.requestFocus();
        txtTitle.setText("Guide Overview");
        String path = Controller.class.getResource("/Controller/WebPage/guideOverview.html").toExternalForm();
        engine.load(path);
        
        linkOverview.setOnAction((event) -> {
            txtTitle.setText("Guide Overview");
            webView.setVisible(true);
            mediaView.setVisible(false);
            mediaButtons.setVisible(false);
            String path1 = Controller.class.getResource("/Controller/WebPage/guideOverview.html").toExternalForm();
            engine.load(path1);
        });
        
        linkGettingStarted.setOnAction((event) -> {
            txtTitle.setText("Getting Started");
            webView.setVisible(true);
            mediaView.setVisible(false);
            mediaButtons.setVisible(false);
            String path1 = Controller.class.getResource("/Controller/WebPage/gettingStarted.html").toExternalForm();
            engine.load(path1);
        });
        
        linkSampleImage.setOnAction((event) -> {
            txtTitle.setText("Sample Image");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/sample_image.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkNewImage.setOnAction((event) -> {
            txtTitle.setText("New Image");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/Newimage.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkPolygonTool.setOnAction((event) -> {
            txtTitle.setText("Polygon Tool");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/Polygon_tool.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
       
        linkZoom.setOnAction((event) -> {
            txtTitle.setText("Zoom In/Out Tool");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/zoom_in_out_tool.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkFitOnScreen.setOnAction((event) -> {
            txtTitle.setText("Fit On Screen Tool");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/Fit_on_screen_tool.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkBrush.setOnAction((event) -> {
            txtTitle.setText("Brush/Negarive Tool");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/Brushtool_and_negative_tool.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkPolygon.setOnAction((event) -> {
            txtTitle.setText("Polygon/Negative Tool");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/polygon_and_negarive_tool.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkShadeCorrector.setOnAction((event) -> {
            txtTitle.setText("Shade Corrector");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/shade_corrector.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkDelShade.setOnAction((event) -> {
            txtTitle.setText("Delete Shade");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/delete_shade.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkPrintPreview.setOnAction((event) -> {
            txtTitle.setText("Print Preview");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/Print_preview.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkExportJPG.setOnAction((event) -> {
            txtTitle.setText("Export As JPG");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/exported_as_jpg.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        linkGrouping.setOnAction((event) -> {
            txtTitle.setText("Group/Ungroup Shades");
            mediaView.setVisible(true);
            webView.setVisible(false);
            mediaButtons.setVisible(true);
            File file = new File("/Controller/WebPage/video/sample_image.mp4");
            Media media = new Media(Controller.class.getResource("/Controller/WebPage/video/Group_ungroup.mp4").toExternalForm());
            //MediaPlayer player = new MediaPlayer(media);
            //mediaView.setMediaPlayer(player);
            setMediaToMediaPlayer(media);
            mediaPlayer.play();
        });
        
        btnPlay.setOnAction((event) -> {
            String curTime = mediaPlayer.getCurrentTime().toString();
            String stopTime = mediaPlayer.getStopTime().toString();
            curTime = curTime.substring(0, 2);
            stopTime = stopTime.substring(0, 2);
            System.out.println("Current Time: " + curTime);
            System.out.println("Stop Time: " + stopTime);
            if((!curTime.equalsIgnoreCase("0.")) && (Integer.parseInt(curTime) == Integer.parseInt(stopTime))){
                mediaPlayer.stop();
                mediaPlayer.play();
            }
            //if(!mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING))
                mediaPlayer.play();
        });
        
        btnPause.setOnAction((event) -> {
            if(!mediaPlayer.getStatus().equals(MediaPlayer.Status.PAUSED))
                mediaPlayer.pause();
        });
        
        btnStop.setOnAction((event) -> {
            if(mediaPlayer.getStatus().equals(MediaPlayer.Status.PLAYING) || mediaPlayer.getStatus().equals(MediaPlayer.Status.PAUSED))
                mediaPlayer.stop();
        });
        
    }
    
    private void setMediaToMediaPlayer(Media media){
        
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        mediaView.setMediaPlayer(mediaPlayer);
        
    }
    
    public JDialog getWindow() {
        return window;
    }

    public void setWindow(JDialog window) {
        this.window = window;
    }
    
}
