/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


import Main.Main;
import Tools.BrightnessAndContrast;
import java.awt.event.WindowAdapter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 * FXML Controller class
 *
 * @author DELL-PC
 */
public class BrightnessAndContrastController implements Initializable {

    @FXML
    private Slider brishtnessSlider;
    @FXML
    private Slider contrastSlider;
    @FXML
    private Button doneBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private ImageView imageView;

    private BrightnessAndContrast brightnessAndContrast;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imageView.setImage(SwingFXUtils.toFXImage(Util.getImg(), null));
        imageView.setFitWidth(Util.getImg().getWidth());
        imageView.setFitHeight(Util.getImg().getHeight());
        brightnessAndContrast = new BrightnessAndContrast(Util.getImg(), imageView);

        brishtnessSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            brightnessAndContrast.revalidateBrightness(oldValue, newValue);
        });
        contrastSlider.valueProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            brightnessAndContrast.revalidateContrast(oldValue, newValue);
        });
    }

    @FXML
    private void onDoneButtonHandle(ActionEvent event) {
        Util.setImg(brightnessAndContrast.getImage());
        VisualizerController frame = new VisualizerController();
        frame.setTitle("Jasmine Paints - Color Choice");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setIconImage(new ImageIcon(getClass().getResource("/Controller/images/logo.png")).getImage());
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(java.awt.event.WindowEvent e) {
                Platform.runLater(() -> {
                    (((Node) event.getSource()).getScene().getWindow()).hide();
                });
            }
        });
        frame.setVisible(true);
        frame.pack();
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
    }

    @FXML
    private void onCancelButtonHandle(ActionEvent event) {
        try {
            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/welcome.fxml"));
            Parent root = (Parent) loader.load();
            WelcomeController controller = (WelcomeController) loader.getController();
            Image image = new Image("/Controller/images/bg.jpg");
            controller.imageView.setImage(image);

            controller.imageView.fitWidthProperty().bind(primaryStage.widthProperty());
            //controller.imageView.fitHeightProperty().bind(primaryStage.heightProperty());
            Scene scene = new Scene(root);
            
            Platform.runLater(()->{
                ObservableList<Screen> screens = Screen.getScreensForRectangle(new Rectangle2D(primaryStage.getX(), primaryStage.getY(), primaryStage.getWidth(), primaryStage.getHeight()));
                Rectangle2D bounds = screens.get(0).getVisualBounds();
                primaryStage.setX(bounds.getMinX());
                primaryStage.setX(bounds.getMinY() - 10);
                primaryStage.setWidth(bounds.getWidth());
                primaryStage.setHeight(bounds.getHeight() - 10);
                
                primaryStage.setScene(scene);
                controller.imageView.fitHeightProperty().bind(scene.heightProperty());
                controller.imageView.fitWidthProperty().bind(scene.widthProperty());
                primaryStage.sizeToScene();
                primaryStage.setMaximized(true);
                primaryStage.setResizable(true);
            });
            
            primaryStage.setTitle("Jasmine Paints - Color Choice");
            primaryStage.show();
            primaryStage.getIcons().add(new Image(Main.class.getResource("logo.png").toExternalForm()));
            primaryStage.setOnCloseRequest((WindowEvent e) -> {
                System.exit(0);
            });
            ((Stage) cancelBtn.getScene().getWindow()).close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
