/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Main;
import java.awt.HeadlessException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;

/**
 * FXML Controller class
 *
 * @author dinesh
 */
public class WelcomeController implements Initializable {

    @FXML
    private Button myImageButton;
    @FXML
    private Button openSavedImage;
    private String workingDirectory;
    @FXML
    private Button sampleImageButton;
    @FXML
    public ImageView imageView;
    @FXML
    private ImageView hoverImageView;
    @FXML
    private Button btnHelp;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        GlobalValues.setSelectedTab(0);
        GlobalValues.setSelectedIndex(0);
        
        getDirectory();
        if (!Util.applicationStart) {
            checkForUpdate();
            Util.applicationStart=true;
        }

        //Clear Item From the Util file
        Util.setFilePath(null);
        Util.setImg(null);
        Util.setShadeColor(null);
        Util.setPf(null);
        //End of the clear
        
        myImageButton.setOnAction((ActionEvent event) -> {
            try {
                GlobalValues.setCameFrom(2);  /* Set value */
                FileChooser fileChooser = new FileChooser();

                //Set extension filter
                FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
                FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
                FileChooser.ExtensionFilter extFilterJPEG = new FileChooser.ExtensionFilter("JPG files (*.jpeg)", "*.JPEG");
                fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG, extFilterJPEG);
                //Show open file dialog
                File file = fileChooser.showOpenDialog((Stage) btnHelp.getScene().getWindow());
                if (file != null) {
                    BufferedImage bufferedImage;
                    bufferedImage = ImageIO.read(file);

                    if (bufferedImage.getWidth() > 1200 || bufferedImage.getHeight() > 1200) {
                        Util.setImg(resizeImage(bufferedImage, 1200));
                    } else {
                        Util.setImg(bufferedImage);
                    }
                    bufferedImage.flush();
                    try {
                        Stage stage = new Stage();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/BrightnessAndContrast.fxml"));
                        Parent root = (Parent) loader.load();
                        BrightnessAndContrastController controller = (BrightnessAndContrastController) loader.getController();
                        Scene scene = new Scene(root);
                        stage.setScene(scene);
                        stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
                        stage.setTitle("Jasmine Paints - Color Choice");
                        stage.setResizable(true);
                        stage.setMaximized(true);
                        stage.addEventHandler(WindowEvent.WINDOW_SHOWN, (WindowEvent window) -> {
                            ((Node) event.getSource()).getScene().getWindow().hide();
                        });
                        stage.show();
                    } catch (Exception ex) {
                        Logger.getLogger(WelcomeController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            } catch (IOException | HeadlessException ex) {
                Logger.getLogger(VisualizerController.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        });
        sampleImageButton.setOnMouseEntered((MouseEvent event) -> {
            sampleImageButton.setStyle("-fx-effect: dropshadow(three-pass-box, black, 2, 2, 0, 0);-fx-background-color:transparent;");
            hoverImageView.setImage(Util.getImage("3"));
        });
        sampleImageButton.setOnMouseExited((MouseEvent event) -> {
            hoverImageView.setImage(Util.getImage("1"));
            sampleImageButton.setStyle("-fx-effect: dropshadow(three-pass-box, transparent, 5, 5, 0, 0);-fx-background-color:transparent;");
        });
        myImageButton.setOnMouseEntered((MouseEvent event) -> {
            hoverImageView.setImage(Util.getImage("2"));
            myImageButton.setStyle("-fx-effect: dropshadow(three-pass-box, black, 2, 2, 0, 0);-fx-background-color:transparent;");
        });
        myImageButton.setOnMouseExited((MouseEvent event) -> {
            hoverImageView.setImage(Util.getImage("1"));
            myImageButton.setStyle("-fx-effect: dropshadow(three-pass-box, transparent, 5, 5, 0, 0);-fx-background-color:transparent;");
        });
        openSavedImage.setOnMouseEntered((MouseEvent event) -> {
            hoverImageView.setImage(Util.getImage("4"));
            openSavedImage.setStyle("-fx-effect: dropshadow(three-pass-box, black, 2, 2, 0, 0);-fx-background-color:transparent;");
        });
        openSavedImage.setOnMouseExited((MouseEvent event) -> {
            hoverImageView.setImage(Util.getImage("1"));
            openSavedImage.setStyle("-fx-effect: dropshadow(three-pass-box, transparent, 5, 5, 0, 0);-fx-background-color:transparent;");

        });
        
        btnHelp.setOnMouseEntered((event) -> {
            btnHelp.setStyle("-fx-background-color: #43C5F2;");
        });
        btnHelp.setOnMouseExited((event) -> {
            btnHelp.setStyle("-fx-background-color: #014919;");
        });
        
//        System.out.println(imageView.getImage());
//        System.out.println(myStage.getWidth());
//        imageView.fitWidthProperty().bind(myStage.widthProperty());

        showHelp();
        
    }
    
    public void showHelp(){
        
        btnHelp.setOnAction((ActionEvent event) -> {
            try{
                Stage currStage = (Stage) btnHelp.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controller/WebViewHelp.fxml"));
                Parent root = (Parent) loader.load();
                WebViewHelpController controller = (WebViewHelpController) loader.getController();
                
                Stage newStage = new Stage();
                Scene scene = new Scene(root);
                newStage.getIcons().add(new Image(Main.class.getResource("logo.png").toExternalForm()));
                newStage.initModality(Modality.APPLICATION_MODAL);
                newStage.initOwner(currStage);
                newStage.setScene(scene);            
                newStage.setTitle("Help");
                newStage.setResizable(false);
                newStage.setFullScreen(false);
                newStage.centerOnScreen();
                newStage.show();
            }
            catch(Exception e){ }
        });
        
    }

    @FXML
    private void openSavedImageHandle(ActionEvent event) {
        try {
            GlobalValues.setCameFrom(1);  /* Set value */
            ((Node) (event.getSource())).getScene().getWindow().hide();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/ImageSelection.fxml"));
            Parent rt = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(rt));
            stage.setTitle("Saved Images");
            stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
            stage.setResizable(true);
            stage.setMaximized(true);
            stage.show();
        } catch (Exception ex) {
            Logger.getLogger(VisualizerController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getDirectory() {
        try {
            String OS = (System.getProperty("os.name")).toUpperCase();
            if (OS.contains("WIN")) {
                workingDirectory = System.getenv("AppData");
            } else {
                workingDirectory = System.getProperty("user.home");
                workingDirectory += "/Library/Application Support";
            }
            workingDirectory += "/jasminepaint";
            String interiorFile = workingDirectory + "/Interior";
            String exteriorFile = workingDirectory + "/Exterior";
            File homeDirectory = new File(workingDirectory);

            if (!homeDirectory.exists()) {
                homeDirectory.mkdir();
            }
            Util.setSaveInterior(interiorFile);
            Util.setSaveExterior(exteriorFile);

            File interior = new File(interiorFile);
            File exterior = new File(exteriorFile);
            if (!interior.exists()) {
                interior.mkdir();
            }
            if (!exterior.exists()) {
                exterior.mkdir();
            }
            //System.out.println(interiorFile + " " + exteriorFile);
        } catch (Exception ex) {
            Logger.getLogger(VisualizerController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void checkForUpdate() {
        String configName = "config.properties";
        String path = "data";
        File config = new File(path);
        if (!config.exists()) {
            createNewData();
        }
        File[] listFiles = config.listFiles();
        for (File d1 : listFiles) {
            if (configName.equals(d1.getName())) {
                try {
                    Properties prop = new Properties();
                    prop.load(new FileInputStream(new File("data/" + configName)));
                    String oldVersion = prop.getProperty("version");
                    prop.load(getClass().getResourceAsStream(configName));
                    String newVersion = prop.getProperty("version");
                    System.out.println(newVersion);
                    if (newVersion.equals(oldVersion)) {
                    } else {
                        deleteOldData();
                        createNewData();
                        System.out.println("Need to Delete File");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(WelcomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("Config not exist");
                deleteOldData();
                createNewData();
            }
        }
    }

    public void deleteOldData() {
        File old = new File("data");
        Util.deleteFolder(old);
    }

    public boolean createNewData() {
        try {
            URL input = getClass().getResource("config.properties");
            File dest = new File("data/config.properties");
            FileUtils.copyURLToFile(input, dest);

        } catch (IOException ex) {
            Logger.getLogger(WelcomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    @FXML
    private void onSampleImageHandle(ActionEvent event) {
        try {
             GlobalValues.setCameFrom(0);  /* Set value */
            ((Node) (event.getSource())).getScene().getWindow().hide();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Controller/SampleSelection.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.getIcons().add(new Image(getClass().getResource("/Controller/images/logo.png").toExternalForm()));
            stage.setTitle("Sample Images");
            stage.setResizable(true);
            stage.setMaximized(true);
            stage.show();
        } catch (Exception ex) {
            Logger.getLogger(VisualizerController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private BufferedImage resizeImage(BufferedImage originalImage, int size) {
        BufferedImage scaledImage = Scalr.resize(originalImage, size);
        return scaledImage;
    }

}