/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Tools.DrawingCanvas;
import Tools.MainPane;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author medinesh
 */
public class DoneResetDialogController implements Initializable {

    @FXML
    private Button doneButton;
    @FXML
    private Button resetButton;
    private DrawingCanvas panel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        doneButton.setOnAction((ActionEvent event) -> {
            panel.doneButtonCalled();
            ((Stage) doneButton.getScene().getWindow()).close();
        });
        resetButton.setOnAction((ActionEvent event) -> {
            panel.resetButtonCalled();
            ((Stage) doneButton.getScene().getWindow()).close();
        });
    }

    public void setPanel(DrawingCanvas panel) {
        this.panel = panel;
    }

  

}
