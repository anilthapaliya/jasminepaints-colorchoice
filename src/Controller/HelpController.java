/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.JDialog;
import org.apache.commons.io.FileUtils;

/**
 * FXML Controller class
 *
 * @author DELL-PC
 */
public class HelpController implements Initializable {

    @FXML
    private Button closeBtn;
    @FXML
    private WebView webView;
    private WebEngine webengine;
    private JDialog window;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        extractFile();
        final URI uri = java.nio.file.Paths.get("data/help/web/viewer.html").toAbsolutePath().toUri();

        final ProgressIndicator progress = new ProgressIndicator();

        this.webengine = this.webView.getEngine();
        this.webengine.load(uri.toString());

        progress.progressProperty().bind(webengine.getLoadWorker().progressProperty());
        progress.visibleProperty().bind(progress.progressProperty().lessThan(1));
        webengine.getLoadWorker().stateProperty().addListener(
                new ChangeListener<State>() {
            @Override
            public void changed(ObservableValue ov, State oldState, State newState) {

                if (newState == Worker.State.SUCCEEDED) {
                    System.out.println("Html loaded");
                } else if (newState == Worker.State.FAILED) {
                    System.out.println("Html Loading Failed");
                    progress.setVisible(false);

                }
            }
        });
    }

    @FXML

    private void closeBtnActionHandle(ActionEvent event) {
        getWindow().dispose();
    }

    public JDialog getWindow() {
        return window;
    }

    public void setWindow(JDialog window) {
        this.window = window;
    }

    public void extractFile() {
        if (!new File("data/help").exists()) {
            try {
                URL input = getClass().getResource("help.zip");
                File dest = new File("data/help.zip");
                FileUtils.copyURLToFile(input, dest);
                UnzipUtility unzipFile = new UnzipUtility();
                unzipFile.unzip(dest.getPath(), dest.getParentFile().getPath());
            } catch (IOException ex) {
                Logger.getLogger(SampleImageController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
